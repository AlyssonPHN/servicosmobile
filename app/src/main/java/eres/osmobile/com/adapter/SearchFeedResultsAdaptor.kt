package eres.osmobile.com.adapter

import android.content.Context
import android.database.Cursor
import android.support.v4.widget.SimpleCursorAdapter
import android.view.View
import android.widget.TextView

import eres.osmobile.com.R

class SearchFeedResultsAdaptor(context: Context, layout: Int, c: Cursor?, from: Array<String>, to: IntArray?, flags: Int) : SimpleCursorAdapter(context, layout, c, from, to, flags) {


    override fun bindView(view: View, context: Context?, cursor: Cursor) {
        val txtnomedocliente = view.findViewById<View>(R.id.txt_nomedocliente) as TextView
        val txtstatus = view.findViewById<View>(R.id.txt_status) as TextView
        txtnomedocliente.text = cursor.getString(1)
        txtstatus.text = cursor.getString(2)


    }
}
