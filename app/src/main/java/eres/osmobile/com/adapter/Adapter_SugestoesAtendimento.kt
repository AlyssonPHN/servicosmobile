package eres.osmobile.com.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import eres.osmobile.com.R
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.interfaces.AtendimentoListener
import eres.osmobile.com.model.Atendimento
import eres.osmobile.com.model.Status

class Adapter_SugestoesAtendimento(private val context: Context, private val atendimentoListener: AtendimentoListener, private val atendimentoList: ArrayList<Atendimento?>, private val statusList: List<Status>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_ITEM = 1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val vh: RecyclerView.ViewHolder
        if (viewType == VIEW_ITEM) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.adapter_sugestoes_atendimento, parent, false)

            vh = MyViewHolder(view)
        } else {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.adapter_progress, parent, false)

            vh = ProgressViewHolder(v)
        }
        return vh
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            //Pega atendimento da posição
            val atendimento = atendimentoList[position]
            // Pega status do atendimento
            val stato = MetodoEmComum.buscarStatus(statusList!!, atendimento!!.status)

            // Preenchedo componentes com os valores do atendimento
            holder.txt_status.text = if (stato != null) stato.descricao else "STATUS INVÁLIDO OU NÃO EXISTE"
            holder.txt_nomedocliente.text = atendimento.nome
            holder.txtCod.text = atendimento.idatd.toString()
        }
    }

    override fun getItemCount(): Int {
        return atendimentoList.size
    }

    fun addItemList(atendimento: Atendimento?, position: Int) {
        atendimentoList.add(atendimento)
        notifyItemInserted(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        val VIEW_PROG = 0
        return if (atendimentoList[position] != null) VIEW_ITEM else VIEW_PROG
    }

    private inner class MyViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        internal var ctl_corpo: ConstraintLayout
        internal var txtCod: TextView
        internal var txt_nomedocliente: TextView
        internal var txt_status: TextView


        init {
            ctl_corpo = view.findViewById(R.id.ctl_corpo)
            txtCod = view.findViewById(R.id.txtCod)
            txt_nomedocliente = view.findViewById(R.id.txt_nomedocliente)
            txt_status = view.findViewById(R.id.txt_status)

            ctl_corpo.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            val atendimento: Atendimento? = atendimentoList[adapterPosition]
            when(v){
                ctl_corpo ->{
                    atendimentoListener.onAtendimento(adapterPosition,atendimento)
                }
            }
        }
    }

    private class ProgressViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
        internal var progressBar: ProgressBar

        init {
            progressBar = v.findViewById<View>(R.id.progressfooter) as ProgressBar
        }
    }

}