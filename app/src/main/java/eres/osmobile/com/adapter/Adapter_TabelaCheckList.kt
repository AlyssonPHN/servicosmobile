package eres.osmobile.com.adapter

import android.support.constraint.ConstraintLayout
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ProgressBar
import android.widget.TextView
import eres.osmobile.com.R
import eres.osmobile.com.model.TabelaCheckList

class Adapter_TabelaCheckList(private val listTabelaCheckList: ArrayList<TabelaCheckList?>, private var mSelecionadas: ArrayList<TabelaCheckList>, private val fab: FloatingActionButton) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEWITEM = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var vh: RecyclerView.ViewHolder? = null
        when (viewType) {
            VIEWITEM -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_tabelachecklist, parent, false)
                vh = MyViewHolder(view)
            }
            0 -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_progress, parent, false)
                vh = ProgressViewHolder(view)
            }
        }
        return vh!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            val tabelaChekList = listTabelaCheckList[position]

            when (tabelaChekList) {
                is TabelaCheckList -> {
                    holder.txtDescricao.text = tabelaChekList.descricao
                    holder.txtCod.text = tabelaChekList._id.toString()
                    clickCheckbox(holder.checkbox, tabelaChekList)
                    clickcorpo(holder.ctrl_corpo, holder.checkbox, tabelaChekList)
                    mostrarTxtPreenchido(holder.txtpreenchido, tabelaChekList)
                    persistirSelecao(holder.checkbox, tabelaChekList)
                }

            }
        }

    }

    private fun clickCheckbox(checkBox: CheckBox, tabelaCheckList: TabelaCheckList) {
        checkBox.setOnClickListener {
            selecionar(checkBox, tabelaCheckList)
        }
    }

    private fun clickcorpo(ctrl_corpo: ConstraintLayout, checkBox: CheckBox, tabelaCheckList: TabelaCheckList) {
        ctrl_corpo.setOnClickListener {
            selecionar(checkBox, tabelaCheckList)
        }
    }

    private fun selecionar(checkBox: CheckBox, tabelaCheckList: TabelaCheckList) {
        if (checkBox.isChecked && !mSelecionadas.contains(tabelaCheckList)) {
            mSelecionadas.add(tabelaCheckList)
        } else if (!checkBox.isChecked && !mSelecionadas.contains(tabelaCheckList)){
            mSelecionadas.add(tabelaCheckList)
            checkBox.isChecked = true
        } else{
            // Percorrer pelos tabelas da lista e exclui caso o id seja igual
            mSelecionadas.filter { it._id == tabelaCheckList._id }.forEach { mSelecionadas.remove(it) }
            checkBox.isChecked = false
        }
        mostrarFab()
    }

    private fun persistirSelecao(checkBox: CheckBox, tabelaCheckList: TabelaCheckList) {
        // Percorrer pelos tabelas da lista e marca o checkbox que deve ser selecionado.
        mSelecionadas.filter { it._id == tabelaCheckList._id }.forEach { checkBox.isChecked = true }
    }

    private fun mostrarTxtPreenchido(textView: TextView, tabelaCheckList: TabelaCheckList) {
        if (tabelaCheckList.preenchido != 0) {
            textView.visibility = View.VISIBLE
        } else {
            textView.visibility = View.GONE
        }
    }

    private fun mostrarFab() {
        if (mSelecionadas.size > 0) {
            fab.show()
        } else {
            fab.hide()
        }
    }

    override fun getItemCount(): Int {
        return listTabelaCheckList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (listTabelaCheckList[position] != null) VIEWITEM else 0
    }


    fun addItemList(tabelaCheckList: TabelaCheckList?, position: Int) {
        if (tabelaCheckList != null) {
            listTabelaCheckList.add(tabelaCheckList)
        }
        notifyItemInserted(position)
    }

    inner class MyViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val txtDescricao: TextView = mView.findViewById(R.id.tituloChecklist)
        val txtCod: TextView = mView.findViewById(R.id.txtCod)
        val checkbox: CheckBox = mView.findViewById(R.id.checkbox)
        val txtpreenchido: TextView = mView.findViewById(R.id.txtpreenchido)
        val ctrl_corpo: ConstraintLayout = mView.findViewById(R.id.ctrl_corpo)
    }

    private class ProgressViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
        internal var progressBar: ProgressBar

        init {
            progressBar = v.findViewById(R.id.progressfooter)
        }
    }
}
