package eres.osmobile.com.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import eres.osmobile.com.R
import eres.osmobile.com.adapter_viewholders.CabCheckList_ViewHolder
import eres.osmobile.com.adapter_viewholders.CheckListPerguntasViewHolder
import eres.osmobile.com.model.CabCheckListPronto
import eres.osmobile.com.model.CheckList_Resposta
import java.util.*
import kotlin.collections.ArrayList

class Adapter_CheckList_ResponderNovo(private val context: Context, groups: ArrayList<CabCheckListPronto?>) : ExpandableRecyclerViewAdapter<CabCheckList_ViewHolder, CheckListPerguntasViewHolder>(groups) {

    var date = Date()
    var posicoesPendentes: ArrayList<Int>? = null

    override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): CabCheckList_ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_cheklist_cabecalho, parent, false)
        return CabCheckList_ViewHolder(view)
    }

    override fun onBindGroupViewHolder(holder: CabCheckList_ViewHolder, flatPosition: Int,
                                       group: ExpandableGroup<*>) {
        holder.setGenreTitle(group)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int): CheckListPerguntasViewHolder {
        val view = LayoutInflater.from(parent!!.context)
                .inflate(R.layout.list_item_checklist_perguntas, parent, false)
        return CheckListPerguntasViewHolder(view, context)
    }

    override fun onBindChildViewHolder(holder: CheckListPerguntasViewHolder?, flatPosition: Int, group: ExpandableGroup<*>?, childIndex: Int) {
        @Suppress("CAST_NEVER_SUCCEEDS")
        val checkList_resposta = group!!.items[childIndex] as CheckList_Resposta
        holder!!.perguntaTextView.text = checkList_resposta.descricao

        holder.posicoesPendentes = posicoesPendentes
        holder.pergunta(checkList_resposta, date)
    }

}
