package eres.osmobile.com.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import eres.osmobile.com.R
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.model.ItemProduto
import eres.osmobile.com.model.Produto
import eres.osmobile.com.viewPagers.CadastroAtendimento

class Adapter_Produtos(val context: Context, val produtosList: ArrayList<Produto>, private val podeAdicionar: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_ITEM = 1
    var mSelecionados: ArrayList<ItemProduto> = ArrayList()
    private var mAutoIncrement = false
    private var mAutoDecrement = false
    private var impediralteracao: Boolean = false
    private var valorProdutoInicial = 0.0
    private val repeatUpdateHandler = Handler()
    private val selectedItems = SparseBooleanArray()
    private var mItensDeletar: ArrayList<Int>? = null
    var mRecyclerViewOnClickListenerHack: RecyclerViewOnClickListenerHack? = null

    init {
        if (podeAdicionar) {
            // Get selected items
            mSelecionados = if (CadastroAtendimento.itensProdutoSelecionados != null) CadastroAtendimento.itensProdutoSelecionados!! else mSelecionados
        } else {
            // Selected items are the same displayed
            mSelecionados = produtosList as ArrayList<ItemProduto>
            mItensDeletar = ArrayList()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var vh: RecyclerView.ViewHolder? = null
        when (viewType) {
            VIEW_ITEM -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.produtos_adapter, parent, false)
                vh = MyViewHolder(view)
            }
            0 -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_progress, parent, false)
                vh = ProgressViewHolder(view)
            }
        }
        return vh!!
    }

    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            val objeto = produtosList[position]
            realcarProdutoSelecionado(objeto, holder, position)
            when (objeto) {
                is ItemProduto -> {
                    val itemProduto = objeto
                    holder.txtDescricao.text = "${itemProduto.produtoID} - ${itemProduto.descricao}"
                    holder.txtValor.text = MetodoEmComum.formatoMoedasemCifrao(itemProduto.preco)
                    holder.txtQuantidade.text = MetodoEmComum.doubleComDuasCasas(itemProduto.quantidade).toString()
                    holder.txtValorEscolhido.text = MetodoEmComum.formatoMoedasemCifrao(itemProduto.preco)
                    holder.txtTotal.text = MetodoEmComum.formatoMoeda(itemProduto.quantidade * itemProduto.preco)
                    holder.ctl_valor.visibility = View.GONE
                    holder.ctl_soma.visibility = View.VISIBLE
                }
                is Produto -> {
                    val produto = objeto
                    holder.txtDescricao.text = "${produto.IDpro} - ${produto.xdesc}"
                    holder.txtValor.text = MetodoEmComum.formatoMoedasemCifrao(produto.prc_sug)
                    holder.txtValorEscolhido.text = MetodoEmComum.formatoMoedasemCifrao(produto.prc_sug)
                    setInformacoesItemSelecionado(produto, holder)
                }
            }
            if (!podeAdicionar) {
                holder.ctl_corpo.setOnClickListener(holder)
                holder.ctl_corpo.setOnLongClickListener(holder)
            }
            holder.btn_menos.setOnClickListener(holder)
            holder.btn_mais.setOnClickListener(holder)
            holder.btn_menos.setOnTouchListener(holder)
            holder.btn_mais.setOnTouchListener(holder)
            holder.btn_menos.setOnLongClickListener(holder)
            holder.btn_mais.setOnLongClickListener(holder)
        }


    }

    override fun getItemCount(): Int = produtosList.size

    override fun getItemViewType(position: Int): Int {
        return if (produtosList[position] != null) VIEW_ITEM else 0
    }


    fun addItemList(produto: Produto?, position: Int) {
        if (produto != null) {
            produtosList.add(produto)
        }
        notifyItemInserted(position)
    }

    fun atualizarItem(position: Int) {
        notifyItemChanged(position)
    }

    fun desmarcarItem(position: Int) {
        notifyItemChanged(position)
    }

    //Remove os itens selecionados da lista
    fun removerItensList() {
        for (i in 0..mItensDeletar!!.size - 1) {
            removerProdutoSelecionado(mItensDeletar!!.get(i))
        }
        mItensDeletar!!.clear()
        selectedItems.clear()
    }

    private fun removerProdutoSelecionado(idproduto: Int) {
        for (i in 0 until mSelecionados.size) {

            if (mSelecionados[i].produtoID == idproduto) {
                //Pega o item selecionado
                //val itemProduto = mSelecionados[i]
                //Atualiza o saldo disponível do cliente ao  cancelar produto
                //Remove o item da lista de itens selecionados
                mSelecionados.removeAt(i)
                CadastroAtendimento.itensProdutoSelecionados = mSelecionados
                //Atualiza subtotal
                atualizarSubTotal(i)
                notifyItemRemoved(i)
                //sai o for
                break
            }
        }
    }

    fun cancelarExclusao() {
        selectedItems.clear()
        mItensDeletar!!.clear()
        notifyDataSetChanged()
    }

    // Atualiza ProdutosFragments pela interface
    private fun atualizarSubTotal(position: Int) {
        if (mRecyclerViewOnClickListenerHack != null) {
            //Mostra que ouve mudança no item
            CadastroAtendimento.mudancaItens = true
            mRecyclerViewOnClickListenerHack!!.onAtualizar(position)
        }
    }

    inner class MyViewHolder(mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener, RecyclerViewOnClickListenerHack, View.OnLongClickListener, View.OnTouchListener {

        val ctl_corpo: ConstraintLayout = mView.findViewById(R.id.ctl_corpo)
        val ctl_valor: ConstraintLayout = mView.findViewById(R.id.ctl_valor)
        val ctl_soma: ConstraintLayout = mView.findViewById(R.id.ctl_soma)
        val txtDescricao: TextView = mView.findViewById(R.id.tituloChecklist)
        val txtValor: TextView = mView.findViewById(R.id.txtValor)
        val txtQuantidade: TextView = mView.findViewById(R.id.txtQuantidade)
        val txtValorEscolhido: TextView = mView.findViewById(R.id.txtValorEscolhido)
        val txtTotal: TextView = mView.findViewById(R.id.txtTotal)
        var btn_menos: ImageButton = mView.findViewById(R.id.btn_menos)
        val btn_mais: ImageButton = mView.findViewById(R.id.btn_mais)


        override fun onClick(v: View?) {
            valorInicial()
            val itemProduto = pegarItemProduto()
            when (v) {
                btn_mais -> {
                    val quantidade = itemProduto?.quantidade ?: 0.0

                    //Só permite adicionar caso a quantidade esteja visível ou se a quantidade
                    // for igual a 0 e componente esteja visível
                    if (((ctl_soma.visibility == View.VISIBLE) || (quantidade == 0.0 && ctl_soma.getVisibility() == View.GONE))) {
                        //Se não passou pelo onlongclick então pode adicionar
                        if (!impediralteracao) {
                            aumentaQuantidade(quantidade, itemProduto)
                        } else {
                            impediralteracao = false
                        }

                    }
                }
                btn_menos -> {
                    if (ctl_soma.visibility == View.VISIBLE) {
                        //Se não passou pelo onlongclick então pode adicionar
                        if (!impediralteracao) {
                            diminuiQuantidade(itemProduto)
                        } else {
                            impediralteracao = false
                        }
                    }
                }
                ctl_corpo -> {
                    if (selectedItems.size() > 0) {
                        adicionarRemoverItemAExcluir(adapterPosition, itemProduto!!.produtoID)
                    }
                }
            }


        }

        private fun diminuiQuantidade(itemProduto: ItemProduto?) {
            var quantidadeeditada = 0.0
            try {
                val quantidade = ("0" + txtQuantidade.getText().toString().replace(",", ".")).toDouble()

                if (quantidade > 0) {
                    //Diminui conforme a numeração
                    if (mAutoDecrement) {
                        if (quantidade > 10) {
                            quantidadeeditada = quantidade - 10
                        } else {
                            quantidadeeditada = quantidade - 1
                        }
                    } else {
                        quantidadeeditada = quantidade - 1
                    }
                }
                //Evita diminuir mais que 0
                if (quantidadeeditada >= 0) {
                    alterarItemPedido(itemProduto!!, quantidadeeditada)
                }
            } catch (ignored: Exception) {
            }

        }

        private fun aumentaQuantidade(qtdproduto: Double, itemProduto1: ItemProduto?) {
            var itemProduto = itemProduto1
            //Verifica se item existe na lista
            if (!mSelecionados.contains(itemProduto)) {
                itemProduto = addItemNovo()
            }

            val quantidadeeditada: Double
            try {
                val quantidade = qtdproduto
                //Aumenta conforme a numeração
                if (mAutoIncrement) {
                    if (quantidade > 10.0) {
                        quantidadeeditada = quantidade + 10.0
                    } else {
                        quantidadeeditada = quantidade + 1.0
                    }
                } else {
                    quantidadeeditada = quantidade + 1.0
                }
                alterarItemPedido(itemProduto!!, quantidadeeditada)
            } catch (ignored: Exception) {
            }
        }

        private fun alterarItemPedido(itemProduto: ItemProduto, quantidade: Double) {
            val index = mSelecionados.indexOf(itemProduto)
            //atualiza item pedido
            itemProduto.quantidade = quantidade
            //Atualiza na lista
            mSelecionados[index] = itemProduto

            // Update txt
            val valor = quantidade * itemProduto.preco
            txtQuantidade.text = quantidade.toString()
            txtTotal.text = MetodoEmComum.formatoMoeda(valor)
            // Indicates that item has changed
            CadastroAtendimento.mudancaItens = true

            if (quantidade == 0.0) {
                mSelecionados.removeAt(index)
                atualizarSubTotal(adapterPosition)
                if (podeAdicionar) {
                    onExcluir(null, adapterPosition)
                } else {
                    notifyItemRemoved(adapterPosition)
                }
            } else {
                atualizarSubTotal(adapterPosition)
            }
        }

        private fun valorInicial() {
            var quantidade = 0.0
            val preco = (txtValor.getText().toString().replace(",", ".")).toDouble()
            //Pega a quantidade apenas se ela for visível, evita bug de quantidade ser 1
            // quando está invisível
            if (ctl_soma.visibility == View.VISIBLE) {
                quantidade = (txtQuantidade.getText().toString().replace(",", ".")).toDouble()
            }
            valorProdutoInicial = preco * quantidade
        }

        private fun pegarItemProduto(): ItemProduto? {
            var itemProduto: ItemProduto? = null
            when (podeAdicionar) {
            // It is an array of Produto
                true -> {
                    try {
                        itemProduto = mSelecionados[getIndexProdutoSelecionado(produtosList[adapterPosition])]
                    } catch (ignored: Exception) {

                    }
                }
            // It's an array of ItemProduto
                false -> {
                    if (adapterPosition >= 0) {
                        itemProduto = mSelecionados[adapterPosition]
                    }
                }
            }
            return itemProduto
        }

        fun getIndexProdutoSelecionado(produto: Produto): Int {
            // Retorna a primeira posição que achar, se não achar id será -1
            val retorno = (0..mSelecionados.size - 1).firstOrNull { mSelecionados[it].produtoID == produto.IDpro }
                    ?: -1
            return retorno
        }

        private fun addItemNovo(): ItemProduto {
            val produto = produtosList[adapterPosition]
            val itemProduto: ItemProduto = ItemProduto()

            var valorEdtPreco = 0.0
            try {
                valorEdtPreco = MetodoEmComum.doubleComDuasCasas(produto.prc_sug)
            } catch (ignored: Exception) {
                Toast.makeText(context, "Produto sem preço", Toast.LENGTH_SHORT).show()
            }

            if (valorEdtPreco != 0.0) {
                itemProduto.atendimentoId = 0
                itemProduto.produtoID = produto.IDpro
                itemProduto.descricao = produto.xdesc
                itemProduto.quantidade = 1.0
                itemProduto.preco = valorEdtPreco

                mSelecionados.add(itemProduto)
                CadastroAtendimento.itensProdutoSelecionados = mSelecionados
                onAtualizar(adapterPosition)
            }

            return itemProduto
        }

        internal inner class RepeticaoUpdater : Runnable {
            override fun run() {
                when {
                    mAutoIncrement -> {
                        valorInicial()
                        MetodoEmComum.aumentaQuantidadeProdutoTxt(txtQuantidade, mAutoIncrement)
                        val quantidade = ("0" + txtQuantidade.getText().toString().replace(",", ".")).toDouble()
                        val itemProduto = pegarItemProduto()
                        itemProduto!!.quantidade = quantidade
                        repeatUpdateHandler.postDelayed(RepeticaoUpdater(), 100)
                        impediralteracao = true

                    }
                    mAutoDecrement -> {
                        valorInicial()
                        MetodoEmComum.diminuiQuantidadeProdutoTxt(txtQuantidade, mAutoDecrement)
                        val quantidade = ("0" + txtQuantidade.getText().toString().replace(",", ".")).toDouble()
                        val itemProduto = pegarItemProduto()
                        itemProduto!!.quantidade = quantidade
                        repeatUpdateHandler.postDelayed(RepeticaoUpdater(), 100)
                        impediralteracao = true
                    }
                }

            }
        }

        override fun onLongClick(v: View?): Boolean {
            when (v) {
                btn_mais -> {
                    //Só aciona a adição de click longo caso o componentes estiver visível
                    if (ctl_soma.visibility == View.VISIBLE) {
                        mAutoIncrement = true
                        repeatUpdateHandler.post(RepeticaoUpdater())
                        return false
                    }
                }
                btn_menos -> {
                    //Só aciona a adição de click longo caso o componentes estiver visível
                    if (ctl_soma.visibility == View.VISIBLE) {
                        mAutoDecrement = true
                        repeatUpdateHandler.post(RepeticaoUpdater())
                        return false
                    }
                }
                ctl_corpo -> {
                    mRecyclerViewOnClickListenerHack!!.onClickListenerRv(v, adapterPosition)
                    val itemProduto = mSelecionados.get(adapterPosition)
                    adicionarRemoverItemAExcluir(adapterPosition, itemProduto.produtoID)
                }
            }
            return true
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            //Remover item caso quantidade seja 0
            val quantidade = ("0" + txtQuantidade.text.toString().replace(",", ".")).toDouble()

            when (v) {
                btn_mais -> {
                    //Quando o botão for soltado então autoincremento será false e contador também
                    if ((event!!.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL) && mAutoIncrement) {
                        mAutoIncrement = false
                        MetodoEmComum.zerarContador()

                        //Se quantidade é igual a 0, adiciona o item
                        if (quantidade == 0.0 && ctl_soma.visibility == View.GONE) {
                            aumentaQuantidade(quantidade, addItemNovo())
                        }
                        atualizarValores(quantidade)
                    }
                }
                btn_menos -> {
                    if ((event!!.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_CANCEL) && mAutoDecrement) {
                        mAutoDecrement = false
                        MetodoEmComum.zerarContador()
                        atualizarValores(quantidade)
                        if (quantidade == 0.0) {
                            if (podeAdicionar) {
                                val index = mSelecionados.indexOf(pegarItemProduto())
                                mSelecionados.removeAt(index)
                                onExcluir(null, adapterPosition)
                            } else {
                                mSelecionados.removeAt(adapterPosition)
                                notifyItemRemoved(adapterPosition)
                                //notifyItemRemoved(adapterPosition)
                            }
                        }

                    }
                }
            }
            return false
        }

        private fun atualizarValores(quantidade: Double) {
            var valorEdtPreco: Double = 0.0
            val objeto = produtosList[adapterPosition]
            when (objeto) {
                is ItemProduto -> {
                    valorEdtPreco = objeto.preco
                }
                is Produto -> {
                    valorEdtPreco = MetodoEmComum.doubleComDuasCasas(objeto.prc_sug)
                }
            }
            atualizarSubTotal(adapterPosition)
            atualizaValorTotal(quantidade, valorEdtPreco)

        }

        private fun atualizaValorTotal(quantidade: Double, preco: Double) {
            //Atualiza txt
            val valor = quantidade * preco
            txtQuantidade.text = quantidade.toString()
            txtTotal.text = MetodoEmComum.formatoMoeda(valor)
        }

        override fun onClickListenerRv(view: View, position: Int) {

        }

        override fun onExcluir(view: View?, position: Int) {
            selectedItems.delete(position)
            desmarcarItem(position)
        }

        override fun onAdicionar(posicao: Int) {

        }

        override fun onAtualizar(posicao: Int) {
            atualizarItem(posicao)
        }

        override fun onAlterar() {

        }
    }

    //Itens a serem adicionados ou removidos da exclusão
    private fun adicionarRemoverItemAExcluir(position: Int, idProduto: Int) {
        //Adiciona itens ainda não adicionados e exclui da lista itens clicados já adicionados
        if (selectedItems.get(position, false)) {
            //Apaga da lista
            selectedItems.delete(position)
            //Pega a posição do idProduto
            val key = mItensDeletar!!.indexOf(idProduto)
            //Deleta o id do produto na posição
            mItensDeletar!!.remove(key)

            //Caso não tenha mais itens a serem excluídos
            if (selectedItems.size() == 0) {
                //Então tirar botão de excluir do menu
                mRecyclerViewOnClickListenerHack!!.onAlterar()
            }
        } else {
            //Adiciona na lista
            selectedItems.put(position, true)
            mItensDeletar!!.add(idProduto)
        }
        //Atualiza item no adapter
        notifyItemChanged(position)
    }

    private fun realcarProdutoSelecionado(produto: Produto, holder: MyViewHolder, posicao: Int) = try {
        //Verifica as posições dos produtos foram selecionados
        if (podeAdicionar) {
            mSelecionados
                    .filter { it.produtoID == produto.IDpro }
                    .forEach { selectedItems.put(posicao, true) }
        }

        // Selected positions
        if (selectedItems.get(posicao, false)) {
            // Blue background
            holder.ctl_corpo.setBackgroundColor(Color.parseColor("#4734badb"))
        } else {
            // White background
            holder.ctl_corpo.setBackgroundColor(Color.parseColor("#FFFFFF"))
        }

    } catch (ignored: Exception) {

    }

    fun setInformacoesItemSelecionado(produto: Produto, holder: MyViewHolder) {

        try {
            for (i in 0..mSelecionados.size - 1) {

                if (mSelecionados[i].produtoID == produto.IDpro) {
                    holder.ctl_soma.visibility = View.VISIBLE
                    holder.ctl_valor.visibility = View.GONE
                    holder.txtQuantidade.text = String.format("%.2f", mSelecionados[i].quantidade)
                    holder.txtValorEscolhido.text = (MetodoEmComum.formatoMoeda(mSelecionados[i].preco))
                    holder.txtTotal.text = (MetodoEmComum.formatoMoeda(mSelecionados[i].quantidade * mSelecionados[i].preco))
                    break
                } else {
                    holder.ctl_soma.visibility = View.GONE
                    holder.ctl_valor.visibility = View.VISIBLE
                }
            }
        } catch (ignored: Exception) {

        }

    }

    private class ProgressViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
        internal var progressBar = v.findViewById<ProgressBar>(R.id.progressfooter)

    }
}
