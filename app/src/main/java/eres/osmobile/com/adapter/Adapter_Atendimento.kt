package eres.osmobile.com.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.fragments.AtendimentoFragment
import eres.osmobile.com.fragments.AtendimentosFragment
import eres.osmobile.com.fragments.TabelaCheckListFragment
import eres.osmobile.com.model.Atendimento
import eres.osmobile.com.model.Status

class Adapter_Atendimento(private val context: Context, private val atendimentoList: ArrayList<Atendimento?>, private val statusList: List<Status>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_ITEM = 1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val vh: RecyclerView.ViewHolder
        if (viewType == VIEW_ITEM) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.adapter_atendimento, parent, false)

            vh = MyViewHolder(view)
        } else {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.adapter_progress, parent, false)

            vh = ProgressViewHolder(v)
        }
        return vh
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            //Pega atendimento da posição
            val atendimento = atendimentoList[position]
            // Pega status do atendimento
            val stato = MetodoEmComum.buscarStatus(statusList, atendimento!!.status)

            // Aplica a cor do topo do cardview
            try {
                holder.ll_topo.setBackgroundColor(Color.parseColor("#" + stato!!.cor_topo))
            } catch (ignored: Exception) {
            }


            // Preenchedo componentes com os valores do atendimento

            holder.txt_status.text = if (stato != null) stato.descricao else "STATUS INVÁLIDO OU NÃO EXISTE"
            holder.txt_nome.text = atendimento.nome
            holder.txt_cod_id.text = atendimento.idatd.toString()
            holder.txt_data_entrada.text = atendimento.dataEntrada
            holder.txt_data_previsao.text = atendimento.dataPrevisao
            holder.txt_cpf_cnpj.text = atendimento.cnpj_cpf
            holder.txt_produto.text = atendimento.produto
            holder.txt_valor_total.text = MetodoEmComum.formatoMoedasemCifrao(atendimento.valorTotal)
        }

    }

    override fun getItemCount(): Int = atendimentoList.size

    fun addItemList(atendimento: Atendimento, position: Int) {
        atendimentoList.add(atendimento)
        notifyItemInserted(position)
    }

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItemViewType(position: Int): Int {
        val VIEW_PROG = 0
        return if (atendimentoList[position] != null) VIEW_ITEM else VIEW_PROG
    }

    private inner class MyViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        internal var txt_status: TextView
        internal var txt_nome: TextView
        internal var txt_cod_id: TextView
        internal var txt_data_entrada: TextView
        internal var txt_data_previsao: TextView
        internal var txt_cpf_cnpj: TextView
        internal var txt_produto: TextView
        internal var txt_valor_total: TextView
        internal var img_checklist: ImageView
        internal val cardView: CardView
        internal val ll_corpo: ConstraintLayout
        internal var ll_topo: LinearLayout

        init {
            txt_status = view.findViewById<View>(R.id.txt_status) as TextView
            txt_nome = view.findViewById<View>(R.id.txt_nome) as TextView
            txt_cod_id = view.findViewById<View>(R.id.txt_cod_id) as TextView
            txt_valor_total = view.findViewById<View>(R.id.txt_valor_total) as TextView
            txt_data_entrada = view.findViewById<View>(R.id.txt_data_entrada) as TextView
            txt_data_previsao = view.findViewById<View>(R.id.txt_data_previsao) as TextView
            txt_cpf_cnpj = view.findViewById<View>(R.id.txt_cpf_cnpj) as TextView
            txt_produto = view.findViewById<View>(R.id.txt_produto) as TextView
            img_checklist = view.findViewById<View>(R.id.img_checklist) as ImageView
            cardView = view.findViewById<View>(R.id.cardview) as CardView
            ll_corpo = view.findViewById<View>(R.id.ll_corpo) as ConstraintLayout
            ll_topo = view.findViewById<View>(R.id.ll_topo) as LinearLayout
            img_checklist.setOnClickListener(this)
            cardView.setOnClickListener(this)
            ll_corpo.setOnClickListener(this)

        }

        override fun onClick(v: View) {
            val atendimento = atendimentoList[adapterPosition]
            if (v === img_checklist) {
                direcionarCheckList(atendimento!!.idatd)
            } else if (v === ll_corpo) {
                val fragment = AtendimentoFragment()
                fragment.statusList = statusList
                // Passando o atendimento
                fragment.setAtendimento(atendimento!!)
                // Passando status
                fragment.setStatus(MetodoEmComum.busDescricaoStatus(statusList, atendimento.status))
                // Prepara intent para abrir o fragment
                val i = Intent(context, ActEdicao::class.java)
                ActEdicao.fragment = fragment
                ActEdicao.tituloFragmento = "Atendimento"
                context.startActivity(i)
            }
        }

        private fun direcionarCheckList(idAtendimento: Int) {
            val fragment = TabelaCheckListFragment()
            fragment.setIDAtendimento(idAtendimento)
            val i = Intent(context, ActEdicao::class.java)
            ActEdicao.fragment = fragment
            AtendimentosFragment.naoAtualizar = true
            ActEdicao.tituloFragmento = "Tabelas Checklists"
            context.startActivity(i)
        }
    }

    private class ProgressViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
        internal var progressBar: ProgressBar = v.findViewById<View>(R.id.progressfooter) as ProgressBar

    }

}
