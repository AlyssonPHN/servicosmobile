package eres.osmobile.com.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import eres.osmobile.com.R
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.model.Servico
import eres.osmobile.com.viewPagers.CadastroAtendimento


class Adapter_ServicosList(private val servicosList: ArrayList<Servico?>, private val podeAdicionar: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_ITEM = 1
    var mRecyclerViewOnClickListenerHack: RecyclerViewOnClickListenerHack? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var vh: RecyclerView.ViewHolder? = null
        when(viewType){
            VIEW_ITEM -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.servicos_adapter, parent, false)
                vh = MyViewHolder(view)
            }
            0 -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_progress, parent, false)
                vh = ProgressViewHolder(view)
            }
        }
        return vh!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            val servico = servicosList[position]
            // Checks if service is not null
            when (servico) {
                is Servico -> {
                    holder.txtDescricao.text = servico.descr_item
                    holder.txtCodigo.text = servico.identificacaoID.toString()
                    holder.txtValor.text = MetodoEmComum.formatoMoedasemCifrao(servico.valor)

                    adicionarRemoverServico(holder.ll_corpo, servico, holder.img_mais, holder.img_check)
                    marcarcomoSelecionado(servico, holder.img_mais, holder.img_check)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return servicosList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(servicosList[position] != null) VIEW_ITEM else 0
    }

    fun addItemList(servico: Servico?, position: Int){
        if (servico != null){
            servicosList.add(servico)
        }
        notifyItemInserted(position)
    }

    // Atualiza ProdutosFragments pela interface
    private fun atualizarSubTotal(position: Int) {
        if (mRecyclerViewOnClickListenerHack != null) {
            mRecyclerViewOnClickListenerHack!!.onAtualizar(position)
        }
    }

    inner class MyViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val ll_corpo: ConstraintLayout = mView.findViewById(R.id.ll_corpo)
        val img_mais: ImageView = mView.findViewById(R.id.img_mais)
        val img_check: ImageView = mView.findViewById(R.id.img_check)
        val txtDescricao: TextView = mView.findViewById(R.id.tituloChecklist)
        val txtCodigo: TextView = mView.findViewById(R.id.txtCodigo)
        val txtValor: TextView = mView.findViewById(R.id.txtValor)

    }

    private fun adicionarRemoverServico(ll_corpo: ConstraintLayout, servico: Servico?, img_add: ImageView, img_check: ImageView) {
        val rotateRigth = RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        val rotateLeft = RotateAnimation(0f, -360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotateRigth.duration = 300
        rotateLeft.duration = 300


        ll_corpo.setOnClickListener {

            if (img_add.visibility == View.VISIBLE) {
                // Rotate Right
                img_add.startAnimation(rotateRigth)
                rotateRigth.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {

                    }

                    override fun onAnimationEnd(animation: Animation) {
                        img_add.visibility = View.GONE
                        img_check.visibility = View.VISIBLE

                        // To add selected service
                        //If service already exists, upgrade; If it does not exist, add
                        adicionarEditarServico(servico)
                    }

                    override fun onAnimationRepeat(animation: Animation) {

                    }
                })
            } else {
                // Rotate Left
                img_check.startAnimation(rotateLeft)
                rotateLeft.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {

                    }

                    override fun onAnimationEnd(animation: Animation) {
                        img_check.visibility = View.GONE
                        img_add.visibility = View.VISIBLE

                        // To remove service
                        removerServico(servico)
                    }

                    override fun onAnimationRepeat(animation: Animation) {

                    }
                })
            }
        }
    }

    private fun marcarcomoSelecionado(servico: Servico?, img_add: ImageView, img_check: ImageView) {
        for (servico2 in CadastroAtendimento.servicosSelecionados!!) {
            if (servico!!.identificacaoID == servico2!!.identificacaoID) {
                img_add.visibility = View.GONE
                img_check.visibility = View.VISIBLE
            }
        }
    }

    private fun adicionarEditarServico(servico: Servico?) {
        var encontrou = false
        for (i in 0 until CadastroAtendimento.servicosSelecionados!!.size) {
            if (servico!!.identificacaoID == CadastroAtendimento.servicosSelecionados!![i]!!.identificacaoID) {
                CadastroAtendimento.servicosSelecionados!![i] = servico
                encontrou = true
                CadastroAtendimento.mudancaServico = true
            }
        }
        if (!encontrou) {
            CadastroAtendimento.servicosSelecionados!!.add(servico)
            CadastroAtendimento.mudancaServico = true
            atualizarSubTotal(0)
        }
    }

    private fun removerServico(servico: Servico?) {
        for (i in 0 until CadastroAtendimento.servicosSelecionados!!.size) {
            // Check if list has index
            if (i < CadastroAtendimento.servicosSelecionados!!.size) {
                // If index is found, remove from list
                if (servico!!.identificacaoID == CadastroAtendimento.servicosSelecionados!![i]!!.identificacaoID) {
                    CadastroAtendimento.servicosSelecionados!!.removeAt(i)
                    atualizarSubTotal(0)
                    CadastroAtendimento.mudancaServico = true
                    if (!podeAdicionar) {
                        notifyItemRemoved(i)
                    }
                }
            }
        }
    }

    internal class ProgressViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
        internal var progressBar: ProgressBar

        init {
            progressBar = v.findViewById<ProgressBar>(R.id.progressfooter)
        }
    }

}
