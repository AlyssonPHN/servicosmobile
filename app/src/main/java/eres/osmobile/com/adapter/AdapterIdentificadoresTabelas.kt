package eres.osmobile.com.adapter

import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.dialogs.DialogAssociarCheckList
import eres.osmobile.com.fragments.CheckListResponderFragment
import eres.osmobile.com.interfaces.AcoesLiteners
import eres.osmobile.com.model.IdentificadorTabelas
import eres.osmobile.com.model.TabelaCheckList


class AdapterIdentificadoresTabelas(private var context:Context, var acoesLiteners: AcoesLiteners, private val listIdentificadorTabelas: ArrayList<IdentificadorTabelas?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_ITEM = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var vh: RecyclerView.ViewHolder? = null
        when(viewType){
            VIEW_ITEM -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_tabelas_identificadores, parent, false)
                vh = MyViewHolder(view)
            }
            0 -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_progress, parent, false)
                vh = ProgressViewHolder(view)
            }
        }
        return vh!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder){
            val identificadorTabelas = listIdentificadorTabelas[position]

            when (identificadorTabelas){
                is IdentificadorTabelas -> {
                    holder.txtIdentificador.text = identificadorTabelas.identificador
                    holder.txttabelas.text = preencherTabelas(identificadorTabelas.tabelas)
                }

            }
        }
    }

    private fun preencherTabelas(arrayTabelaCheckList: ArrayList<TabelaCheckList>): String{
        var tabelas = ""

        for(tabelaChecklist in arrayTabelaCheckList){
            tabelas = if (tabelas.isEmpty())
                tabelaChecklist.descricao
            else
                "$tabelas; ${tabelaChecklist.descricao}"

        }
        return "$tabelas."
    }


    override fun getItemCount(): Int {
        return listIdentificadorTabelas.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (listIdentificadorTabelas[position] !=null) VIEW_ITEM else 0
    }


    fun addItemList(identificadorTabelas: IdentificadorTabelas?, position: Int){
        if (identificadorTabelas != null){
            listIdentificadorTabelas.add(identificadorTabelas)
        }
        notifyItemInserted(position)
    }

    inner class MyViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val txtIdentificador: TextView = mView.findViewById(R.id.txtIdentificador)
        val txttabelas: TextView = mView.findViewById(R.id.txttabelas)
        val ctl_corpo: ConstraintLayout = mView.findViewById(R.id.ctl_corpo)

        init {
            ctl_corpo.setOnClickListener{
                mostrarOpcoes(listIdentificadorTabelas[adapterPosition])
            }
        }

    }

    private fun mostrarOpcoes(identificadorTabelas: IdentificadorTabelas?){
        val items = arrayOf("Editar", "Vincular a Atendimento")
        val icons = arrayOf(R.drawable.ic_editcinza, R.drawable.ic_vinc_atendimento)
        val adapter = ArrayAdapterWithIcon(context, items, icons)

        val dialog:AlertDialog.Builder = AlertDialog.Builder(context)
        dialog.setTitle("Opções")
        dialog.setIcon(R.drawable.ic_circle_marcador)
        dialog.setAdapter(adapter, {
            _, item ->
            run {

                when (item) {
                    // Ao editar
                    0 -> abrirChecklistRespondidas(identificadorTabelas)
                    // Ao associar
                    1 -> {
                        val dialog = DialogAssociarCheckList(context, acoesLiteners, identificadorTabelas)
                        dialog.mostrar()
                    }
                }
            }
        })
        dialog.setNegativeButton("Cancelar", null)
        dialog.show()

    }

    private fun abrirChecklistRespondidas(identificadorTabelas: IdentificadorTabelas?){
        val fragment = CheckListResponderFragment()
        fragment.setSelecionados(identificadorTabelas!!.tabelas)
        fragment.setEstaEditando(identificadorTabelas.identificador)
        val i = Intent(context, ActEdicao::class.java)
        ActEdicao.fragment = fragment
        ActEdicao.tituloFragmento = "Chekcklist"
        context.startActivity(i)
    }

    private class ProgressViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
        internal var progressBar: ProgressBar

        init {
            progressBar = v.findViewById(R.id.progressfooter)
        }
    }
}