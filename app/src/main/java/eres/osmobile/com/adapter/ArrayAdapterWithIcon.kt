package eres.osmobile.com.adapter

import android.content.Context
import android.os.Build
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import eres.osmobile.com.R
import java.util.*

class ArrayAdapterWithIcon : ArrayAdapter<String> {

    private var images: List<Int>? = null

    constructor(context: Context, items: List<String>, images: List<Int>) : super(context, R.layout.custom_simple_list_1, items) {
        this.images = images
    }

    constructor(context: Context, items: Array<String>, images: Array<Int>) : super(context, R.layout.custom_simple_list_1, items) {
        this.images = Arrays.asList(*images)
    }

    constructor(context: Context, items: Int, images: Int) : super(context, R.layout.custom_simple_list_1, context.resources.getTextArray(items) as Array<String>) {

        val imgs = context.resources.obtainTypedArray(images)
        this.images = object : ArrayList<Int>() {
            init {
                for (i in 0 until imgs.length()) {
                    add(imgs.getResourceId(i, -1))
                }
            }
        }

        // recycle the array
        imgs.recycle()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)
        val textView = view.findViewById<View>(android.R.id.text1) as TextView

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(images!![position], 0, 0, 0)
        } else {
            textView.setCompoundDrawablesWithIntrinsicBounds(images!![position], 0, 0, 0)
        }
        textView.compoundDrawablePadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12f, context.resources.displayMetrics).toInt()
        return view
    }

}
