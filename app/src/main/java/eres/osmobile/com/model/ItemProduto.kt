package eres.osmobile.com.model


class ItemProduto(var atendimentoId: Int = 0,
                  var produtoID: Int = 0,
                  var pedidoID: Int = 0,
                  var itemID: Int = 0,
                  var descricao: String = "",
                  var unidade: String = "",
                  var quantidade: Double = 0.0,
                  var preco: Double = 0.0,
                  var desconto: Double = 0.0,
                  var acrescimo: Double = 0.0) : Produto()
