package eres.osmobile.com.model

open class Produto (var IDpro: Int = 0,
                    var xdesc: String ="",
                    var xunid: String ="",
                    var IDsec: Int = 0,
                    var IDgrp: Int = 0,
                    var IDsgp: Int = 0,
                    var ref: String = "",
                    var SDO: Double = 0.0,
                    var prc: Double = 0.0,
                    var qtd_min: Double = 0.0,
                    var qtd_emb: Double = 0.0,
                    var IDcom: Int = 0,
                    var IDfpc: Int = 0,
                    var chv_cfg_fpc: Int = 0,
                    var prc_min: Double = 0.0,
                    var prc_sug: Double = 0.0,
                    var prc_com: Double = 0.0,
                    var prc_max: Double = 0.0)







