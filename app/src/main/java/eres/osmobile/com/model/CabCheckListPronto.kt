package eres.osmobile.com.model

import com.thoughtbot.expandablecheckrecyclerview.models.MultiCheckExpandableGroup
import java.util.*

class CabCheckListPronto(var tabela: String, var checklist: ArrayList<CheckList_Resposta>, var _id: Int = 0, var descricao: String = "") : MultiCheckExpandableGroup(tabela, checklist)