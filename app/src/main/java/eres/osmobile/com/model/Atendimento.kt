package eres.osmobile.com.model


class Atendimento() {

    var idatd: Int = 0
    var status: Int = 0
    var problema: String? = null
    var valorTotal: Double = 0.toDouble()
    var valorAcrescimo: Double = 0.toDouble()
    var valorDesconto: Double = 0.toDouble()
    var dataPrevisao: String? = null
    var dataEntrada: String? = null
    var identificador: String? = null
    var usuario: String? = null
    var clienteID: Int = 0
    var produto: String? = null
    var valorEstimado: Double = 0.toDouble()
    var nome: String? = null
    var nomeFantasia: String? = null
    var cnpj_cpf: String? = null
}
