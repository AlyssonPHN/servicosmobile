package eres.osmobile.com.model

class Cliente {

    var IDcli: Int? = 0
    var nome: String? = null
    var xfant: String? = null
    var xie_rg: String? = null
    var indIE: Int? = null
    var xcnpj_cpf: String? = null
    var xend: String? = null
    var num: String? = null
    var xbai: String? = null
    var cep: String? = null
    var xcompl: String? = null
    var xcid: String? = null
    var uf: String? = null
    var sit: Int = 0
    var xsit: String? = null
    var iDfpc: Int = 0
    var iDcom: Int = 0
    var iDven: Int = 0
    var banco: String? = null
    var agencia: String? = null
    var conta_banco: String? = null
}
