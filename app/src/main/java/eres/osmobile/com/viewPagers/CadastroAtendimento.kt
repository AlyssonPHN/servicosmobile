package eres.osmobile.com.viewPagers

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.*
import android.widget.Toast
import com.android.volley.Request
import com.google.gson.Gson
import eres.osmobile.com.R
import eres.osmobile.com.controller.Mensagem
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.controller.Progressos
import eres.osmobile.com.controller.ReturnException
import eres.osmobile.com.fragments.DadosAtendimentoFragment
import eres.osmobile.com.fragments.ProdutosAdicionadosFragment
import eres.osmobile.com.fragments.ServicosAdicionadosFragment
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.interfaces.VolleyResponseListener
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.VolleyRequest
import eres.osmobile.com.json.WebServiceCliente
import eres.osmobile.com.model.Atendimento
import eres.osmobile.com.model.ItemProduto
import eres.osmobile.com.model.Servico
import eres.osmobile.com.model.Status
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class CadastroAtendimento : Fragment(), RecyclerViewOnClickListenerHack, VolleyResponseListener {

    // companion object - Everything inside it will be static
    companion object {
        var servicosSelecionados: ArrayList<Servico?>? = null
        var itensProdutoSelecionados: ArrayList<ItemProduto>? = null
        var idAtendimento = 0
        var atendimento: Atendimento? = null
        var mudancaAtendimento = false
        var mudancaServico = false
        var mudancaItens = false
        var salvo = false
    }

    var fragmentsList: ArrayList<Fragment> = ArrayList()
    var statusList: List<Status>? = null
    var mensagem: String = ""
    private val tabTitles = arrayOf("Dados Iniciais", "Serviços", "Produtos")

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    /**
     * The [ViewPager] that will host the section contents.
     */
    private var mViewPager: ViewPager? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // To activate the top menu
        setHasOptionsMenu(true)
        return inflater!!.inflate(R.layout.viewpager_cadastro_atendimento, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //Initializing array
        servicosSelecionados = ArrayList()
        addDadosAtendimento()

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)

        // Set up the ViewPager with the sections adapter.
        mViewPager = view!!.findViewById(R.id.container)
        mViewPager!!.adapter = mSectionsPagerAdapter


        val tabs = MetodosemComum.tabLayout
        tabs!!.visibility = View.VISIBLE
        tabs.setupWithViewPager(mViewPager)

        if (atendimento != null) {
            idAtendimento = atendimento!!.idatd
            addFragmentsRestantes()
            buscarItensProdutos()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        servicosSelecionados = null
        itensProdutoSelecionados = null
        idAtendimento = 0
        atendimento = null
        mudancaAtendimento = false
        mudancaServico = false
        mudancaItens = false
        salvo = false
    }

    override fun onDetach() {
        val fragment: Fragment = mSectionsPagerAdapter!!.getItem(mViewPager!!.currentItem)
        fragment.onDetach()
        super.onDetach()
    }

    private fun addDadosAtendimento() {
        val fragments = DadosAtendimentoFragment()
        fragments.statusList = statusList
        fragments.mRecyclerViewOnClickListenerHack = this@CadastroAtendimento

        fragmentsList.add(fragments)
    }

    private fun addFragmentsRestantes() {
        if (fragmentsList.size == 1) {
            val fragmentServico = ServicosAdicionadosFragment()
            fragmentServico.mRecyclerViewOnClickListenerHack = this@CadastroAtendimento
            fragmentsList.add(fragmentServico)

            val fragmentProduto = ProdutosAdicionadosFragment()
            fragmentProduto.mRecyclerViewOnClickListenerHack = this@CadastroAtendimento
            fragmentsList.add(fragmentProduto)

            mSectionsPagerAdapter!!.notifyDataSetChanged()
        }
    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment =// getItem is called to instantiate the fragment for the given page.
                // Return a PlaceholderFragment (defined as a static inner class below).
                //return PlaceholderFragment.newInstance(position + 1)
                fragmentsList[position]

        override fun getCount(): Int =// Show total pages.
                fragmentsList.size

        override fun getPageTitle(position: Int): CharSequence = tabTitles[position]

    }

    @SuppressLint("ShowToast")
    private fun buscarItensProdutos() {

        // Se tiver atendimento existente
        doAsync {
            var resposta = kotlin.arrayOfNulls<String>(2)
            try {
                resposta = WebServiceCliente(context)["TOrdemServico/\"ItensAtendimento\"/$idAtendimento/1"]
                itensProdutoSelecionados = RespostaJson.getRespostaItensProduto(resposta[1] as String)
                if (itensProdutoSelecionados!!.size == 0) {
                    itensProdutoSelecionados = null
                }
                if (resposta[0] == "0") {
                    throw Exception(resposta[1])
                }

            } catch (e: Exception) {

            }
            uiThread {
                if (resposta[0] == "0") {
                    Toast.makeText(context, "Produtos não carregados", Toast.LENGTH_SHORT)
                }
            }
        }
    }

    private val idAtendimentoZero = idAtendimento == 0
    private fun enviaAntendimento() {
        when {
            atendimento != null && mudancaAtendimento -> {
                salvo = true
                val objeto: String = Gson().toJson(atendimento)

                VolleyRequest.stringRequest(context, Request.Method.POST, "TOrdemServico/\"InsertAtendimento\"",
                        objeto, this, "enviaAntendimento")
            }
            else -> {
                enviaServicos()
            }
        }
    }

    private fun respostaAtendimento(resposta: String) {
        idAtendimento = RespostaJson.getRespostaAendimento(resposta)
        atendimento!!.idatd = idAtendimento

        mudancaAtendimento = false
        if (idAtendimentoZero) {
            Mensagem.exibeMensagem(context, "Sucesso!", "Enviado com sucesso!\nID: ${CadastroAtendimento.idAtendimento}", true)
            addFragmentsRestantes()
            Progressos.fecharProgress()
        } else {
            mensagem = "Enviado com sucesso."
        }

        // Se estiver criando atendimento, então não enviar serviços (apenas envia se tiver atendimento criado)
        if (!idAtendimentoZero) {
            enviaServicos()
        }
    }

    private fun respostaErroAtendimento(resposta: String) {
        if (idAtendimentoZero) {
            Mensagem.exibeMensagem(context, "Erro!", "Erro ao enviar Atendimento!\n" +
                    "${ReturnException.mostraErroConexaoGeral(resposta)}\n", false)
        } else {
            mensagem = "Erro ao enviar Atendimento!${ReturnException.mostraErroConexaoGeral(resposta)}"
        }

        if (!idAtendimentoZero) {
            enviaServicos()
        }
        Progressos.fecharProgress()
    }

    private fun enviaServicos() {
        when {
            mudancaServico && (servicosSelecionados != null || servicosSelecionados!!.isNotEmpty()) -> {
                salvo = true
                val objeto = Gson().toJson(servicosSelecionados)
                VolleyRequest.stringRequest(context, Request.Method.POST,
                        "TOrdemServico/\"AdicionaServicoAtdm\"/${CadastroAtendimento.idAtendimento}",
                        objeto, this, "enviaServicos")
            }
            else -> {
                enviaItens()
            }
        }
    }

    private fun respostaServicos() {

        if (!mensagem.contains("Erro")) {
            mensagem = "Enviado com sucesso!"
        }
        enviaItens()
    }

    private fun respostaErroServicos(message: String) {
        if (!mensagem.contains("Erro")) {
            mensagem = "Erro ao enviar Serviço!\n$message"
        }
        enviaItens()
    }


    private fun enviaItens() {
        when {
            mudancaItens && (itensProdutoSelecionados != null) -> {
                salvo = true
                val objeto = Gson().toJson(itensProdutoSelecionados)
                VolleyRequest.stringRequest(context, Request.Method.POST,
                        "TOrdemServico/\"AdicionaProdutoAtdm\"/${CadastroAtendimento.idAtendimento}",
                        objeto, this, "enviaItens")
            }
            else -> {
                respostaItens()
            }
        }

    }

    private fun respostaItens() {
        Progressos.fecharProgress()
        if (mudancaServico || mudancaItens) {
            if (!mensagem.contains("Erro")) {
                mensagem = "Enviado com sucesso!"
                mudancaItens = false
                mudancaServico = false
                Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show()
                mensagem = ""
            } else {
                Mensagem.exibeMensagem(context, "Erro!", "${ReturnException.mostraErroConexaoGeral(mensagem)}\n", false)
                mensagem = ""
            }
        }
    }

    private fun respostaErroItens(message: String) {
        Progressos.fecharProgress()
        if (!mensagem.contains("Erro")) {
            mensagem = "Erro ao enviar Itens!\n$message"
        }
        Mensagem.exibeMensagem(context, "Erro!", mensagem, false)
        mensagem = ""
    }

    private fun enviarTudoJunto() {
        Progressos.criaProgressSemInterromper(context, "Aguarde!", "Enviando...")
        enviaAntendimento()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_save, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_save -> {
                //enviarAtendimento()

            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResponse(response: Any, tag: String) {
        when (tag) {
            "enviaAntendimento" -> {
                respostaAtendimento(response.toString())
            }
            "enviaServicos" -> {
                respostaServicos()
            }
            "enviaItens" -> {
                respostaItens()
            }
        }
    }

    override fun onError(message: String, tag: String) {
        when (tag) {
            "enviaAntendimento" -> {
                respostaErroAtendimento(message)
            }
            "enviaServicos" -> {
                respostaErroServicos(message)
            }
            "enviaItens" -> {
                respostaErroItens(message)
            }
        }
    }

    override fun onClickListenerRv(view: View, position: Int) {
    }

    override fun onExcluir(view: View?, position: Int) {
    }

    override fun onAdicionar(posicao: Int) {
        addFragmentsRestantes()
    }

    override fun onAtualizar(posicao: Int) {
    }

    override fun onAlterar() {
        enviarTudoJunto()
    }
}
