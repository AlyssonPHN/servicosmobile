package eres.osmobile.com.interfaces

interface VolleyResponseListener {

    fun onResponse(response: Any, tag: String)

    fun onError(message: String, tag: String)
}
