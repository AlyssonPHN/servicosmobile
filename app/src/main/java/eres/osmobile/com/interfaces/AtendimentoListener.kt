package eres.osmobile.com.interfaces

import eres.osmobile.com.model.Atendimento

/**
 * Interface para atualizar Atendimento
 */

interface AtendimentoListener {

    fun onAtendimento(position: Int, atendimento: Atendimento?)
}
