package eres.osmobile.com.interfaces

import android.view.View

interface RecyclerViewOnClickListenerHack {

    fun onClickListenerRv(view: View, position: Int)

    fun onExcluir(view: View?, position: Int)

    fun onAdicionar(posicao: Int)

    fun onAtualizar(posicao: Int)

    fun onAlterar()
}
