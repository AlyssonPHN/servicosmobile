package eres.osmobile.com.json

import android.content.Context
import android.text.TextUtils

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

object VolleyController {

    private val TAG = VolleyController::class.java.simpleName

    private var mRequestQueue: RequestQueue? = null


    @Synchronized private fun getRequestQueue(context: Context): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context)
        }

        return mRequestQueue
    }

    @Synchronized
    fun <T> addToRequestQueue(context: Context, req: Request<T>, tag: String) {
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        getRequestQueue(context)!!.add(req)
        getRequestQueue(context)!!.cache.clear()
    }

    @Synchronized
    fun <T> addToRequestQueue(context: Context, req: Request<T>) {
        req.tag = TAG
        getRequestQueue(context)!!.add(req)
    }

    @Synchronized
    fun cancelPendingRequests(tag: Any) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }
}