package eres.osmobile.com.json

import com.google.gson.Gson
import eres.osmobile.com.controller.ReturnException
import eres.osmobile.com.model.*
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class RespostaJson {

    companion object {
        //SESSÃO
        @Throws(JSONException::class)
        fun getSessao(resposta: String?, senha: String): Sessao {
            var sessao: Sessao? = null
            val array = JSONArray(resposta)
            val gson = Gson()

            try {
                //Pegando a json em string
                val json = array.getString(0)
                //Convertendo diretamente de Json sessao
                sessao = gson.fromJson(json, Sessao::class.java)
                //Coloca o ID
                sessao!!.idsessao = 1
                //Colocando a data
                val data = SimpleDateFormat("dd/MM/yyyy", Locale.US)
                val dataAtual = data.format(Date(System.currentTimeMillis()))
                //Coloca a data
                sessao.data = dataAtual
                sessao.senha = senha

            } catch (ignored: Exception) {

            }

            return sessao!!
        }

        // ATENDIMENTO
        @Throws(JSONException::class)
        fun getListAtendimento(resposta: String?): ArrayList<Atendimento?> {
            val atendimentoList = ArrayList<Atendimento?>()

            //Iniciando array de Json
            val array = JSONArray(resposta)
            val gson = Gson()

            // Caminhando pelo array Json
            // Criando objetos Json
            for (i in 0 until array.length()) {
                try {
                    //Pegando a json em string
                    val json = array.getString(i)
                    //Convertendo diretamente para Json
                    val atendimento = gson.fromJson(json, Atendimento::class.java)
                    //Adicionando cliente no array
                    atendimentoList.add(atendimento)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }
            return atendimentoList
        }

        // STATUS
        @Throws(JSONException::class)
        fun getLitStatus(resposta: String): List<Status> {
            val statusList = ArrayList<Status>()
            val gson = Gson()
            // Pegando a json em string
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        //Criando objetos Json e Cliente
                        array.getString(it)

                        //Convertendo diretamente de Json para objeto
                    }
                    .mapTo(statusList) { gson.fromJson(it, Status::class.java) }

            return statusList
        }

        // CLIENTE
        @Throws(JSONException::class)
        fun getListCliente(resposta: String): List<Cliente> {
            val clienteList = ArrayList<Cliente>()
            val gson = Gson()
            //Pegando o json em string
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Criando objetos Json e Cliente
                        array.getString(it)

                        // Convertendo diretamente de Json para objeto
                    }
                    .mapTo(clienteList) { gson.fromJson(it, Cliente::class.java) }
            return clienteList
        }

        @Throws(JSONException::class)
        fun getListServicos(resposta: String?): ArrayList<Servico?> {
            val servicoList = ArrayList<Servico?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Getting object json in String
                        array.getString(it)

                        // Converting directly from Json to object
                    }
                    .mapTo(servicoList) { gson.fromJson(it, Servico::class.java) }
            return servicoList
        }

        @Throws(JSONException::class)
        fun getListProdutos(resposta: String?): ArrayList<Produto?> {
            val produtoList = ArrayList<Produto?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Getting object json in String
                        array.getString(it)

                        // Converting directly from Json to object
                    }
                    .mapTo(produtoList) { gson.fromJson(it, Produto::class.java) }
            return produtoList
        }

        @Throws(JSONException::class, IOException::class, ReturnException::class)
        fun getListSecoes(resposta: String?): ArrayList<Secao?> {
            val secoesList = ArrayList<Secao?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Getting object json in String
                        array.getString(it)
                    }
                    // Converting directly from Json to object and add
                    .mapTo(secoesList) { gson.fromJson(it, Secao::class.java) }
            return secoesList
        }

        @Throws(JSONException::class, IOException::class, ReturnException::class)
        fun getListGrupos(resposta: String?): ArrayList<Grupo?> {
            val gruposList = ArrayList<Grupo?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Getting object json in String
                        array.getString(it)
                    }
                    // Converting directly from Json to object and add
                    .mapTo(gruposList) { gson.fromJson(it, Grupo::class.java) }
            return gruposList
        }

        @Throws(JSONException::class, IOException::class, ReturnException::class)
        fun getListSubGrupos(resposta: String?): ArrayList<SubGrupo?> {
            val subgruposList = ArrayList<SubGrupo?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Getting object json in String
                        array.getString(it)
                    }
                    // Converting directly from Json to object and add
                    .mapTo(subgruposList) { gson.fromJson(it, SubGrupo::class.java) }
            return subgruposList
        }

        @Throws(JSONException::class, IOException::class, ReturnException::class)
        fun getRespostaAendimento(resposta: String): Int {
            var idAtendimento = 0
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map { array.getJSONObject(it) }
                    .forEach {
                        try {
                            idAtendimento = it.getInt("idatdm")
                        } catch (e: Exception) {
                            // Se der erro é que não tem Id do atendimento
                            // Então enviar o erro
                            throw Exception(it.getString("error"))
                        }
                    }

            return idAtendimento
        }

        @Throws(JSONException::class, IOException::class, ReturnException::class)
        fun getRespostaItensProduto(resposta: String): ArrayList<ItemProduto> {
            val itemProdutoList = ArrayList<ItemProduto>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Getting object json in String
                        array.getString(it)

                        // Converting directly from Json to object
                    }
                    .mapTo(itemProdutoList) { gson.fromJson(it, ItemProduto::class.java) }
            return itemProdutoList
        }

        fun getListTabelaCheckList(resposta: String): ArrayList<TabelaCheckList?> {
            val listaTabelaCheckList = arrayListOf<TabelaCheckList?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length())
                    .map {
                        // Getting object json in String
                        array.getString(it)
                        // Converting directly from Json to object
                    }
                    .mapTo(listaTabelaCheckList) {
                        // Adds an object inside the array
                        gson.fromJson(it, TabelaCheckList::class.java)
                    }
            return listaTabelaCheckList
        }

        fun getListaCheckList(resposta: String, selecionados: ArrayList<TabelaCheckList>): ArrayList<CabCheckListPronto?> {
            val listaCheckList = arrayListOf<CabCheckListPronto?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length()).map { array.getString(it) }.mapTo(listaCheckList) {
                val cabCheckList = gson.fromJson(it, CabCheckList::class.java)
                val checklist = CabCheckListPronto(cabCheckList.tabela, cabCheckList.checklist)
                (0 until selecionados.size).map {
                    if (selecionados[it].tabela == cabCheckList.tabela) {
                        checklist._id = selecionados[it]._id
                        checklist.descricao = selecionados[it].descricao
                    }
                }
                checklist
            }
            return listaCheckList
        }

        fun getListIdentificadorTabelas(resposta: String): ArrayList<IdentificadorTabelas?> {
            val listaIdentificadorCheckList = arrayListOf<IdentificadorTabelas?>()
            val gson = Gson()
            // Getting array json
            val array = JSONArray(resposta)
            (0 until array.length()).map {
                // Getting object json in String
                array.getString(it)
            }.mapTo(listaIdentificadorCheckList) {
                // Adds an object inside the array
                gson.fromJson(it, IdentificadorTabelas::class.java)
            }
            return listaIdentificadorCheckList
        }

    }
}
