package eres.osmobile.com.json


import android.content.Context
import android.util.Base64
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.StringRequest
import eres.osmobile.com.controller.Start
import eres.osmobile.com.interfaces.VolleyResponseListener
import java.io.UnsupportedEncodingException
import java.util.*


object VolleyRequest {

    //Tenta buscar a cada 3 segundo no total de 10 tentativas
    private val socketTimeout = 3000//30 seconds - change to what you want
    private val policy = DefaultRetryPolicy(socketTimeout, 10, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

    fun stringRequest(context: Context, method: Int, url: String, requestBody: String?, listener: VolleyResponseListener, tag: String) {
        val novaURL = Start.getURL() + url
        Log.i("Requisição", novaURL)
        val stringRequest = object : StringRequest(method, novaURL,
                Response.Listener { response ->
                    listener.onResponse(response, tag)
                    Log.i("resposta", response.toString())
                },
                Response.ErrorListener { error ->
                    listener.onError(error.toString(), tag)
                    Log.i("erro resposta", error.toString())
                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "text/plain; charset=utf-8")
                val auth = "Basic " + Base64.encodeToString(Start.CREDENCIAIS.toByteArray(), Base64.NO_WRAP)
                headers.put("Authorization", auth)
                headers.put("Accept", "application/JSON")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray? {
                return try {
                    //return requestBody?.toByteArray(charset("utf-8"))
                    requestBody?.toByteArray(charset("utf-8"))
                } catch (uee: UnsupportedEncodingException) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8")
                    null
                }

            }

        }

        stringRequest.retryPolicy = policy
        VolleyController.addToRequestQueue(context, stringRequest, tag)

    }

}
