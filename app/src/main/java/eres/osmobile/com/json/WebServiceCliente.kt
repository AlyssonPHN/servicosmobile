package eres.osmobile.com.json

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import android.util.Log

import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URISyntaxException
import java.net.URL

import eres.osmobile.com.controller.ReturnException
import eres.osmobile.com.controller.Start
import eres.osmobile.com.fragments.ConfiguracaoFragment

/**
 * Responsável por fazer conexão com o web service
 */
class WebServiceCliente(context: Context?) {
    private val JSON_CONNECTION_TIMEOUT = 3000
    private val JSON_SOCKET_TIMEOUT = 5000
    internal lateinit var sharedpreferences: SharedPreferences

    init {
        try {
            //Para preencher as a autenticação
            if (context != null) {
                preencherAutenticao(context)
            }
            //Pega o host e a porta, salvo no sharedpreference
            sharedpreferences = context!!.getSharedPreferences(ConfiguracaoFragment.WEB_SERVICE, Context.MODE_PRIVATE)
            val HOST = sharedpreferences.getString("caminho", null)
            val PORTA = sharedpreferences.getString("porta", null)
            //As credenciais é preenchida pelo SharedPrefence Temporário
            CREDENCIAIS = USUARIO + ":" + SENHA
            CAMINHO = "http://$HOST:$PORTA"
            sURL = CAMINHO + "/Datasnap/rest/"

        } catch (ignored: Exception) {

        }

    }

    private fun preencherAutenticao(context: Context) {
        val sharedpreferences = context.getSharedPreferences("LoginTemporario", Context.MODE_PRIVATE)
        USUARIO = sharedpreferences.getString("LNome", null)
        SENHA = sharedpreferences.getString("LSenha", null)
    }

    operator fun get(metodo: String): Array<String?> {
        val resposta = arrayOfNulls<String>(2)
        var connection: HttpURLConnection? = null
        try {
            val url = URL(Start.getURL() + metodo)
            connection = iniciaConnection(url)
            //Método GET
            connection.requestMethod = "GET"
            connection.doInput = true
            connection.connect()
            Log.i("Protocolo", url.protocol)
            Log.i("Protocolo", sURL + metodo)
            resposta[0] = "200"
            resposta[1] = respostaWebService(connection)
            //connection.disconnect();
        } catch (e: Exception) {
            //connection.disconnect();
            e.printStackTrace()
            resposta[0] = "0"
            resposta[1] = ReturnException.mostraErroConexaoGeral(e.message!!)
            Log.e("Erro de conexão", "Connection Error !!! - " + e.toString())
        }

        return resposta
    }

    @Throws(IOException::class, URISyntaxException::class)
    fun post(metodo: String, json: String): Array<String?> {
        val resposta = arrayOfNulls<String>(2)
        try {
            val url = URL(Start.getURL() + metodo)
            Log.d("POST", sURL + metodo)
            Log.d("POST", json)
            val connection = iniciaConnection(url)
            //Método POST
            connection.requestMethod = "POST"
            connection.doOutput = true
            connection.connect()
            val wr = OutputStreamWriter(connection.outputStream)
            wr.write(json)
            wr.flush()
            wr.close()
            resposta[1] = respostaWebService(connection)
            resposta[0] = "200"
            connection.disconnect()
        } catch (e: Exception) {
            e.printStackTrace()
            resposta[0] = "0"
            resposta[1] = ReturnException.mostraErroConexaoGeral(e.message!!)
            Log.e("Erro de conexão", "Connection Error !!! - " + e.toString())
        }

        return resposta
    }

    fun testeConexao(metodo: String): Array<String?> {
        val resposta = arrayOfNulls<String>(2)
        try {
            //use a proper url instead of onlineUrl
            val url = URL(Start.getURL() + metodo)
            val connection = iniciaConnection(url)
            //Método GET
            connection.requestMethod = "GET"
            connection.doInput = true
            connection.connect()
            resposta[1] = respostaWebService(connection)
            resposta[0] = "200"

        } catch (e: FileNotFoundException) {
            resposta[0] = "200"
            resposta[1] = "Conectado"
        } catch (e: Exception) {
            e.printStackTrace()
            resposta[0] = "0"
            resposta[1] = "Falha de rede!"
            Log.e("Erro de conexão", "Connection Error !!! - " + e.toString())
        }

        return resposta
    }

    @Throws(IOException::class)
    private fun iniciaConnection(url: URL): HttpURLConnection {
        connection = url.openConnection() as HttpURLConnection
        val credentials = Start.CREDENCIAIS
        connection!!.setRequestProperty("Content-Type", "text/plain; charset=utf-8")
        connection!!.setRequestProperty("Accept", "application/JSON")
        val base64EncodedCredentials = Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
        val basicauth = "Basic " + base64EncodedCredentials
        connection!!.setRequestProperty("Authorization", basicauth)
        connection!!.useCaches = false
        connection!!.instanceFollowRedirects = false
        connection!!.connectTimeout = JSON_CONNECTION_TIMEOUT
        connection!!.readTimeout = JSON_SOCKET_TIMEOUT
        return connection as HttpURLConnection
    }

    @Throws(IOException::class)
    private fun respostaWebService(connection: HttpURLConnection): String {
        val headerFields = connection.headerFields
        Log.w("header", headerFields.toString())
        Log.w("Response Mensagem", connection.responseMessage)
        //Log.w("Response Code", String.valueOf(connection.getResponseCode()));

        val inputStream = BufferedInputStream(connection.inputStream)
        val bufferedReader = BufferedReader(InputStreamReader(inputStream))

        var line: String? = null
        val result = StringBuilder()
        while ({ line = bufferedReader.readLine(); line }() != null) {
            result.append(line)
        }
        inputStream.close()
        bufferedReader.close()

        return result.toString()
    }


    internal fun obterEndereco(metodo: String): String? {
        try {
            val url = URL(metodo)
            val connection = iniciaConnection(url)
            //Método GET
            connection.requestMethod = "GET"
            connection.doInput = true
            connection.connect()
            return respostaWebService(connection)
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    companion object {
        private var sURL = ""
        private var USUARIO: String? = ""
        private var SENHA: String? = ""
        private var CAMINHO = ""
        private var CREDENCIAIS: String? = null
        private var connection: HttpURLConnection? = null
    }
}
