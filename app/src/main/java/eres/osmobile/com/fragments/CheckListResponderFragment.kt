package eres.osmobile.com.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.android.volley.Request
import com.google.gson.Gson
import eres.osmobile.com.R
import eres.osmobile.com.adapter.Adapter_CheckList_ResponderNovo
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.dialogs.DialogSalvarCheckList
import eres.osmobile.com.interfaces.AcoesLiteners
import eres.osmobile.com.interfaces.VolleyResponseListener
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.VolleyRequest
import eres.osmobile.com.model.CabCheckListEnviar
import eres.osmobile.com.model.CabCheckListPronto
import eres.osmobile.com.model.CheckList_RespostaEnviar
import eres.osmobile.com.model.TabelaCheckList
import kotlinx.android.synthetic.main.content_geral.*


/**
 * A fragment representing a list of Items.
 */
class CheckListResponderFragment : Fragment(), VolleyResponseListener, AcoesLiteners {

    private var adapter: Adapter_CheckList_ResponderNovo? = null
    private var mSelecionadas: ArrayList<TabelaCheckList> = arrayListOf()
    private var mChecklists: ArrayList<CabCheckListPronto?>? = null
    private var scroll: RecyclerView.OnScrollListener? = null
    private var identificadordeEdicao = ""
    private var idAtendimento: Int = 0

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(context)
        scrollInfinito()
        recyclerView.recycledViewPool.setMaxRecycledViews(1, 0)
        recyclerView.addOnScrollListener(scroll)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // To activate the top menu
        setHasOptionsMenu(true)
        return inflater!!.inflate(R.layout.fragment_checklist_responder, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buscarCheckListSelecionadas()
    }

    override fun onResume() {
        super.onResume()
        MetodosemComum.funcionarOnBackPressEdicao()
    }

    private fun scrollInfinito() {
        scroll = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(mRecyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(mRecyclerView, dx, dy)
                // Esconder teclado ao mover scroll
                if (dy > 0 || dy < 0) {
                    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                }
            }
        }
    }

    private fun mostrarCheckListsCompleta(checklist: ArrayList<CabCheckListPronto?>?) {
        ll_carregando.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        adapter = Adapter_CheckList_ResponderNovo(context, checklist!!)
        recyclerView.adapter = adapter
        expandirGeral()
    }

    private fun expandirGeral() {
        for (i in adapter!!.groups.size - 1 downTo 0) {
            expandGroup(i)
        }
    }

    private fun expandGroup(gPos: Int) {
        if (adapter!!.isGroupExpanded(gPos)) {
            return
        }
        adapter!!.toggleGroup(gPos)
    }

    fun setSelecionados(arrayList: ArrayList<TabelaCheckList>) {
        mSelecionadas = arrayList
    }

    fun setEstaEditando(identificador: String) {
        identificadordeEdicao = identificador
    }

    fun setIDAtendimento(atendimentoID: Int) {
        idAtendimento = atendimentoID
    }

    private fun buscarCheckListSelecionadas() {
        val arrayTabelas: ArrayList<Tabela> = arrayListOf()
        // Adicionando tabelas no array
        (0 until mSelecionadas.size).mapTo(arrayTabelas) { Tabela(mSelecionadas[it].tabela) }
        val arrayJson: String = Gson().toJson(arrayTabelas)
        // Ao passar a url verifico a condição se identificador de Edição é diferente de vazio
        // Se for vazio busco checklist para preencher
        // Se não for vazia então eu busco checklist respondidas para edição
        VolleyRequest.stringRequest(context, Request.Method.POST,
                if (identificadordeEdicao.isEmpty()
                        && idAtendimento == 0) "TOrdemServico/\"BuscarCheckListSelecionada\""
                else "TOrdemServico/\"BuscarCheckListsSelecionadasPreenchida\"/$identificadordeEdicao/$idAtendimento",
                arrayJson, this, "buscarCheckListSelecionadas")
    }

    private fun respostasCheckListSelecionadas(response: String) {
        mChecklists = RespostaJson.getListaCheckList(response, mSelecionadas)
        if (mChecklists!!.size > 0) {
            mostrarCheckListsCompleta(mChecklists)
        }
    }

    internal class Tabela(var tabela: String)


    private fun adicionarCamposObrigatorios(cabecalho: CabCheckListPronto?, cabecalhoEnviar: CabCheckListEnviar) {
        (0 until mSelecionadas.size)
                .filter { cabecalho!!.tabela == mSelecionadas[it].tabela }
                .forEach { cabecalhoEnviar.checklist.add(CheckList_RespostaEnviar("FK_CHKID", cabecalho!!._id)) }
        cabecalhoEnviar.checklist.add(CheckList_RespostaEnviar("ATDMID", 1))
        // 1 - de preenchido
        cabecalhoEnviar.checklist.add(CheckList_RespostaEnviar("FLAG", 1))
        cabecalhoEnviar.checklist.add(CheckList_RespostaEnviar("IDENTIFICADOR", "testando"))
    }

    override fun onResponse(response: Any, tag: String) {
        when (tag) {
            "buscarCheckListSelecionadas" -> {
                respostasCheckListSelecionadas(response.toString())
            }
            "salvarCheckList" -> {
                Toast.makeText(context, "Enviado", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onError(message: String, tag: String) {
        when (tag) {
            "buscarCheckListSelecionadas" -> {

            }
        }
    }

    override fun fecharActivity() {
        activity.finish()
    }

    override fun onResumeInterface() {}

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_save, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_save -> {
                if (verificarCamposEmBranco()) {
                    val dialogSalvar = DialogSalvarCheckList(context, this, mChecklists, mSelecionadas, identificadordeEdicao, idAtendimento)
                    dialogSalvar.mostrar()
                } else {
                    Toast.makeText(context, "Campo(s) vazio(s)!", Toast.LENGTH_SHORT).show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun verificarCamposEmBranco(): Boolean {

        var tamanho = 0
        val posicoesPendentes: ArrayList<Int> = arrayListOf()
        (0 until mChecklists!!.size).map {
            // Conta a cabeça de cada lista
            tamanho += 1
            for (i in 0 until mChecklists!![it]!!.checklist.size) {
                // Conta cada pergunta da lista
                tamanho += 1
                val valor = mChecklists!![it]!!.checklist[i].prowess
                if (valor == null || valor == "") {
                    posicoesPendentes.add(tamanho)
                }
            }

        }

        adapter!!.posicoesPendentes = posicoesPendentes

        (0 until posicoesPendentes.size).filter { posicoesPendentes.isNotEmpty() }
                .forEach {
                    adapter!!.notifyItemChanged(posicoesPendentes[it] - 1)
                }

        if (posicoesPendentes.isNotEmpty()) {
            // Ir para primeira posição onde o campo estiver vazio
            recyclerView.scrollToPosition(posicoesPendentes[0] - 1)
        }
        return posicoesPendentes.isEmpty()
    }


}