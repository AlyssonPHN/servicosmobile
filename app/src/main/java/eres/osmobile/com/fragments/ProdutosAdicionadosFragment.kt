package eres.osmobile.com.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.Toast
import com.google.gson.Gson
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.adapter.Adapter_Produtos
import eres.osmobile.com.controller.Mensagem
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.controller.Progressos
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.WebServiceCliente
import eres.osmobile.com.model.Produto
import eres.osmobile.com.viewPagers.CadastroAtendimento
import kotlinx.android.synthetic.main.content_geral.*
import kotlinx.android.synthetic.main.fragment_produtos_adicionados.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class ProdutosAdicionadosFragment : Fragment(), View.OnClickListener, RecyclerViewOnClickListenerHack {

    // TODO: Customize parameters
    private var adapter: Adapter_Produtos? = null
    var mRecyclerViewOnClickListenerHack: RecyclerViewOnClickListenerHack? = null
    var removerItem: Boolean = false

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(context)
        fab.setOnClickListener(this)
        MetodosemComum.mostrareSumuirFab(recyclerView, fab)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // To activate the top menu
        setHasOptionsMenu(true)
        activity.invalidateOptionsMenu()
        return inflater!!.inflate(R.layout.fragment_produtos_adicionados, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mostrarItensAdicionados()
        buscarItens()
        MetodosemComum.funcionarOnBackPressEdicao()
    }

    override fun onResume() {
        super.onResume()
        mostrarItensAdicionados()
        subtotal()
    }

    override fun onPause() {
        super.onPause()
        removerItem = false
    }

    override fun onDetach() {
        // Se não for remover item então pode fechar fragment
        if (!removerItem) {
            MetodosemComum.funcionarOnBackPressEdicao()
        } else {
            cancelarExclusao()
        }
        super.onDetach()
    }

    private fun mostrarItensAdicionados() {
        if (CadastroAtendimento.itensProdutoSelecionados != null) {
            txtNadaConsta.visibility = View.GONE
            ll_carregando.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            // For setter adapter
            adapter = Adapter_Produtos(context, CadastroAtendimento.itensProdutoSelecionados!! as java.util.ArrayList<Produto>, podeAdicionar = false)
            adapter!!.mRecyclerViewOnClickListenerHack = this@ProdutosAdicionadosFragment
            recyclerView.adapter = adapter
        } else {
            txtNadaConsta.visibility = View.GONE
        }
    }

    //Somar  produtos selecionados
    private fun subtotal() {
        if (CadastroAtendimento.itensProdutoSelecionados != null) {
            var subtotal = 0.0
            //Percorrer itens
            for (itemProduto in CadastroAtendimento.itensProdutoSelecionados!!) {
                subtotal += itemProduto.preco * itemProduto.quantidade
            }
            if (subtotal == 0.0)
                esconderMenuDelete()

            //Mostra subtotal no edittext
            txtSoma.text = MetodoEmComum.formatoMoeda(subtotal)
        }
    }

    private fun buscarItens() {
        if (CadastroAtendimento.itensProdutoSelecionados == null) {
            buscarItensExistentes()
        }
    }

    private fun buscarItensExistentes() {
        when {
        // Se tiver atendimento existente
            CadastroAtendimento.idAtendimento != 0 -> {

                MetodoEmComum.mostrarProgressnoAsync(recyclerView, ll_carregando, txtNadaConsta, txtSemConexao)

                doAsync {
                    var resposta = kotlin.arrayOfNulls<String>(2)
                    try {
                        resposta = WebServiceCliente(context).get("TOrdemServico/\"ItensAtendimento\"/${CadastroAtendimento.idAtendimento}/1")
                        CadastroAtendimento.itensProdutoSelecionados = RespostaJson.getRespostaItensProduto(resposta[1] as String)
                        if (resposta[0] == "0") {
                            throw Exception(resposta[1])
                        }

                    } catch (e: Exception) {

                    }
                    uiThread {
                        if (CadastroAtendimento.itensProdutoSelecionados != null) {
                            when {
                                CadastroAtendimento.itensProdutoSelecionados!!.isNotEmpty() -> {
                                    ll_carregando.visibility = View.GONE
                                    mostrarItensAdicionados()
                                    subtotal()
                                }
                                else -> {
                                    MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(MetodoEmComum.SEM_ERRO, ll_carregando, txtNadaConsta, txtSemConexao)
                                    esconderMenuDelete()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun enviarItens() {
        Progressos.criaProgressSemInterromper(context, "Aguarde!", "Enviando...")
        doAsync {
            var erro: String = ""
            val objeto = Gson().toJson(CadastroAtendimento.itensProdutoSelecionados)

            try {
                val resposta = WebServiceCliente(context).post("TOrdemServico/\"AdicionaProdutoAtdm\"/${CadastroAtendimento.idAtendimento}", objeto)
                // Checks for error in response
                if (resposta[0] == "0") {
                    throw Exception(resposta[1])
                }
            } catch (e: Exception) {
                erro = e.message.toString()
            }

            uiThread {
                if (erro.isEmpty()) {
                    Progressos.fecharProgress()
                    Toast.makeText(context, "Enviado com sucesso!", Toast.LENGTH_SHORT).show()
                } else {
                    Progressos.fecharProgress()
                    Mensagem.exibeMensagem(context, "Erro!", "Erro ao enviar!\n$erro", false)
                }

            }
        }
    }

    private fun esconderMenuDelete() {
        removerItem = false
        activity.invalidateOptionsMenu()
    }

    private fun cancelarExclusao() {
        esconderMenuDelete()
        MetodosemComum.funcionarOnBackPressEdicao()
        adapter!!.cancelarExclusao()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        if (removerItem) {
            //Adicionando botão excluir
            //0 Grupo // 0 ID // 100 Posição // Descrição
            menu!!.add(0, 0, 101, "Remover produtos").setIcon(R.drawable.ic_delete)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_save -> {
                //enviarItens()
                mRecyclerViewOnClickListenerHack!!.onAlterar()
            }
        // Delete button
            0 -> {
                esconderMenuDelete()
                MetodosemComum.funcionarOnBackPressEdicao()
                Toast.makeText(context, "Itens excluídos", Toast.LENGTH_SHORT).show()
                adapter!!.removerItensList()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v) {
            fab -> {
                val fragment = ProdutosListFragment()
                val i = Intent(activity, ActEdicao::class.java)
                ActEdicao.fragment = fragment
                ActEdicao.tituloFragmento = "Produtos"
                startActivity(i)
            }
        }
    }

    override fun onClickListenerRv(view: View, position: Int) {
        // Para quando clicar em voltar não fechar a Activity atual
        ActEdicao.fragment = this

        //aqui farei mudar o menu
        removerItem = true
        //ativarBotaoCancelar()
        //Reecria menu toolbar
        activity.invalidateOptionsMenu()
    }

    override fun onExcluir(view: View?, position: Int) {

    }

    override fun onAdicionar(posicao: Int) {

    }

    override fun onAtualizar(posicao: Int) {
        subtotal()
    }

    override fun onAlterar() {
        esconderMenuDelete()
        MetodosemComum.funcionarOnBackPressEdicao()
    }
}
