package eres.osmobile.com.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.android.volley.Request
import eres.osmobile.com.R
import eres.osmobile.com.activity.MainActivity
import eres.osmobile.com.adapter.AdapterIdentificadoresTabelas
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.interfaces.AcoesLiteners
import eres.osmobile.com.interfaces.VolleyResponseListener
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.VolleyRequest
import eres.osmobile.com.model.IdentificadorTabelas
import kotlinx.android.synthetic.main.content_geral.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * A fragment representing a list of Items.
 */
class ListaIdentificadoresTabelasListFragment : Fragment(), VolleyResponseListener, AcoesLiteners {

    private var scroll: RecyclerView.OnScrollListener? = null
    private var adapter: AdapterIdentificadoresTabelas? = null
    var listaIdentificadoresTabelas: ArrayList<IdentificadorTabelas?>? = null
    private var pagina = 1
    private var pesquisa = ""
    private var naoRepetirScrolled: Boolean = false
    private var searchView: SearchView? = null
    private var primeiraSearch: Boolean = true

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(context)
        scrollInfinito()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        //Para ativar o menu do topo
        setHasOptionsMenu(true)
        return inflater!!.inflate(R.layout.fragment_identificadores_tabelas, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).supportActionBar!!.title = "Checklists Abertas"
        (activity as MainActivity).supportActionBar!!.subtitle = "por Identificador"
    }

    override fun onResume() {
        super.onResume()
        buscaIdentificadoresTabelas()
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as MainActivity).supportActionBar!!.subtitle = ""
    }

    private fun scrollInfinito() {
        scroll = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(mRecyclerView: RecyclerView?, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                }
                super.onScrollStateChanged(mRecyclerView, newState)
            }

            override fun onScrolled(mRecyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(mRecyclerView, dx, dy)

                val llm: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (listaIdentificadoresTabelas!!.size == llm.findLastVisibleItemPosition() + 1) {
                    // Verifica se houve mudança no intervalo de ítens em Y
                    if (dy > 0) {
                        //Fazer um list contato com mais 20
                        adicionarNovosRegistros()
                    } else if (!naoRepetirScrolled) {
                        doAsync {
                            uiThread {
                                // Se não houve mudança e está no fim da página
                                removerProgressdaLista()
                            }
                        }
                        // impedir de chamar novamente o scroll infinito
                        recyclerView.removeOnScrollListener(scroll)
                    }
                }
            }
        }
    }

    private fun buscaIdentificadoresTabelas() {

        MetodoEmComum.mostrarProgressnoAsync(recyclerView, ll_carregando, txtNadaConsta,
                txtSemConexao)
        pagina = 1
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montarURL()),
                null, this, "buscaIdentificadoresTabelas")
    }

    private fun montarURL(): String =
            "TOrdemServico/\"BuscarTabelasporIdentificador\"/$pesquisa/$pagina"

    private fun respostaIdentificadoresTabelas(response: String) {
        listaIdentificadoresTabelas = RespostaJson.getListIdentificadorTabelas(response)

        recyclerView.visibility = View.VISIBLE
        adapter = AdapterIdentificadoresTabelas(context, this, listaIdentificadoresTabelas!!)
        recyclerView.adapter = adapter
        if (listaIdentificadoresTabelas!!.size == 0){
            MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(MetodoEmComum.SEM_ERRO, ll_carregando, txtNadaConsta, txtSemConexao)
        }
        listaIdentificadoresTabelas!!.add(null)
        naoRepetirScrolled = false
        recyclerView.addOnScrollListener(scroll)

    }

    private fun adicionarNovosRegistros() {
        pagina++
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montarURL()),
                null, this, "adicionaIdentificadoresTabelas")
    }

    private fun respostaAdicionaIdentificadoresTabelas(response: String) {
        val listIdentificadoresTab = RespostaJson.getListIdentificadorTabelas(response)

        removerProgressdaLista()

        if (listIdentificadoresTab.size > 0) {
            for (i in 0 until listIdentificadoresTab.size) {
                adapter!!.addItemList(listIdentificadoresTab[i], listaIdentificadoresTabelas!!.size)
            }
            adicionarProgressnaLista()
            naoRepetirScrolled = false
        } else {
            recyclerView.removeOnScrollListener(scroll)
        }

    }

    private fun adicionarProgressnaLista() {
        listaIdentificadoresTabelas!!.add(null)
        // Additional item in the recycleview mode adapter
        adapter!!.notifyItemInserted(listaIdentificadoresTabelas!!.size - 1)
    }

    private fun removerProgressdaLista() {
        naoRepetirScrolled = true
        //Check that the last position is not a service, so that you can exclude
        if (listaIdentificadoresTabelas!![listaIdentificadoresTabelas!!.lastIndex] == null) {
            // Remove progress from the recyclerview
            try {
                listaIdentificadoresTabelas!!.removeAt(listaIdentificadoresTabelas!!.lastIndex)
                adapter!!.notifyItemRemoved(listaIdentificadoresTabelas!!.size)
            } catch (e: Exception) {

            }

        }
    }

    override fun onResponse(response: Any, tag: String) {
        when (tag) {
            "buscaIdentificadoresTabelas" -> {
                ll_carregando.visibility = View.GONE
                respostaIdentificadoresTabelas(response.toString())
            }
            "adicionaIdentificadoresTabelas" -> {
                respostaAdicionaIdentificadoresTabelas(response.toString())
            }
        }
    }

    override fun onError(message: String, tag: String) {
        when (tag) {
            "buscaIdentificadoresTabelas" -> {
                MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(message, ll_carregando, txtNadaConsta, txtSemConexao)
            }
            "adicionaIdentificadoresTabelas" -> {
                removerProgressdaLista()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        // Infla o menu; este adiciona items para a action bar se está existe.
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_tabela_checklist, menu)

        // Ouvindo searchview do menu
        val search: MenuItem = menu.findItem(R.id.action_search)
        searchView = search.actionView as SearchView

        // Pesquisas no searchview
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    pagina = 1
                    pesquisa = query.toUpperCase()
                    buscaIdentificadoresTabelas()
                } else {
                    pesquisa = ""
                    buscaIdentificadoresTabelas()
                }
                return false
            }

            // Pesquisa enquanto digita o texto
            override fun onQueryTextChange(newText: String?): Boolean {
                //if (newText!!.isNotEmpty()) {
                // Para pesquisar apenas os 5 primeiros
                if (!primeiraSearch) {
                    pagina = 1
                    pesquisa = newText!!.toUpperCase()
                    buscaIdentificadoresTabelas()
                } else primeiraSearch = false


                return false
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun fecharActivity() {}

    override fun onResumeInterface() {
        onResume()
    }


}