package eres.osmobile.com.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.android.volley.Request
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.activity.MainActivity
import eres.osmobile.com.adapter.Adapter_TabelaCheckList
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.interfaces.VolleyResponseListener
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.VolleyRequest
import eres.osmobile.com.model.TabelaCheckList
import kotlinx.android.synthetic.main.content_geral.*
import kotlinx.android.synthetic.main.fragment_tabelachecklist_list.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class TabelaCheckListFragment : Fragment(), VolleyResponseListener, View.OnClickListener {

    private var scroll: RecyclerView.OnScrollListener? = null
    private var adapter: Adapter_TabelaCheckList? = null
    var listaTabelaCheckList: ArrayList<TabelaCheckList?>? = null
    private var mSelecionadas: ArrayList<TabelaCheckList> = arrayListOf()
    private var pagina = 1
    private var pesquisa = ""
    private var naoRepetirScrolled: Boolean = false
    private var idAtendimento: Int = 0
    private var searchView: SearchView? = null
    private var primeiraSearch: Boolean = true

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fab.hide()
        fab.setOnClickListener(this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        scrollInfinito()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        //Para ativar o menu do topo
        setHasOptionsMenu(true)
        return inflater!!.inflate(R.layout.fragment_tabelachecklist_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            (activity as MainActivity).supportActionBar!!.title = "Tabelas Checklist"
        } catch (ignored: Exception) {
            MetodosemComum.funcionarOnBackPressEdicao()
        }

        buscaTabelaCheckList()
    }

    override fun onResume() {
        super.onResume()
        if (atualizar) {
            buscaTabelaCheckList()
        } else {
            atualizar = false
        }
    }

    private fun scrollInfinito() {
        scroll = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(mRecyclerView: RecyclerView?, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (mSelecionadas.size > 0) {
                        fab.show()
                    }
                }
                super.onScrollStateChanged(mRecyclerView, newState)
            }

            override fun onScrolled(mRecyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(mRecyclerView, dx, dy)

                val llm: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (listaTabelaCheckList!!.size == llm.findLastVisibleItemPosition() + 1) {
                    // Esconder fab ao percorrer lista
                    if (dy > 0 || dy < 0 && fab.isShown) {
                        fab.hide()
                    }
                    // Verifica se houve mudança no intervalo de ítens em Y
                    if (dy > 0) {
                        //Fazer um list contato com mais 20
                        adicionarNovosRegistros()
                    } else if (!naoRepetirScrolled) {
                        doAsync {
                            uiThread {
                                // Se não houve mudança e está no fim da página
                                removerProgressdaLista()
                            }
                        }
                        // impedir de chamar novamente o scroll infinito
                        recyclerView.removeOnScrollListener(scroll)
                    }
                }
            }
        }
    }

    private fun buscaTabelaCheckList() {
        MetodoEmComum.mostrarProgressnoAsync(recyclerView, ll_carregando, txtNadaConsta,
                txtSemConexao)
        pagina = 1
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montarURL()),
                null, this, "buscaTabelaCheckList")
    }

    private fun montarURL(): String = when (idAtendimento) {
        0 -> urlChecklist()
        else -> urlAtendimentocheckList()
    }

    private fun urlChecklist(): String {
        var idChecklist = "0"
        var texto = ""
        if (!pesquisa.isEmpty()) {
            // Se só tem números
            if (pesquisa.trim { it <= ' ' }.matches("^[0-9]*$".toRegex())) {
                idChecklist = pesquisa
            } else {
                //Se não; é string
                //Pesquisar por Nome
                texto = pesquisa
            }
        }
        return "TOrdemServico/Checklists/$idChecklist/$texto/$pagina"
    }

    private fun urlAtendimentocheckList(): String {
        var idchecklist = ""
        var descricao = ""

        if (!pesquisa.isEmpty()) {
            // Se só tem números
            if (pesquisa.trim { it <= ' ' }.matches("^[0-9]*$".toRegex())) {
                idchecklist = pesquisa
            } else {
                //Se não; é string
                //Pesquisar por Nome
                descricao = pesquisa
            }
        }

        return "TOrdemServico/CheckListsdoAtendimento/$idAtendimento/$idchecklist/$descricao/$pagina"
    }

    private fun respostaBuscaTabelaCheckList(response: String) {
        listaTabelaCheckList = RespostaJson.getListTabelaCheckList(response)

        recyclerView.visibility = View.VISIBLE
        adapter = Adapter_TabelaCheckList(listaTabelaCheckList!!, mSelecionadas, fab)
        recyclerView.adapter = adapter
        if (listaTabelaCheckList!!.size == 0) {
            MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(MetodoEmComum.SEM_ERRO, ll_carregando, txtNadaConsta, txtSemConexao)
        }
        listaTabelaCheckList!!.add(null)
        naoRepetirScrolled = false
        recyclerView.addOnScrollListener(scroll)

    }

    private fun adicionarNovosRegistros() {
        pagina++
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montarURL()),
                null, this, "adicionaTabelaCheckList")
    }

    private fun respostaAdicionaTabelaCheckList(response: String) {
        val listTabelaCheckList = RespostaJson.getListTabelaCheckList(response)

        removerProgressdaLista()

        if (listTabelaCheckList.size > 0) {
            for (i in 0 until listTabelaCheckList.size) {
                adapter!!.addItemList(listTabelaCheckList[i], listaTabelaCheckList!!.size)
            }
            adicionarProgressnaLista()
            naoRepetirScrolled = false
        } else {
            recyclerView.removeOnScrollListener(scroll)
        }

    }

    private fun adicionarProgressnaLista() {
        listaTabelaCheckList!!.add(null)
        // Additional item in the recycleview mode adapter
        adapter!!.notifyItemInserted(listaTabelaCheckList!!.size - 1)
    }

    private fun removerProgressdaLista() {
        try {
            naoRepetirScrolled = true
            //Check that the last position is not a service, so that you can exclude
            if (listaTabelaCheckList!![listaTabelaCheckList!!.lastIndex] == null) {
                // Remove progress from the recyclerview
                try {
                    listaTabelaCheckList!!.removeAt(listaTabelaCheckList!!.lastIndex)
                    adapter!!.notifyItemRemoved(listaTabelaCheckList!!.size)
                } catch (ignored: Exception) {
                }
            }
        } catch (ignored: Exception) {
        }
    }

    fun setIDAtendimento(idAtendimento: Int) {
        this.idAtendimento = idAtendimento
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        // Infla o menu; este adiciona items para a action bar se está existe.
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_tabela_checklist, menu)

        // Ouvindo searchview do menu
        val search: MenuItem = menu.findItem(R.id.action_search)
        searchView = search.actionView as SearchView

        // Pesquisas no searchview
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.isNotEmpty()) {
                    pagina = 1
                    pesquisa = query.toUpperCase()
                    buscaTabelaCheckList()
                } else {
                    pesquisa = ""
                    buscaTabelaCheckList()
                }
                return false
            }

            // Pesquisa enquanto digita o texto
            override fun onQueryTextChange(newText: String?): Boolean {
                // Para pesquisar apenas os 5 primeiros
                if (!primeiraSearch) {
                    pagina = 1
                    pesquisa = newText!!.toUpperCase()
                    buscaTabelaCheckList()
                } else primeiraSearch = false
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onResponse(response: Any, tag: String) {
        when (tag) {
            "buscaTabelaCheckList" -> {
                try {
                    ll_carregando.visibility = View.GONE
                    respostaBuscaTabelaCheckList(response.toString())
                } catch (ignored: Exception) {
                }
            }
            "adicionaTabelaCheckList" -> {
                respostaAdicionaTabelaCheckList(response.toString())
            }
        }
    }

    override fun onError(message: String, tag: String) {
        when (tag) {
            "buscaTabelaCheckList" -> {
                naomostrarNadaCosnta()
                MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(message, ll_carregando, txtNadaConsta, txtSemConexao)
            }
            "adicionaTabelaCheckList" -> {
                removerProgressdaLista()
            }
        }
    }

    private fun naomostrarNadaCosnta() {
        try {
            if (listaTabelaCheckList!!.size > 0) {
                txtNadaConsta.visibility = View.GONE
            }
        } catch (e: Exception) {
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            fab -> {
                val fragment = CheckListResponderFragment()
                fragment.setSelecionados(mSelecionadas)
                fragment.setIDAtendimento(idAtendimento)
                val i = Intent(activity, ActEdicao::class.java)
                ActEdicao.fragment = fragment
                ActEdicao.tituloFragmento = "Chekcklist"
                startActivity(i)
            }
        }
    }

    companion object {
        var atualizar: Boolean = false
    }

}
