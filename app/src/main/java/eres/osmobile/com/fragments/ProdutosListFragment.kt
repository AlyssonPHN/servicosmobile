package eres.osmobile.com.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import eres.osmobile.com.R
import eres.osmobile.com.adapter.Adapter_Produtos
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.WebServiceCliente
import eres.osmobile.com.model.Produto
import eres.osmobile.com.model.Secao
import eres.osmobile.com.viewPagers.CadastroAtendimento
import kotlinx.android.synthetic.main.content_geral.*
import kotlinx.android.synthetic.main.fragment_produtos_list.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import android.view.animation.TranslateAnimation
import android.view.animation.Animation
import android.widget.AdapterView
import android.widget.Toast
import eres.osmobile.com.model.SubGrupo
import eres.osmobile.com.model.Grupo


class ProdutosListFragment : Fragment(), RecyclerViewOnClickListenerHack, AdapterView.OnItemSelectedListener {

    private var scroll: RecyclerView.OnScrollListener? = null
    private var produtosList: ArrayList<Produto?>? = null
    private var produtosSelecionados = ArrayList<Produto>()
    private var secaoList: ArrayList<Secao?>? = arrayListOf()
    private var gruposList: ArrayList<Grupo?>? = arrayListOf()
    private var subgrupoList: ArrayList<SubGrupo?>? = arrayListOf()
    private var gruposSelecionados: ArrayList<Grupo>? = null
    private var subgruposSelecionados: ArrayList<SubGrupo>? = null
    private var secao: Secao? = null
    private var grupo: Grupo? = null
    private var subGrupo: SubGrupo? = null
    private var IDpromocao = 0
    private var pagina = 1
    private var adapter: Adapter_Produtos? = null
    private var naoRepetirScrolled: Boolean = false
    private var pesquisa = ""
    private var searchView: SearchView? = null
    private var tamanhoPesquisa = 0
    private var vemdaSecao: Boolean = false
    private var vemdoGrupo: Boolean = false


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // To activate the top menu
        setHasOptionsMenu(true)
        spinner_secoes.onItemSelectedListener = this
        spinner_grupos.onItemSelectedListener = this
        spinner_subgrupos.onItemSelectedListener = this
        recyclerView.layoutManager = LinearLayoutManager(context)
        scrollInfinito()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater!!.inflate(R.layout.fragment_produtos_list, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buscarCategorias()
        buscarProdutos("")
        MetodosemComum.funcionarOnBackPressEdicao()
    }

    override fun onResume() {
        super.onResume()
        subtotal()
    }

    private fun scrollInfinito() {
        scroll = object : RecyclerView.OnScrollListener() {

            override fun onScrolled(mRecyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(mRecyclerView, dx, dy)

                val llm: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (produtosList!!.size == llm.findLastVisibleItemPosition() + 1) {
                    // Verifica se houve mudança no intervalo de ítens em Y
                    if (dy > 0) {
                        //Fazer um list contato com mais 20
                        adicionarNovosProdutos()
                    } else if (!naoRepetirScrolled) {
                        doAsync {
                            uiThread {
                                // Se não houve mudança e está no fim da página
                                removerProgressdaLista()
                            }
                        }
                        // impedir de chamar novamente o scroll infinito
                        recyclerView.removeOnScrollListener(scroll)
                    }
                }
            }
        }
    }

    //Monta o URL de pesquisa e verifica se pesquisa é string ou inteiro
    @Synchronized private fun montaURLdePesquisa(pesquisa: String): String {
        //pegar tabela de preço
        val tabela = 1
        //variáveis de contrução da url
        var url: String
        val idcliente = 0
        var idProduto = "0"
        var descricao = ""
        var idSecao = ""
        val referencia = ""
        var idGrupo = 0
        var idSubGrupo = 0

        if (pesquisa.trim { it <= ' ' } != "") {
            //Se só tem números
            if (pesquisa.trim { it <= ' ' }.matches("^[0-9]*$".toRegex())) {
                //pesquisar por id produto
                idProduto = pesquisa.trim { it <= ' ' }
            } else {
                //Se não; é string. Pesquisar por Nome
                descricao = pesquisa
            }
        } else {
            //Se a pesquisa for vazia
            idProduto = "0"
        }
        //Busca o id do vendedor
        val codVendedor = 0
        //Se tiver seção
        if (secao != null) {
            idSecao = secao!!.IDSEC
        }
        //Se tiver grupo
        if (grupo != null) {
            idGrupo = grupo!!.IDGRP
        }
        //Se tiver subgrupo
        if (subGrupo != null) {
            idSubGrupo = subGrupo!!.IDSGP
        }

        //Montada Url para buscar o produto
        //Se seção for promoção, ou filtro foi feito diretamente na tela de pedido
        //Se não, veio de outro tela e usa a URL normal
        if (idSecao.equals("PROMOCAO", ignoreCase = true)) {
            try {
                IDpromocao = grupo!!.IDGRP
                url = "TCadastroGeral/ProdutosPromocao/$codVendedor/$idcliente/$tabela/$idProduto//$descricao//$referencia/0/0/$pagina/$IDpromocao"
                //Limpando variável IDpromoção
                IDpromocao = 0
            } catch (ignored: Exception) {
                url = "TCadastroGeral/ProdutosPromocao/$codVendedor/$idcliente/$tabela/$idProduto//$descricao//$referencia/0/0/$pagina/0"
            }

        } else if (IDpromocao == 0) {
            url = "TCadastroGeral/Produtos/$codVendedor/$idcliente/$tabela/$idProduto//$descricao/$idSecao/$referencia/$idGrupo/$idSubGrupo/$pagina"
        } else {
            url = "TCadastroGeral/ProdutosPromocao/$codVendedor/$idcliente/$tabela/$idProduto//$descricao/$idSecao/$referencia/$idGrupo/$idSubGrupo/$pagina/$IDpromocao"
        }

        //URL montada
        return url
    }

    private fun buscarProdutos(pesquisal: String = "") {
        // limpar paginação
        pagina = 1
        doAsync {
            pesquisa = MetodosemComum.aplicarStringPesquisa(pesquisal)
            // Do something in a secondary thread (Faz alfuma coisa na segunda thread)
            val resposta = WebServiceCliente(context).get(montaURLdePesquisa(MetodoEmComum.trataEspacoemREST(pesquisal)))
            try {
                produtosList = RespostaJson.getListProdutos(resposta[1])
            } catch (e: Exception) {

            }
            uiThread {
                if (produtosList != null && produtosList!!.isNotEmpty()) {
                    ll_carregando.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                    // For setter adapter
                    adapter = Adapter_Produtos(context, produtosList!! as kotlin.collections.ArrayList<Produto>, podeAdicionar = true)
                    adapter!!.mRecyclerViewOnClickListenerHack = this@ProdutosListFragment
                    recyclerView.adapter = adapter
                    produtosList!!.add(null)
                    naoRepetirScrolled = false
                    recyclerView.addOnScrollListener(scroll)
                }
            }

        }

    }

    private fun adicionarNovosProdutos() {
        doAsync {

            pagina++
            val resposta: Array<String?>
            var listProdutosAuxiliar: ArrayList<Produto?>? = null

            try {
                resposta = WebServiceCliente(context)[montaURLdePesquisa(MetodoEmComum.trataEspacoemREST(pesquisa))]
                listProdutosAuxiliar = RespostaJson.getListProdutos(resposta[1])
            } catch (ignored: Exception) {

            }
            uiThread {
                removerProgressdaLista()
                when {
                    listProdutosAuxiliar != null -> {
                        if (listProdutosAuxiliar!!.size > 0) {
                            for (i in 0 until listProdutosAuxiliar!!.size) {
                                adapter!!.addItemList(listProdutosAuxiliar!![i], produtosList!!.size)
                            }
                            adicionarProgressnaLista()
                            naoRepetirScrolled = false
                        } else {
                            recyclerView.removeOnScrollListener(scroll)
                        }
                    }
                }
            }
        }
    }

    private fun buscarCategorias() {
        val threadSecoe = Thread(Runnable {
            buscarSecoes()
            buscarGrupos()
            buscarSubGrupos()
        })
        threadSecoe.start()
        threadSecoe.join()

    }

    private fun buscarSecoes() {
        doAsync {
            try {
                val resposta = WebServiceCliente(context).get("TCadastroGeral/\"Secoes\"")
                secaoList = RespostaJson.getListSecoes(resposta[1])
            } catch (e: Exception) {
            }
            uiThread {
                preencherSpinnerSecoes()
            }
        }
    }

    private fun buscarGrupos() {
        doAsync {
            try {
                val resposta = WebServiceCliente(context).get("TCadastroGeral/\"Grupos\"")
                gruposList = RespostaJson.getListGrupos(resposta[1])
            } catch (e: Exception) {
            }
        }
    }

    private fun buscarSubGrupos() {
        doAsync {
            try {
                val resposta = WebServiceCliente(context).get("TCadastroGeral/\"SubGrupos\"")
                subgrupoList = RespostaJson.getListSubGrupos(resposta[1])
            } catch (e: Exception) {
            }
        }

    }

    private fun preencherSpinnerSecoes(){
        val secaoDescricoes = arrayListOf<String>()
        if (secaoList!!.size > 0) {
            secaoDescricoes.add("Todos")
            for (secao in secaoList!!){
                Log.i("Secao", "" + secao!!.XDESC)
                secaoDescricoes.add(secao.XDESC)
            }
        } else {
            secaoDescricoes.add("SEM INFORMAÇÃO")
        }
        MetodoEmComum.preencherSpinnerCenterBranco(context, spinner_secoes, secaoDescricoes)
    }

    private fun preencherSpinnerGrupo(){
        val grupoDescricoes = arrayListOf<String>()
        if (gruposSelecionados!!.size > 0){
            grupoDescricoes.add("Todos")
            for (grupo in gruposSelecionados!!){
                Log.i("Secao", "" + grupo.XDESC)
                grupoDescricoes.add(grupo.XDESC)
            }
        } else {
            grupoDescricoes.add("SEM INFORMAÇÃO")
        }
        MetodoEmComum.preencherSpinnerCenterBranco(context, spinner_grupos, grupoDescricoes)
    }

    private fun preencherSpinnerSubGrupo(){
        val subgrupoDescricoes = arrayListOf<String>()
        if (subgruposSelecionados!!.size > 0){
            subgrupoDescricoes.add("Todos")
            for (subGrupo in subgruposSelecionados!!){
                Log.i("Secao", "" + subGrupo.XDESC)
                subgrupoDescricoes.add(subGrupo.XDESC)
            }
        } else {
            subgrupoDescricoes.add("SEM INFORMAÇÃO")
        }
        MetodoEmComum.preencherSpinnerCenterBranco(context, spinner_subgrupos, subgrupoDescricoes)
    }

    private fun buscarGruposporIDSecao() {
        try {
            gruposSelecionados = ArrayList()
            //Percorre os grupos da lista, para verificar o IDSEC
            Log.w("grupoList", "" + gruposList!!.size)
            for (grupo in gruposList!!) {
                //Se IDSEC do grupo for igual a IDSEc da secao, adiciona o grupo na lista
                if (grupo!!.IDSEC.equals(secao!!.IDSEC, ignoreCase = true)) {
                    gruposSelecionados!!.add(grupo)
                }
            }
            //Se tiver grupos, então mostre-o
            if (gruposSelecionados!!.size > 0) {
                ll_grupos.visibility = View.VISIBLE
                vemdaSecao = true
                //preeche spinner grupos
                preencherSpinnerGrupo()
            } else {
                //esconde spinner subgrupos
                esconderGrupoeSubGrupo()
            }
        } catch (ignored: Exception) {

        }

    }

    private fun buscarSubGruposporIDSecao() {
        subGrupo = null
        subgruposSelecionados = ArrayList()
        //Percorre os subgrupos da lista, para verificar o IDSEC
        for (subgrupo in subgrupoList!!) {
            //Se IDSEC do subgrupo for igual a IDSEc da secao, adiciona o subgrupo na lista
            if (subgrupo!!.IDGRP.equals(grupo!!.IDGRP) && subgrupo.IDSEC.equals(grupo!!.IDSEC)) {
                subgruposSelecionados!!.add(subgrupo)
            }
        }
        //Se tiver subgrupos, mostre-os
        if (subgruposSelecionados!!.size > 0) {
            ll_subgrupos.visibility = View.VISIBLE
            vemdoGrupo = true
            preencherSpinnerSubGrupo()
        } else {
            //Se não tiver subgrupo, limpa subgrupo
            subGrupo = null
            //Se não tiver subgrupo, esconde subgrupos
            ll_subgrupos.visibility = View.GONE
        }
    }

    //Esconde grupo e subgrupos
    private fun esconderGrupoeSubGrupo() {
        //Se não tiver grupo, limpa objeto grupo e subgrupo
        grupo = null
        //Limpa subgrupo
        subGrupo = null
        //Se não tiver grupo, também não tem subgrupo e o esconde-os
        ll_grupos.visibility = View.GONE
        ll_subgrupos.visibility = View.GONE
    }

    private fun adicionarProgressnaLista() {
        produtosList!!.add(null)
        // Additional item in the recycleview mode adapter
        adapter!!.notifyItemInserted(produtosList!!.size - 1)
    }

    private fun removerProgressdaLista() {
        naoRepetirScrolled = true
        //Check that the last position is not a service, so that you can exclude
        if (produtosList!![produtosList!!.lastIndex] == null) {
            // Remove progress from the recyclerview
            produtosList!!.removeAt(produtosList!!.lastIndex)
            adapter!!.notifyItemRemoved(produtosList!!.size)
        }
    }

    //Somar  produtos selecionados
    private fun subtotal() {
        if (CadastroAtendimento.itensProdutoSelecionados != null) {
            var subtotal = 0.0
            //Percorrer itens
            for (itemProduto in CadastroAtendimento.itensProdutoSelecionados!!) {
                subtotal += itemProduto.preco * itemProduto.quantidade
            }
            //Mostra subtotal no edittext
            txtSoma.text = MetodoEmComum.formatoMoeda(subtotal)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_search_filter, menu)

        searchView = MenuItemCompat.getActionView(menu.findItem(R.id.action_search)) as SearchView

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // Searching by clicking the keyboard search button
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                when {
                    newText!!.length > 2 && newText.length >= tamanhoPesquisa -> {
                        buscarProdutos(newText.toUpperCase())
                    }
                    newText.isEmpty() && tamanhoPesquisa > 0 -> {
                        buscarProdutos()
                    }
                }
                tamanhoPesquisa = newText!!.length
                return false
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.action_filtro -> {
                if (topo_menu.visibility == View.GONE) {
                    topo_menu.visibility = View.VISIBLE
                    val deslocamentotopo = TranslateAnimation(500f, 0f, 0f, 0f)
                    deslocamentotopo.duration = 300
                    topo_menu.startAnimation(deslocamentotopo)
                } else {
                    //Se ll_topo_menu, não estiver oculto, oculte-o (com Animação)
                    val deslocamentotopo = TranslateAnimation(0f, 500f, 0f, 0f)
                    deslocamentotopo.duration = 300
                    topo_menu.startAnimation(deslocamentotopo)
                    deslocamentotopo.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationStart(animation: Animation) {

                        }

                        override fun onAnimationEnd(animation: Animation) {
                            topo_menu.visibility = View.GONE
                        }

                        override fun onAnimationRepeat(animation: Animation) {

                        }
                    })
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent){
            spinner_secoes ->{
                if (position > 0) {
                    secao = secaoList!!.get(position - 1)
                    buscarGruposporIDSecao()
                    // Filtrar produtos
                    buscarProdutos(pesquisa)
                } else {
                    //Limpar secao
                    secao = null
                    //Para limpar os grupos e subgrupos
                    esconderGrupoeSubGrupo()
                    // Filtrar produtos
                    buscarProdutos(pesquisa)
                }
            }

            spinner_grupos -> {
                if (position > 0) {
                    grupo = gruposSelecionados!!.get(position - 1)
                    buscarSubGruposporIDSecao()
                    // Filtrar produtos
                    buscarProdutos(pesquisa)
                } else {
                    //Se não tiver grupo, também não tem subgrupo limpa e esconde
                    grupo = null
                    subGrupo = null
                    ll_subgrupos.visibility = View.GONE
                    if (!vemdaSecao) {
                        // Filtrar Produtos
                        buscarProdutos(pesquisa)
                    }
                }
                vemdaSecao = false
            }

            spinner_subgrupos -> {
                if (position > 0) {
                    subGrupo = subgruposSelecionados!!.get(position - 1)
                    // Filtrar produtos
                    buscarProdutos(pesquisa)
                } else {
                    //Limpar subgrupo
                    subGrupo = null
                    if (!vemdoGrupo) {
                        // Filtrar produtos
                        buscarProdutos(pesquisa)
                    }
                }
                vemdoGrupo = false
            }
        }
    }

    override fun onClickListenerRv(view: View, position: Int) {

    }

    override fun onExcluir(view: View?, position: Int) {

    }

    override fun onAdicionar(posicao: Int) {

    }

    override fun onAtualizar(posicao: Int) {
        subtotal()
    }

    override fun onAlterar() {

    }


}
