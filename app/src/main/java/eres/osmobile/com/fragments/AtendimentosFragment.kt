package eres.osmobile.com.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.database.MatrixCursor
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.Request
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.activity.MainActivity
import eres.osmobile.com.adapter.Adapter_Atendimento
import eres.osmobile.com.adapter.SearchFeedResultsAdaptor
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.interfaces.VolleyResponseListener
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.VolleyRequest
import eres.osmobile.com.json.WebServiceCliente
import eres.osmobile.com.model.Atendimento
import eres.osmobile.com.model.Status
import eres.osmobile.com.viewPagers.CadastroAtendimento
import org.json.JSONException
import java.util.*


class AtendimentosFragment : Fragment(), View.OnClickListener, VolleyResponseListener {

    private var mRecyclerView: RecyclerView? = null
    private var fab: FloatingActionButton? = null
    private var scroll: RecyclerView.OnScrollListener? = null
    private var atendimentoList: ArrayList<Atendimento?> = ArrayList()
    private var llcarregando: LinearLayout? = null
    private var txtNadaconsta: TextView? = null
    private var txtStatus: TextView? = null
    private var txtSemConexao: TextView? = null
    private var statusList: List<Status>? = null
    private var adapter: Adapter_Atendimento? = null
    private var carregouStatus: Boolean = false
    private var status = ""
    private var atendimentoID = ""
    private var naoRepetirScrolled: Boolean = false
    private var searchView: SearchView? = null
    private var mSearchViewAdapter: SearchFeedResultsAdaptor? = null
    /* Página é o select do atendimento
      * 0 = traz os 5 primeiros
      * 1 ou + = traz de 50 em 50
     */
    private var pagina = 1

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        //Para ativar o menu do topo
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_atendimento_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtNadaconsta = view!!.findViewById(R.id.txtNadaConsta)
        llcarregando = view.findViewById(R.id.ll_carregando)
        txtStatus = view.findViewById(R.id.txt_status)
        txtSemConexao = view.findViewById(R.id.txtSemConexao)
        fab = view.findViewById(R.id.fab)
        fab!!.setOnClickListener(this)

        mRecyclerView = view.findViewById(R.id.recyclerView)
        mRecyclerView!!.setHasFixedSize(true)
        //Seta configuração da re cyclerview
        val llmanager = LinearLayoutManager(activity)
        llmanager.orientation = LinearLayout.VERTICAL
        mRecyclerView!!.layoutManager = llmanager
        mostrareSumuirFab()

        //INICIA O SCROLL
        //Para adicionar mais itens na lista através da ação de descer do scroll
        scroll = object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val llm = mRecyclerView!!.layoutManager as LinearLayoutManager
                adapter = mRecyclerView!!.adapter as Adapter_Atendimento
                //Verifica se já está no final do layout
                if (atendimentoList.size == llm.findLastCompletelyVisibleItemPosition() + 1) {
                    // Verifica se houve mudança no intervalo de ítens em Y
                    if (dy > 0) {
                        //Fazer um list contato com mais 20
                        adicionarNovosAtendimentos(adapter)
                    } else if (!naoRepetirScrolled) {
                        // Se não houve mudança e está no fim da página
                        removerProgressdaLista()
                        // impedir de chamar novamente o scroll infinito
                        mRecyclerView!!.removeOnScrollListener(scroll)
                    }
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        MetodosemComum.funcionarOnBackPressEdicao()
        // Nome do tela no topo
        (activity as MainActivity).supportActionBar!!.title = "Atendimentos"

    }

    override fun onResume() {
        super.onResume()
        if (!naoAtualizar) {
            buscaStatus()
        } else {
            naoAtualizar = false
        }
    }

    private fun buscaStatus() {
        MetodoEmComum.mostrarProgressnoAsync(mRecyclerView!!, llcarregando!!, txtNadaconsta!!,
                txtSemConexao!!)

        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST("TOrdemServico/Status"), null, this, "buscaStatus")

    }

    private fun respostaStatus(resposta: String) {
        var statusLista: List<eres.osmobile.com.model.Status>? = null
        try {
            statusLista = RespostaJson.getLitStatus(resposta)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (statusLista == null) {
            MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(MetodoEmComum.SEM_ERRO, llcarregando!!,
                    txtNadaconsta!!, txtSemConexao!!)
        } else {
            statusList = statusLista
            carregouStatus = true
            try {
                activity.invalidateOptionsMenu()
            } catch (ignored: Exception) {
            }

            if (carregouStatus) {
                buscarAtendimentos("")
            }
        }
    }

    fun buscarAtendimentos(pesquisa: String) {

        // Mostra que está carregando apenas se está buscando mais de 5 atendimentos
        if (pagina != 0) {
            pagina = 1
            // Progress visível caso não esteja visível
            llcarregando!!.visibility = View.VISIBLE
        }
        //Sumir nada consta
        txtNadaconsta!!.visibility = View.GONE
        // Esconder Textview Sem Conexão
        txtSemConexao!!.visibility = View.GONE
        // Esconder Recyclerview
        mRecyclerView!!.visibility = View.GONE

        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montaURLdePesquisa(pesquisa)), null, this, "buscarAtendimentos")
    }

    private fun respostaAtendimentos(resposta: String) {
        var atendimentos: ArrayList<Atendimento?>? = null
        try {
            atendimentos = RespostaJson.getListAtendimento(resposta)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (atendimentos != null) {
            // Se página é igual a zera, está usando a suggestões de pesuisa
            if (pagina == 0) {
                atendimentoList.clear()
                atendimentoList = atendimentos
                val matrixCursorClientes = convertToCursor(atendimentos)
                mSearchViewAdapter!!.changeCursor(matrixCursorClientes)
            } else {
                atendimentoList.clear()
                atendimentoList = atendimentos
                try {
                    if (atendimentoList.size > 0) {
                        //Se tem ítens, sumir progress e nada consta
                        llcarregando!!.visibility = View.GONE
                        txtNadaconsta!!.visibility = View.GONE
                        //Visualizar RecycleverView
                        mRecyclerView!!.visibility = View.VISIBLE
                        adapter = Adapter_Atendimento(activity, atendimentoList, statusList!!)
                        mRecyclerView!!.adapter = adapter
                        //Adicionando  valor nulo no produto o recycleview irá adicionar o progresss no rodapé
                        atendimentoList.add(null)
                        naoRepetirScrolled = false
                        //Ativa ou reativa o scroll se necessário
                        mRecyclerView!!.addOnScrollListener(scroll)

                    } else {
                        // Progress não fica mais visível
                        llcarregando!!.visibility = View.GONE
                        //Mostrar nada consta
                        txtNadaconsta!!.visibility = View.VISIBLE
                    }
                } catch (ignored: Exception) {
                    //Lista está vazia
                    //Sumir progress e mostrar nada consta
                    llcarregando!!.visibility = View.GONE
                    txtNadaconsta!!.visibility = View.VISIBLE
                }

            }
        } else {
            MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(MetodoEmComum.SEM_ERRO, llcarregando!!,
                    txtNadaconsta!!, txtSemConexao!!)
        }
        if (atendimentos!!.size == 0){
            MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(MetodoEmComum.SEM_ERRO, llcarregando!!,
                    txtNadaconsta!!, txtSemConexao!!)
        }


    }

    /**
     * @param pesquisa - deve pesquisar por
     * * Nome do Cliente e identificador (juntos)
     * * cpf_cnpj
     * * Id do atendimento
     * @return url
     */
    fun montaURLdePesquisa(pesquisa: String): String {
        //variáveis de contrução da url
        val url: String
        // Variáveis
        val clienteID = 0
        val nomeCliente = ""
        var nomeEidentificador = ""
        var cpfcnpj = ""
        val usuario = ""
        val dataEntrada = ""
        val identificador = ""
        val dataEntradanicio = ""
        val dataEntradafinal = ""

        if (!pesquisa.isEmpty()) {
            // Se só tem números
            if (pesquisa.trim { it <= ' ' }.matches("^[0-9]*$".toRegex())) {
                if (pesquisa.trim { it <= ' ' }.length <= 8) {
                    // Pesquisar por ID do ATENDIMENTO
                    atendimentoID = pesquisa.trim { it <= ' ' }
                } else {
                    // Pesquisar por CPF/CNPJ
                    cpfcnpj = pesquisa.trim { it <= ' ' }
                }
            } else {
                //Se não; é string
                //Pesquisar por Nome
                nomeEidentificador = pesquisa
            }
        }


        url = "TOrdemServico/Atendimentos/" + clienteID + "/" + nomeCliente + "/" + nomeEidentificador + "/" +
                cpfcnpj + "/" + usuario + "/" + status + "/" + atendimentoID + "/" + dataEntrada +
                "/" + dataEntradanicio + "/" + dataEntradafinal + "/" + identificador +
                "/" + pagina
        // limpar atendimentoid
        atendimentoID = ""
        return url
    }

    private fun adicionarNovosAtendimentos(adapter: Adapter_Atendimento?) {
        val thread = object : Thread() {
            override fun run() {
                super.run()
                try {
                    pagina++
                    //Requisição e resposta do web service
                    val resposta = WebServiceCliente(activity)
                            .get(MetodoEmComum.trataEspacoemREST(montaURLdePesquisa("")))
                    try {
                        //Lista auxiliar criada para recebe novos produtos vindo do web service
                        val listAtendimentoAux: ArrayList<Atendimento?> = RespostaJson.getListAtendimento(resposta[1])
                        //lista auxiliar recebe os produtos do json
                        removerProgressdaLista()
                        //Se a listaauxiliar estiver preenchida, adiciona no adapter
                        if (listAtendimentoAux.size > 0) {
                            for (i in listAtendimentoAux.indices) {
                                //Adicionar o contato da posição i, na última posição da lista
                                adapter!!.addItemList(listAtendimentoAux[i]!!, atendimentoList.size)
                            }
                            adicionarProgressnaLista()
                            naoRepetirScrolled = false
                        } else {
                            //Desativa o scroll
                            mRecyclerView!!.removeOnScrollListener(scroll)
                        }
                    } catch (e: JSONException) {
                        // Se der algum erro, tirar o progress
                        removerProgressdaLista()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
        thread.start()
        try {
            thread.join()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    private fun mostrareSumuirFab() {
        // Sumir botão flutuante ao rolar
        mRecyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    fab!!.show()
                }

                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 || dy < 0 && fab!!.isShown) {
                    fab!!.hide()
                }
            }
        })
    }

    private fun removerProgressdaLista() {
        naoRepetirScrolled = true
        //Verificar se na última posição não é um atendimento, para poder excluir
        if (atendimentoList[atendimentoList.size - 1] == null) {
            //Tira o progress do recyclerview
            atendimentoList.removeAt(atendimentoList.size - 1)
            //Remove o item do adapter do modo recycleview
            adapter!!.notifyItemRemoved(atendimentoList.size)
        }
    }

    private fun adicionarProgressnaLista() {
        atendimentoList.add(null)
        //Adiciona o item no adapter do modo recycleview
        adapter!!.notifyItemInserted(atendimentoList.size - 1)
    }

    //Cliente para array string, para cursor
    private fun convertToCursor(atendimentos: ArrayList<Atendimento?>): MatrixCursor {
        val cursor = MatrixCursor(columns)
        for (atendimento in atendimentos) {
            val temp = arrayOfNulls<String>(3)
            temp[0] = atendimento!!.idatd.toString()
            temp[1] = atendimento.nome.toString()
            temp[2] = retornarStatus(atendimento.status)
            cursor.addRow(temp)
        }
        return cursor
    }

    private fun retornarStatus(statusAtendimento: Int): String {
        var descricaoStatus = ""
        statusList!!
                .filter { it.status_id == statusAtendimento }
                .forEach { descricaoStatus = it.descricao!! }
        return descricaoStatus
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        // Infla o menu; este adiciona items para a action bar se está existe.
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_atendimento, menu)

        // Ouvindo searchview do menu
        val search: MenuItem = menu.findItem(R.id.action_search)
        searchView = search.actionView as SearchView

        // Pesquisas no searchview
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            // Pesquisa depois de digitar o texto e clicar no botão de pesquisar
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.length > 1) {
                    pagina = 1
                    buscarAtendimentos(query.toUpperCase())
                }
                return false
            }

            // Pesquisa enquanto digita o texto
            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.length > 1) {
                    // Para pesquisar apenas os 5 primeiros
                    pagina = 0
                    buscarAtendimentos(newText.toUpperCase())
                }

                return false
            }
        })

        //SearchView Adapter com Feed de Resultado
        mSearchViewAdapter = SearchFeedResultsAdaptor(context, R.layout.sugestoes_layout, null, columns, null, -1000)
        searchView!!.suggestionsAdapter = mSearchViewAdapter

        searchView!!.setOnSuggestionListener(object : SearchView.OnSuggestionListener {
            override fun onSuggestionSelect(position: Int): Boolean {
                return false
            }

            //Ao clicar na sugestão esse é o que trabalha
            override fun onSuggestionClick(position: Int): Boolean {
                pagina = 1
                atendimentoID = atendimentoList[position]!!.idatd.toString()
                buscarAtendimentos("")
                return false
            }
        })

        // Controla quando Searchview é colapsado ou expandido
        @Suppress("DEPRECATION")
        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.action_search), object : MenuItemCompat.OnActionExpandListener {
            // Faz algo quando é expandido
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                return true
            }

            // Faz algo quando é collapsdo
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {

                pagina = 1
                buscarAtendimentos("")
                return true
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        if (carregouStatus) {
            // Adicionar os menus dos status
            val submenu = menu!!.getItem(1).subMenu
            for (status in statusList!!) {
                submenu.add(0, status.status_id, status.status_id, status.descricao)
            }
        }
    }

    // Para ítens selecionados do menu
    @SuppressLint("SetTextI18n")
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        pagina = 1
        val id = item!!.itemId
        //Verificar em qual item do menu, foi clicado
        if (id == R.id.action_todos) {
            // Status vazio, pois irá pesquisar por todos os status
            status = ""
            // Alterar o texto do status
            txtStatus!!.text = "TODOS"
            // Atualizar lista de atendimento segundo o status
            buscarAtendimentos("")
        } else {
            if (statusList != null) {
                //Percorrer os status no menu do topo para alterar o filtro status
                for (i in statusList!!.indices) {
                    val key = statusList!![i].status_id

                    if (id == key) {
                        // Aplicando valor da key, para filtrar o status
                        status = key.toString()
                        // Alterar o texto do status
                        txtStatus!!.text = statusList!![i].descricao
                        // Atualizar lista de atendimento segundo o status
                        buscarAtendimentos("")
                        //sai do laço
                        break
                    }

                }
            }
        }

        return super.onOptionsItemSelected(item)


    }

    override fun onClick(v: View) {
        if (v === fab) {
            val fragment = CadastroAtendimento()
            fragment.statusList = statusList
            val i = Intent(activity, ActEdicao::class.java)
            ActEdicao.fragment = fragment
            ActEdicao.tituloFragmento = "Novo Atendimento"
            startActivity(i)


            //Animar o botão de click
            //            final OvershootInterpolator interpolator = new OvershootInterpolator();
            //            ViewCompat.animate(fab).
            //                    rotation(360f).
            //                    withLayer().
            //                    setDuration(300).
            //                    setInterpolator(interpolator).
            //                    start();

        }
    }

    override fun onResponse(response: Any, tag: String) {
        if (tag.equals("buscarAtendimentos", ignoreCase = true)) {
            respostaAtendimentos(response.toString())
        } else if (tag.equals("buscaStatus", ignoreCase = true)) {
            respostaStatus(response.toString())
        }
    }

    override fun onError(message: String, tag: String) {
        MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(message, llcarregando!!,
                txtNadaconsta!!, txtSemConexao!!)

    }

    companion object {
        val columns = arrayOf("_id", "NomedoCliente", "Status")
        var naoAtualizar: Boolean = false
    }


}
