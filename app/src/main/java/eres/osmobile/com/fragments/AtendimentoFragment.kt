package eres.osmobile.com.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.model.Atendimento
import eres.osmobile.com.model.Status
import eres.osmobile.com.viewPagers.CadastroAtendimento
import kotlinx.android.synthetic.main.fragment_atendimento.*


class AtendimentoFragment : Fragment() {

    private var atendimento: Atendimento? = null
    private var status: String? = null
    var statusList: List<Status>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        //Para ativar o menu do topo
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_atendimento, container, false)
    }

    override fun onResume() {
        super.onResume()
        verificaMudancaAtendimento()
        MetodosemComum.funcionarOnBackPressEdicao()
        preencherInformacoes()
    }

    private fun verificaMudancaAtendimento(){
        if (CadastroAtendimento.salvo){
            atendimento = CadastroAtendimento.atendimento
        }
    }

    private fun preencherInformacoes() {
        // Verifica se a informação veio preenchida ou está vazia
        txt_cod_atendimento.text = atendimento!!.idatd.toString()
        val sem_informacao = "Sem Informação"
        txt_nome.text = if (atendimento!!.nome!!.isEmpty()) sem_informacao else atendimento!!.nome
        txt_cod_id.text = atendimento!!.clienteID.toString()
        txt_cpf_cnpj.text = (if (atendimento!!.cnpj_cpf!!.isEmpty()) sem_informacao else atendimento!!.cnpj_cpf).toString()
        txt_status.text = status
        txt_identificador.text = if (atendimento!!.identificador!!.isEmpty()) sem_informacao else atendimento!!.identificador
        txt_usario.text = if (atendimento!!.usuario!!.isEmpty()) sem_informacao else atendimento!!.usuario
        txt_data_entrada.text = if (atendimento!!.dataEntrada!!.isEmpty()) sem_informacao else atendimento!!.dataEntrada
        txt_previsao.text = if (atendimento!!.dataPrevisao!!.isEmpty()) sem_informacao else atendimento!!.dataPrevisao
        txt_valor_estimado.text = MetodoEmComum.formatoMoeda(atendimento!!.valorTotal)
        txt_acrescimo.text = MetodoEmComum.formatoMoeda(atendimento!!.valorAcrescimo)
        txt_desconto.text = MetodoEmComum.formatoMoeda(atendimento!!.valorDesconto)
        txt_total.text = MetodoEmComum.formatoMoeda(atendimento!!.valorTotal)
        txt_produto.text = if (atendimento!!.produto!!.isEmpty()) sem_informacao else atendimento!!.produto
        txt_problema.text = if (atendimento!!.problema!!.isEmpty()) sem_informacao else atendimento!!.problema
    }

    fun setAtendimento(atendimento: Atendimento) {
        this.atendimento = atendimento
    }

    fun setStatus(status: String) {
        this.status = status
    }

    private fun direcionarParaEdicao(){
        val fragment = CadastroAtendimento()
        fragment.statusList = statusList
        CadastroAtendimento.atendimento = atendimento
        val i = Intent(activity, ActEdicao::class.java)
        ActEdicao.fragment = fragment
        ActEdicao.tituloFragmento = "Atendimento ${atendimento!!.idatd}"
        startActivity(i)
    }

    private fun direcionarCheckList(idAtendimento: Int) {
        val fragment = TabelaCheckListFragment()
        fragment.setIDAtendimento(idAtendimento)
        val i = Intent(context, ActEdicao::class.java)
        ActEdicao.fragment = fragment
        AtendimentosFragment.naoAtualizar = true
        ActEdicao.tituloFragmento = "Tabelas Checklists"
        context.startActivity(i)
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        // Infla o menu; este adiciona items para a action bar se está existe.
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_atendimento_individual, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_editar -> {
                direcionarParaEdicao()
            }
            R.id.action_checklist -> {
                direcionarCheckList(atendimento!!.idatd)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
