package eres.osmobile.com.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import eres.osmobile.com.R
import eres.osmobile.com.adapter.Adapter_ServicosList
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.WebServiceCliente
import eres.osmobile.com.model.Servico
import eres.osmobile.com.viewPagers.CadastroAtendimento
import kotlinx.android.synthetic.main.content_geral.*
import kotlinx.android.synthetic.main.fragment_servico_list.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ServicosListFragment : Fragment(), RecyclerViewOnClickListenerHack {

    private var scroll: RecyclerView.OnScrollListener? = null
    private var servicosList: ArrayList<Servico?>? = null
    private var adapter: Adapter_ServicosList? = null
    private var naoRepetirScrolled: Boolean = false
    private var pesquisa = ""
    private var pagina = 1
    private var searchView: SearchView? = null
    private var tamanhoPesquisa = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // To activate the top menu
        setHasOptionsMenu(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        scrollInfinito()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_servico_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buscarServicos()
        subtotal()
        MetodosemComum.funcionarOnBackPressEdicao()
    }

    private fun scrollInfinito() {
        scroll = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(mRecyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(mRecyclerView, newState)
            }

            override fun onScrolled(mRecyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(mRecyclerView, dx, dy)

                val llm: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (servicosList!!.size == llm.findLastVisibleItemPosition() + 1) {
                    // Verifica se houve mudança no intervalo de ítens em Y
                    if (dy > 0) {
                        //Fazer um list contato com mais 20
                        adicionarNovosServicos()
                    } else if (!naoRepetirScrolled) {
                        doAsync {
                            uiThread {
                                // Se não houve mudança e está no fim da página
                                removerProgressdaLista()
                            }
                        }
                        // impedir de chamar novamente o scroll infinito
                        recyclerView.removeOnScrollListener(scroll)
                    }
                }
            }
        }
    }

    private fun montaURLdePesquisa(pesquisa: String): String {
        return "TOrdemServico/Servicos/$pesquisa/$pagina"
    }

    private fun buscarServicos(texto: String = "") {
        doAsync {
            pesquisa = MetodosemComum.aplicarStringPesquisa(texto)
            // Do something in a secondary thread (Faz alfuma coisa na segunda thread)
            val resposta = WebServiceCliente(context).get(montaURLdePesquisa(texto))

            try {
                servicosList = RespostaJson.getListServicos(resposta[1])
            } catch (e: Exception) {

            }
            uiThread {
                // Back to the main thread (Retorna para a thread main)
                if (servicosList != null && servicosList!!.isNotEmpty()) {
                    ll_carregando.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                    // For setter adapter
                    adapter = Adapter_ServicosList(servicosList!!, podeAdicionar = true)
                    adapter!!.mRecyclerViewOnClickListenerHack = this@ServicosListFragment
                    recyclerView.adapter = adapter
                    servicosList!!.add(null)
                    naoRepetirScrolled = false
                    recyclerView.addOnScrollListener(scroll)
                }
            }

        }
    }

    private fun adicionarNovosServicos() {
        doAsync {
            pagina++
            val resposta: Array<String?>
            var listServicosAuxiliar: ArrayList<Servico?>? = null

            try {
                resposta = WebServiceCliente(context).get(montaURLdePesquisa(pesquisa))
                listServicosAuxiliar = RespostaJson.getListServicos(resposta[1])
            } catch (ignored: Exception) {

            }
            uiThread {
                removerProgressdaLista()
                when {
                    listServicosAuxiliar != null -> {
                        if (listServicosAuxiliar!!.size > 0) {
                            for (i in 0 until listServicosAuxiliar!!.size) {
                                adapter!!.addItemList(listServicosAuxiliar!![i], servicosList!!.size)
                            }
                            adicionarProgressnaLista()
                            naoRepetirScrolled = false
                        } else {
                            recyclerView.removeOnScrollListener(scroll)
                        }
                    }
                }
            }
        }
    }

    private fun adicionarProgressnaLista() {
        servicosList!!.add(null)
        // Additional item in the recycleview mode adapter
        adapter!!.notifyItemInserted(servicosList!!.size - 1)
    }

    private fun removerProgressdaLista() {
        naoRepetirScrolled = true
        //Check that the last position is not a service, so that you can exclude
        if (servicosList!![servicosList!!.lastIndex] == null) {
            // Remove progress from the recyclerview
            try {
                servicosList!!.removeAt(servicosList!!.lastIndex)
                adapter!!.notifyItemRemoved(servicosList!!.size)
            } catch (e: Exception) {

            }

        }
    }

    //Somar  produtos selecionados
    private fun subtotal() {
        if (CadastroAtendimento.servicosSelecionados != null) {
            var subtotal = 0.0
            //Percorrer itens
            for (servico in CadastroAtendimento.servicosSelecionados!!) {
                subtotal += servico!!.valor
            }
            //Mostra subtotal no edittext
            txtSoma.text = MetodoEmComum.formatoMoeda(subtotal)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_search_filter, menu)
        menu.findItem(R.id.action_filtro).isVisible = false

        @Suppress("DEPRECATION")
        searchView = MenuItemCompat.getActionView(menu.findItem(R.id.action_search)) as SearchView

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // Searching by clicking the keyboard search button
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                when {
                    newText!!.length > 2 && newText.length >= tamanhoPesquisa -> {
                        buscarServicos(newText.toUpperCase())
                    }
                    newText.isEmpty() && tamanhoPesquisa > 0 -> {
                        buscarServicos()
                    }
                }
                tamanhoPesquisa = newText!!.length
                return false
            }

        })
    }

    override fun onClickListenerRv(view: View, position: Int) {

    }

    override fun onExcluir(view: View?, position: Int) {

    }

    override fun onAdicionar(posicao: Int) {

    }

    override fun onAtualizar(posicao: Int) {
        subtotal()
    }

    override fun onAlterar() {

    }
}
