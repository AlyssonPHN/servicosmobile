package eres.osmobile.com.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.Toast
import com.google.gson.Gson
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.adapter.Adapter_ServicosList
import eres.osmobile.com.controller.Mensagem
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.controller.Progressos
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.WebServiceCliente
import eres.osmobile.com.viewPagers.CadastroAtendimento
import kotlinx.android.synthetic.main.content_geral.*
import kotlinx.android.synthetic.main.fragment_servico_adicionados.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class ServicosAdicionadosFragment : Fragment(), View.OnClickListener, RecyclerViewOnClickListenerHack {

    // TODO: Customize parameters
    private var adapter: Adapter_ServicosList? = null
    var mRecyclerViewOnClickListenerHack: RecyclerViewOnClickListenerHack? = null

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(context)
        ll_carregando.visibility = View.GONE
        fab.setOnClickListener(this)
        MetodosemComum.mostrareSumuirFab(recyclerView, fab)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // To activate the top menu
        setHasOptionsMenu(true)
        return inflater!!.inflate(R.layout.fragment_servico_adicionados, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mostrarServicosAdicionados()
        buscarServicosExistente()
    }

    override fun onResume() {
        super.onResume()
        if (CadastroAtendimento.servicosSelecionados!!.isNotEmpty()){
            mostrarServicosAdicionados()
            txtNadaConsta.visibility = View.GONE
        }
        subtotal()

    }

    private fun mostrarServicosAdicionados() {
        recyclerView.visibility = View.VISIBLE
        // For setter adapter
        adapter = Adapter_ServicosList(CadastroAtendimento.servicosSelecionados!!, podeAdicionar = false)
        adapter!!.mRecyclerViewOnClickListenerHack = this@ServicosAdicionadosFragment
        recyclerView.adapter = adapter
    }

    //Somar  produtos selecionados
    private fun subtotal() {
        if (CadastroAtendimento.servicosSelecionados != null) {
            val subtotal = CadastroAtendimento.servicosSelecionados!!.sumByDouble { it!!.valor }
            //Percorrer itens
            //Mostra subtotal no edittext
            txtSoma.text = MetodoEmComum.formatoMoeda(subtotal)
        }
    }

    private fun buscarServicosExistente() {
        when {
        // Se tiver atendimento existente
            CadastroAtendimento.idAtendimento != 0 -> {

                MetodoEmComum.mostrarProgressnoAsync(recyclerView, ll_carregando, txtNadaConsta, txtSemConexao)
                doAsync {

                    try {
                        val resposta = WebServiceCliente(context).get("TOrdemServico/\"ServicosporAtendimento\"/${CadastroAtendimento.idAtendimento}/1")
                        CadastroAtendimento.servicosSelecionados = RespostaJson.getListServicos(resposta[1] as String)
                        if (resposta[0] == "0") {
                            throw Exception(resposta[1])
                        }

                    } catch (e: Exception) {

                    }
                    uiThread {
                        when {
                            CadastroAtendimento.servicosSelecionados!!.isNotEmpty() -> {
                                ll_carregando.visibility = View.GONE
                                mostrarServicosAdicionados()
                                subtotal()
                            }
                            else -> {
                                MetodoEmComum.mostrarNadaConstaOuPerdadeConexao(MetodoEmComum.SEM_ERRO, ll_carregando, txtNadaConsta, txtSemConexao)
                            }
                        }

                    }
                }
            }
        }
    }

    private fun enviarServicos() {
        Progressos.criaProgressSemInterromper(context, "Aguarde!", "Enviando...")
        doAsync {
            var erro = ""
            val objeto = Gson().toJson(CadastroAtendimento.servicosSelecionados)

            try {
                val resposta = WebServiceCliente(context).post("TOrdemServico/\"AdicionaServicoAtdm\"/${CadastroAtendimento.idAtendimento}", objeto)
                // Checks for error in response
                if (resposta[0] == "0") {
                    throw Exception(resposta[1])
                }
            } catch (e: Exception) {
                erro = e.message.toString()
            }

            uiThread {
                if (erro.isEmpty()) {
                    Progressos.fecharProgress()
                    Toast.makeText(context, "Enviado com sucesso!", Toast.LENGTH_SHORT).show()
                } else {
                    Progressos.fecharProgress()
                    Mensagem.exibeMensagem(context, "Erro!", "Erro ao enviar!\n$erro", false)
                }

            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_save -> {
                //enviarServicos()
                mRecyclerViewOnClickListenerHack!!.onAlterar()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        when (v) {
            fab -> {
                val fragment = ServicosListFragment()
                val i = Intent(activity, ActEdicao::class.java)
                ActEdicao.fragment = fragment
                ActEdicao.tituloFragmento = "Serviços"
                startActivity(i)
            }
        }

    }

    override fun onClickListenerRv(view: View, position: Int) {
    }

    override fun onExcluir(view: View?, position: Int) {

    }

    override fun onAdicionar(posicao: Int) {

    }

    override fun onAtualizar(posicao: Int) {

        subtotal()
    }

    override fun onAlterar() {

    }

}
