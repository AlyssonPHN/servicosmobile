package eres.osmobile.com.fragments

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatEditText
import android.view.*
import android.widget.TextView
import eres.osmobile.com.R
import eres.osmobile.com.activity.MainActivity
import eres.osmobile.com.controller.*
import eres.osmobile.com.json.WebServiceCliente

class ConfiguracaoFragment : Fragment() {
    private lateinit var mEdtHost: AppCompatEditText
    private lateinit var mEdtPorta: AppCompatEditText
    private lateinit var mEdtUsuario: AppCompatEditText
    private lateinit var mEdtIDAparelho: TextView
    @Suppress("DEPRECATION")
    private lateinit var mProgressDialog: ProgressDialog
    private lateinit var sharedpreferences: SharedPreferences
    private var mHandler = Handler()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        //Para ativar o menu do topo
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_configuracao2, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        MetodosemComum.funcionarOnBackPressEdicao()
        try {

            (activity as MainActivity).supportActionBar!!.title = "Configurações"
        } catch (ignored: Exception) {

        }

        //Inicia listines
        inicializaObjetos()

        pegaSerialAparelho()
        //preenche componentes com as informações do objeto
        preecheCampos()
    }

    @Suppress("NAME_SHADOWING")
    private fun opcaoTestarConexao() {
        val dialog: AlertDialog.Builder = AlertDialog.Builder(activity)

        dialog.setMessage("Testar conexão?")
        dialog.setIcon(R.drawable.interroga)
        dialog.setPositiveButton("Sim") { _, _ ->
            mProgressDialog = criaProgress()
            mProgressDialog.show()
            testeConexaoWS()
        }
        dialog.setNeutralButton("Não") { dialog, _ -> dialog.dismiss() }
        dialog.setTitle("Aviso")
        dialog.show()
    }

    private fun preecheCampos() {
        //Verificar se já está configuração do web service
        sharedpreferences = activity.getSharedPreferences(WEB_SERVICE, Context.MODE_PRIVATE)
        val caminho = sharedpreferences.getString(CAMINHO, null)
        val porta = sharedpreferences.getString(PORTA, null)
        val usuario = sharedpreferences.getString(USUARIO, null)
        if (caminho != null) {
            mEdtHost.setText(caminho)
            mEdtPorta.setText(porta)
            mEdtUsuario.setText(usuario)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_PHONE_STATE -> {
                // Se a solicitação for cancelada, as matrizes de resultados ficarão vazias.
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    // permissão negada, boo! Desativar o
                    // funcionalidade que depende desta permissão.
                    // Quando o usuário clicou em nunca mostrar
                    // Pedir para o usuario ativar a permissao manualmente, e mostrar como.
                    // E dirercionar para tela
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.READ_PHONE_STATE)) {
                        perguntarAtivarPermissao()
                    } else {
                        // Quando o usuário apenas negou a permissão.
                        //Pedir para o usuário ativar a permissão do telefone
                        permitirReadPhoneState()
                    }
                }
            }
        }// outras linhas 'case' para verificar outras
        // permissões que este aplicativo pode solicitar
    }


    //Mensagem com caixa de diálogo que permite sair da tela ao clicar em OK
    private fun permitirReadPhoneState() {
        // Aparece uma caixa de diálogo na Activity atual
        val dialog = AlertDialog.Builder(activity)
        dialog.setCancelable(false)
        // Título da caixa de diálogo com o Nome de azul
        dialog.setTitle("Aviso")
        // Com a mensagem de azul
        dialog.setMessage("É necessário permitir acesso ao ID do aparelho.")
        //Adicionando icone
        dialog.setIcon(R.drawable.exclamacao)
        // Cria o Botão OK na caixa de diálogo
        dialog.setPositiveButton("OK") { _, _ ->
            //Abre a janela para requisitar a permissão
            requestPermissions(arrayOf(Manifest.permission.READ_PHONE_STATE),
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE)
        }
        // É o que faz a caixa de diálogo ser exibida
        dialog.show()
    }

    //Mensagem com caixa de diálogo que permite sair da tela ao clicar em OK
    private fun perguntarAtivarPermissao() {
        // Aparece uma caixa de diálogo na Activity atual
        val dialog = AlertDialog.Builder(activity)
        dialog.setCancelable(false)
        // Título da caixa de diálogo com o Nome de azul
        dialog.setTitle("Aviso")
        // Com a mensagem de azul
        dialog.setMessage("É necessário ativar permissão do telefone:\n\n" + "Permissões -> Telefone.")
        //Adicionando icone
        dialog.setIcon(R.drawable.exclamacao)
        // Cria o Botão OK na caixa de diálogo
        dialog.setPositiveButton("OK") { _, _ ->
            //Abre a janela para requisitar a permissão
            mostrarConfiguracoesdoAPP(activity)
        }
        // É o que faz a caixa de diálogo ser exibida
        dialog.show()
    }

    private fun pegaSerialAparelho() {
        if (mEdtIDAparelho.text.toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
            mEdtIDAparelho.text = MetodoEmComum.idDispositivo
        }
    }

    private fun inicializaObjetos() {

        mEdtHost = view!!.findViewById(R.id.confg_EdtHost)
        mEdtPorta = view!!.findViewById(R.id.confg_EdtPorta)
        mEdtUsuario = view!!.findViewById(R.id.confg_EdtUsuario)
        mEdtIDAparelho = view!!.findViewById(R.id.confg_EdtIdAparelho)

        if (mEdtHost.text.toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
            mEdtHost.setText(R.string.ip_inicial)
        }
        if (mEdtPorta.text.toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
            mEdtPorta.setText(R.string._8080)
        }
    }

    private fun camposValidos(): Boolean {
        return mEdtHost.text.toString().trim { it <= ' ' }.isNotEmpty() &&
                mEdtPorta.text.toString().trim { it <= ' ' }.isNotEmpty() &&
                mEdtUsuario.text.toString().trim { it <= ' ' }.isNotEmpty() &&
                mEdtIDAparelho.text.toString().trim { it <= ' ' }.isNotEmpty()
    }

    @Synchronized private fun inserir() {

        if (camposValidos()) {
            val editor: SharedPreferences.Editor = sharedpreferences.edit()
            sharedpreferences = activity.getSharedPreferences(WEB_SERVICE, Context.MODE_PRIVATE) //1
            //Limpa se tiver alguma informação
            editor.clear()
            //Pegando os valores do edittext e colocando no SharedPreference
            editor.putString(CAMINHO, mEdtHost.text.toString().trim { it <= ' ' })
            editor.putString(PORTA, mEdtPorta.text.toString().trim { it <= ' ' })
            editor.putString(USUARIO, mEdtUsuario.text.toString().trim { it <= ' ' })
            editor.putString(IDAPARELHO, mEdtIDAparelho.text.toString().trim { it <= ' ' })
            //Aplica as novas informações
            editor.apply()

            try {
                Start.preencherCaminho(context)
            } catch (ignored: Exception) {

            }

            Mensagem.exibeMensagem(activity, "Aviso", "Salvo com sucesso.", true)
            opcaoTestarConexao()
        } else {
            if (mEdtUsuario.text.toString().isEmpty()) {
                mEdtUsuario.error = "Campo não preenchido!"
            } else {
                Mensagem.exibeMensagem(activity, "Aviso", "Erro ao salvar.", false)
            }
        }
    }


    private fun testeConexaoWS() {
        Thread(Runnable {
            try {
                val resposta = WebServiceCliente(activity).testeConexao("TesteConexao")
                when {
                    resposta[0] == "200" -> //Mensagem confirmando a conexão
                        processaMensagem("Conectado com sucesso!", activity, true)
                    resposta[1] == "" -> processaMensagem("Conectado com sucesso!", activity, true)
                    else -> //Mensagem informando o erro
                        processaMensagem("Erro! " + resposta[1], activity, false)
                }

            } catch (e: Exception) {
                //Verifica o erro e mostra mensagem correspondente
                val erro = e.message
                processaMensagem(ReturnException.mostraErroConexaoGeral(erro!!), activity, false)
            }
        }).start()
    }

    private fun processaMensagem(msg: String, context: Context, sucesso: Boolean) {
        mHandler.post {
            Mensagem.exibeMensagem(context, "Aviso", msg, sucesso)

            if (mProgressDialog.isShowing)
                mProgressDialog.dismiss()
        }
    }

    @Suppress("DEPRECATION")
    private fun criaProgress(): ProgressDialog {
        mProgressDialog = ProgressDialog(activity)
        mProgressDialog.setTitle("Aguarde")
        mProgressDialog.setMessage("Testando conexão...")
        mProgressDialog.isIndeterminate = true
        mProgressDialog.setCancelable(true)

        return mProgressDialog
    }


    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        // Infla o menu; este adiciona items para a action bar se está existe.
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_save, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        //Clicks do toolbar
        val id = item!!.itemId

        //Salvar
        if (id == R.id.action_save) {
            inserir()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        private val MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0
        val WEB_SERVICE = "WEB_SERVICE"
        val CAMINHO = "caminho"
        val PORTA = "porta"
        val USUARIO = "usuario"
        val IDAPARELHO = "idaparelho"

        fun mostrarConfiguracoesdoAPP(context: Activity?) {
            if (context == null) {
                return
            }
            val i = Intent()
            i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            i.addCategory(Intent.CATEGORY_DEFAULT)
            i.data = Uri.parse("package:" + context.packageName)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            context.startActivity(i)
        }
    }
}
