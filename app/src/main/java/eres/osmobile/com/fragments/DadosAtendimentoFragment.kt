package eres.osmobile.com.fragments


import android.content.Intent
import android.database.MatrixCursor
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import com.android.volley.Request
import eres.osmobile.com.R
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.adapter.SearchFeedResultsAdaptor
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.interfaces.RecyclerViewOnClickListenerHack
import eres.osmobile.com.interfaces.VolleyResponseListener
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.VolleyRequest
import eres.osmobile.com.model.Atendimento
import eres.osmobile.com.model.Cliente
import eres.osmobile.com.model.Status
import eres.osmobile.com.viewPagers.CadastroAtendimento
import kotlinx.android.synthetic.main.content_add_atendimento.*
import kotlinx.android.synthetic.main.fragment_add_atendimento.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DadosAtendimentoFragment : Fragment(), View.OnClickListener, View.OnFocusChangeListener, VolleyResponseListener {

    private var mSearchViewAdapter: SearchFeedResultsAdaptor? = null
    private var columns = arrayOf("_id", "NomedoCliente", "Status")
    private var clienteList: List<Cliente>? = null
    var statusList: List<Status>? = null
    private var tamanhoPesquisa = 0
    private var cliente: Cliente? = null
    private var posicaoStatusExistente = 0
    private var dateTimeEntrada = Date()
    private var dateTimeEntrega = Date()
    private var mostrarSugestoes = false
    var mRecyclerViewOnClickListenerHack: RecyclerViewOnClickListenerHack? = null

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fab_produto.setOnClickListener(this)
        fab_servicos.setOnClickListener(this)
        edt_data_entrada.setOnClickListener(this)
        edt_data_previsao.setOnClickListener(this)
        edt_data_entrada.onFocusChangeListener = this
        edt_data_previsao.onFocusChangeListener = this

        //SearchView Adapter com Feed de Resultado
        mSearchViewAdapter = SearchFeedResultsAdaptor(context, R.layout.sugestoes_layout, null, columns, null, -1000)
        edt_cliente.setAdapter(mSearchViewAdapter)
        // Para mostrar sugestões apartir de 3 caractere
        edt_cliente.threshold = 3
        // Como mostrarSugestoes ainda é false não vai mostrar as sugestões assim que entra (Ao editar)
        // How to mostrarSugestoes is still false will not show as suggestions as soon as it enters (When editing)
        mostrarSuggestoesCliente()

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // To activate the top menu
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_add_atendimento, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeValues()
        preencherComAtendimentosExistente()
        mostrarSugestoes = true
        MetodosemComum.funcionarOnBackPressEdicao()
    }

    override fun onResume() {
        super.onResume()
        ativarFab()
        calcularValorEstimados()
    }

    override fun onPause() {
        super.onPause()
        if (CadastroAtendimento.idAtendimento !=0) {
            montarAtendimento()
        }
    }

    private fun preencherComAtendimentosExistente() {
        if (CadastroAtendimento.atendimento != null) {
            val atendimento = CadastroAtendimento.atendimento
            edt_cliente.setText(atendimento!!.nome)
            edt_produto.setText(atendimento.produto)
            edt_identificador.setText(atendimento.identificador)
            edt_data_entrada.setText(atendimento.dataEntrada)
            // Alterar forma como mostrar data de previsão
            val data = MetodoEmComum.formatador_data.parse(atendimento.dataPrevisao)
            edt_data_previsao.setText(MetodoEmComum.formatador_datahora.format(data))
            edt_valor_estimado.setText(MetodoEmComum.formatoMoeda(atendimento.valorEstimado))
            edt_problema.setText(atendimento.problema)
            spinner_status.setSelection(posicaoStatusExistente)
        }
    }

    private fun initializeValues() {
        edt_data_entrada.setText(MetodosemComum.formatadorDateTime.format(dateTimeEntrada))
        edt_data_previsao.setText(MetodosemComum.formatadorDateTime.format(dateTimeEntrega))

        MetodosemComum.preencherSpinnerStatus(context, spinner_status, arrayDescricaoStatus())
    }

    private fun arrayDescricaoStatus(): ArrayList<String> {
        val descricoes = ArrayList<String>()
        for (i in 0 until statusList!!.size) {
            descricoes.add(statusList!![i].descricao!!.toUpperCase())
            verificarPosicaoStatusExistente(statusList!![i].status_id, i)
        }
        return descricoes
    }

    // Verificar posicao do spinner para o status do atendimento já existente
    private fun verificarPosicaoStatusExistente(idStatus: Int, posicao: Int) {
        if (CadastroAtendimento.atendimento != null) {
            if (idStatus == CadastroAtendimento.atendimento!!.status) {
                posicaoStatusExistente = posicao
            }
        }
    }

    private fun mostrarSuggestoesCliente() {
        edt_cliente.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //Buscar cliente a partir do terceiro caractere digitado
                // s.toString().length >= tamanhoPesquisa (Serve para não fazer requisição quando estiver apagando o texto)
                when {
                    before != 0 && (s.toString().length > 2 && s.toString().length >= tamanhoPesquisa && mostrarSugestoes) -> {
                        buscarClientes(s.toString().toUpperCase())
                    }
                }
                tamanhoPesquisa = s.toString().length
            }

        })

        edt_cliente.setOnItemClickListener { _, _, position, _ ->
            cliente = clienteList!![position]
            edt_cliente.setText(cliente!!.nome)
        }
    }

    //Monta o URL de pesquisa e verifica se pesquisa é string ou inteiro
    private fun montaURLdePesquisa(pesquisa: String): String {
        val url: String
        var idcliente = "0"
        val codVendedor = "0"
        var cpf = ""
        var nome = ""
        // Pagina é igual a 0 para poder pegar os 5 primeiros
        val pagina = "0"

        //Se só tem números
        if (pesquisa.trim { it <= ' ' }.matches("^[0-9]*$".toRegex())) {
            if (pesquisa.trim { it <= ' ' }.length <= 7) {
                //pesquisar por id
                idcliente = pesquisa.trim { it <= ' ' }
            } else {
                //pesquisar por cpf ou cnpj
                cpf = pesquisa.trim { it <= ' ' }
            }
        } else {
            //Se não; é string
            //Pesquisar por Nome
            nome = pesquisa
        }
        //Busca o id do vendedor

        //Montando URL
        url = "TOrdemServico/Clientes/$idcliente/$codVendedor/$cpf/$nome/$pagina"
        return url
    }

    private fun buscarClientes(texto: String){
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montaURLdePesquisa(texto)),
                null, this, "buscarClientes")
    }

    private fun respostaClientes(resposta: String){
        clienteList = RespostaJson.getListCliente(resposta)
        // Back to the main thread (Retorna para a thread main)
        if (clienteList != null) {
            val matrixCursorClientes = convertToCursor(clienteList!!)
            mSearchViewAdapter!!.changeCursor(matrixCursorClientes)
        }
    }

    //Cliente para array string, para cursor
    private fun convertToCursor(listacliente: List<Cliente>): MatrixCursor? {
        val cursor = MatrixCursor(columns)
        for (cliente in listacliente) {
            val temp = arrayOfNulls<String>(3)
            temp[0] = cliente.IDcli.toString()
            temp[1] = cliente.nome.toString()
            temp[2] = "cod.: ${cliente.IDcli}"
            cursor.addRow(temp)
        }
        return cursor
    }

    private fun dataEntrada(): Date {
        val dataEntrada: String = edt_data_entrada.text.toString()
        dateTimeEntrada = MetodosemComum.formatadorDateTime.parse(dataEntrada)
        return dateTimeEntrada
    }

    private fun dataPrevisao(): Date {
        val dataPrevisao: String = edt_data_previsao.text.toString()
        dateTimeEntrega = MetodosemComum.formatadorDateTime.parse(dataPrevisao)
        return dateTimeEntrega
    }

    private fun verificarCamposAPreencher(): Boolean {
        var passa = true
        when {
            edt_cliente.text.toString().isEmpty() -> {
                passa = false
                edt_cliente.error = "Obrigatório"
            }
            edt_produto.text.toString().isEmpty() -> {
                passa = false
                edt_produto.error = "Obrigatório"
            }
        }
        return passa
    }

    private fun montarAtendimento(): Atendimento {
        val atendimento = Atendimento()
        if (CadastroAtendimento.idAtendimento != 0) {
            atendimento.idatd = CadastroAtendimento.idAtendimento
        }
        atendimento.status = statusList!![spinner_status.selectedItemPosition].status_id
        atendimento.problema = edt_problema.text.toString()
        val formatadorDateTime = SimpleDateFormat("dd/MM/yyyy", Locale.US)
        val data = formatadorDateTime.parse(edt_data_previsao.text.toString())
        atendimento.dataPrevisao = formatadorDateTime.format(data)
        atendimento.dataEntrada = edt_data_entrada.text.toString()
        atendimento.identificador = edt_identificador.text.toString()
        atendimento.usuario = MetodosemComum.getUsuarioSessao(context).Nome
        atendimento.produto = edt_produto.text.toString()

        if (CadastroAtendimento.atendimento != null) {
            val atendimentoPrincical = CadastroAtendimento.atendimento
            atendimento.clienteID = atendimentoPrincical!!.clienteID
            atendimento.nome = atendimentoPrincical.nome
            atendimento.nomeFantasia = atendimentoPrincical.nomeFantasia
            atendimento.cnpj_cpf = atendimentoPrincical.cnpj_cpf
            atendimento.valorTotal = CadastroAtendimento.atendimento!!.valorTotal
            atendimento.valorEstimado = CadastroAtendimento.atendimento!!.valorEstimado
        } else {
            atendimento.clienteID = cliente!!.IDcli!!
            atendimento.nome = cliente!!.nome
            atendimento.nomeFantasia = cliente!!.xfant
            atendimento.cnpj_cpf = cliente!!.xcnpj_cpf
            atendimento.valorTotal = 0.toDouble()
            atendimento.valorAcrescimo = 0.toDouble()
            atendimento.valorDesconto = 0.toDouble()
            atendimento.valorEstimado = 0.toDouble()
        }
        verificarMudancaAtendimento(atendimento)

        CadastroAtendimento.atendimento = atendimento

        return atendimento
    }

    private fun verificarMudancaAtendimento(atendimento: Atendimento) {
        // Para a nossa alegria
        if (CadastroAtendimento.atendimento != null) {
            val atendimentoBase = CadastroAtendimento.atendimento
            when {
                atendimento.clienteID != atendimentoBase!!.clienteID -> CadastroAtendimento.mudancaAtendimento = true
                !CadastroAtendimento.mudancaAtendimento && atendimento.dataEntrada != atendimentoBase.dataEntrada -> CadastroAtendimento.mudancaAtendimento = true
                !CadastroAtendimento.mudancaAtendimento && atendimento.dataPrevisao != atendimentoBase.dataPrevisao -> CadastroAtendimento.mudancaAtendimento = true
                !CadastroAtendimento.mudancaAtendimento && atendimento.identificador != atendimentoBase.identificador -> CadastroAtendimento.mudancaAtendimento = true
                !CadastroAtendimento.mudancaAtendimento && atendimento.problema != atendimentoBase.problema -> CadastroAtendimento.mudancaAtendimento = true
                !CadastroAtendimento.mudancaAtendimento && atendimento.status != atendimentoBase.status -> CadastroAtendimento.mudancaAtendimento = true
                !CadastroAtendimento.mudancaAtendimento && atendimento.usuario != atendimentoBase.usuario -> CadastroAtendimento.mudancaAtendimento = true
                else ->{
                    CadastroAtendimento.mudancaAtendimento = false
                }
            }
        } else {
            CadastroAtendimento.mudancaAtendimento = true
        }
    }

    private fun ativarFab() {
        if (CadastroAtendimento.idAtendimento != 0) {
            multiple_actions.visibility = View.VISIBLE
        }
    }

    private fun calcularValorEstimados() {
        if (CadastroAtendimento.mudancaServico || CadastroAtendimento.mudancaItens) {
            var subtotalItens = 0.0
            var subtotalServicos = 0.0
            if (CadastroAtendimento.itensProdutoSelecionados != null) {
                //Percorrer itens
                for (itemProduto in CadastroAtendimento.itensProdutoSelecionados!!) {
                    subtotalItens += itemProduto.preco * itemProduto.quantidade
                }
            }

            if (CadastroAtendimento.servicosSelecionados != null) {
                //Percorrer itens
                for (servico in CadastroAtendimento.servicosSelecionados!!) {
                    subtotalServicos += servico!!.valor
                }
            }
            if (CadastroAtendimento.atendimento != null) {
                CadastroAtendimento.atendimento!!.valorEstimado = subtotalServicos + subtotalItens
                CadastroAtendimento.atendimento!!.valorTotal = subtotalServicos + subtotalItens
            }

            edt_valor_estimado.setText(MetodoEmComum.formatoMoeda(subtotalServicos + subtotalItens))
        } else {
            if (CadastroAtendimento.atendimento != null) {
                edt_valor_estimado.setText(MetodoEmComum.formatoMoeda(CadastroAtendimento.atendimento!!.valorTotal))
            }
        }
    }

    private fun direcionarFragments(fragment: Fragment, titulo: String) {
        val i =  Intent(activity, ActEdicao::class.java)
        ActEdicao.fragment = fragment
        ActEdicao.tituloFragmento = titulo
        startActivity(i)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        // Infla o menu; este adiciona items para a action bar se está existe.
        menu!!.clear()
        inflater!!.inflate(R.menu.menu_save, menu)
        onResume()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_save -> {
                if (verificarCamposAPreencher()) {
                    montarAtendimento()
                    mRecyclerViewOnClickListenerHack!!.onAlterar()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResponse(response: Any, tag: String) {
        if (tag == "buscarClientes"){
            respostaClientes(response.toString())
        }
    }

    override fun onError(message: String, tag: String) {

    }


    override fun onClick(v: View?) {
        when (v) {
            edt_data_entrada -> MetodosemComum.showDataandTimePricker(context, dataEntrada(), edt_data_entrada)
            edt_data_previsao -> MetodosemComum.showDataandTimePricker(context, dataPrevisao(), edt_data_previsao)
            fab_servicos -> {
                direcionarFragments(ServicosListFragment(), "Serviços")
            }
            fab_produto -> {
                direcionarFragments(ProdutosListFragment(), "Produtos")
            }
        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (hasFocus) {
            when (v) {
                edt_data_entrada -> MetodosemComum.showDataandTimePricker(context, dataEntrada(), edt_data_entrada)
                edt_data_previsao -> MetodosemComum.showDataandTimePricker(context, dataPrevisao(), edt_data_entrada)
            }
        }
    }
}




