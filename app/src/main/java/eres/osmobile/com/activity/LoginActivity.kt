package eres.osmobile.com.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.google.gson.Gson
import eres.osmobile.com.R
import eres.osmobile.com.controller.Mensagem
import eres.osmobile.com.controller.ReturnException
import eres.osmobile.com.controller.Start
import eres.osmobile.com.fragments.ConfiguracaoFragment
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.WebServiceCliente
import org.json.JSONException
import java.io.IOException
import java.net.MalformedURLException
import java.text.SimpleDateFormat
import java.util.*

class LoginActivity : Activity(), View.OnClickListener, TextView.OnEditorActionListener {

    private var mBtnLogin: Button? = null
    private var mEdtUsuario: EditText? = null
    private var mEdtSenha: EditText? = null
    private var txtConfigurar: TextView? = null
    private var mTxtVersaoSistema: TextView? = null
    private var mTxtDevelopedBy: TextView? = null
    private var mTxtEres: TextView? = null
    private var mChkLembrarMe: CheckBox? = null
    private var ctlCorpo: ConstraintLayout? = null
    private var ctlTopo: ConstraintLayout? = null
    private var mProgressDialog: ProgressDialog? = null
    private var mHandler: Handler? = null
    private val loginTemporario = "LoginTemporario"
    private lateinit var sharedpreferences: SharedPreferences
    private lateinit var sharedpreferences2: SharedPreferences
    private val corview: Int = 0
    private val corcorpo: Int = 0
    private val cortexto: Int = 0
    private val cortitulo: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        Start.preencherCaminho(applicationContext)
        //Verificar se já está configuração do web service
        if (Start.CAMINHO == "") {
            val fragment = ConfiguracaoFragment()
            val i = Intent(this, ActEdicao::class.java)
            ActEdicao.fragment = fragment
            ActEdicao.tituloFragmento = "Configurações"
            startActivity(i)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        iniciaListiners()
    }

    private fun iniciaListiners() {
        val font = Typeface.createFromAsset(assets, "MyriadPro-Regular.otf")
        mHandler = Handler()
        mTxtVersaoSistema = findViewById<View>(R.id.telalogin_txtVersaoSistema) as TextView
        mTxtDevelopedBy = findViewById<View>(R.id.telalogin_txtDevelopedBy) as TextView
        mTxtEres = findViewById<View>(R.id.telalogin_txtEresInformatica) as TextView
        txtConfigurar = findViewById<View>(R.id.configurar) as TextView
        mChkLembrarMe = findViewById<View>(R.id.telalogin_LembraLoginnBox) as CheckBox
        mEdtUsuario = findViewById<View>(R.id.telalogin_Usuario) as EditText
        mEdtSenha = findViewById<View>(R.id.telalogin_Senha) as EditText
        mBtnLogin = findViewById<View>(R.id.telalogin_btnLogin) as Button
        ctlCorpo = findViewById<View>(R.id.ctl_corpo) as ConstraintLayout
        ctlTopo = findViewById<View>(R.id.constraintLayout) as ConstraintLayout
        mEdtSenha!!.setOnEditorActionListener(this)

        mBtnLogin!!.setOnClickListener(this)
        txtConfigurar!!.setOnClickListener(this)
        // Aplicando fonte
        mBtnLogin!!.typeface = font
        mTxtVersaoSistema!!.typeface = font
        mTxtDevelopedBy!!.typeface = font
        mTxtEres!!.typeface = font
        mEdtUsuario!!.typeface = font
        mEdtSenha!!.typeface = font
        preencherLoginSalvo()
    }

    //Login salvo no Shared Preference
    private fun preencherLoginSalvo() {
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE) //1
        val nome = sharedpreferences.getString(LNome, null)
        val senha = sharedpreferences.getString(LSenha, null)
        if (nome != null) {
            mEdtUsuario!!.setText(nome)
            mEdtSenha!!.setText(senha)
            mChkLembrarMe!!.isChecked = true
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun autenticar(usuario2: String, senha2: String) {
        val formatador = SimpleDateFormat("dd")
        object : Thread() {

            override fun run() {
                if (usuario2.equals("eres", ignoreCase = true) && senha2.equals(formatador.format(Date(
                        System.currentTimeMillis())) + "138", ignoreCase = true)) {
                    inicializa()
                } else {
                    verificarCliente(usuario2, senha2)
                }
            }

        }.start()
    }

    private fun verificarCliente(usuario: String, senha: String) {
        //Acessando a preferencia salvas internamente
        sharedpreferences = this.getSharedPreferences(loginTemporario, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = sharedpreferences.edit()
        editor.putString(LNome, usuario)
        editor.putString(LSenha, senha)
        editor.apply()
        editor.commit()

        Start.setAutentication(applicationContext)

        try {
            buscaUsuariosnoWeb(senha)
            guardarLogin()
            inicializa()
        } catch (e: ReturnException) {
            mensagem(this, e.mensagem, false)
        } catch (e: MalformedURLException) {
            //Se erro contem "End of input" é erro de usuário e senha
            //Se não é erro de conexão.
            if (e.message!!.contains("End of input")) {
                mensagem(this, "Usuário ou senha incorreta! ", false)
            } else {
                mensagem(this, "Erro na conexão! ", false)
                e.printStackTrace()
            }
        } catch (e: NullPointerException) {
            if (e.message!!.contains("End of input")) {
                mensagem(this, "Usuário ou senha incorreta! ", false)
            } else {
                mensagem(this, "Erro na conexão! ", false)
                e.printStackTrace()
            }
        } catch (e: JSONException) {
            if (e.message!!.contains("End of input")) {
                mensagem(this, "Usuário ou senha incorreta! ", false)
            } else {
                mensagem(this, "Erro na conexão! ", false)
                e.printStackTrace()
            }
        } catch (e: IOException) {
            messagemConect("Não foi possível conectar-se ao web-service! \nDeseja configurá-lo?")
            e.printStackTrace()
        } catch (e: Exception) {
            mensagem(this, "Usuário não cadastrado", false)
            e.printStackTrace()
        }

    }

    @Throws(IOException::class, ReturnException::class, JSONException::class)
    private fun buscaUsuariosnoWeb(senha: String) {
        //Requisição com resposta dos vendedores
        val resposta = WebServiceCliente(this)["TOrdemServico/DadosVendedor"]
        val sessao = RespostaJson.getSessao(resposta[1], senha)
        //Passando sessão no json
        val gson = Gson()
        val sessaojson = gson.toJson(sessao)

        //Acessando a preferencia salvas internamente
        sharedpreferences = this.getSharedPreferences(SESSAO, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = sharedpreferences.edit()
        //Lipando sessão do sharedpreference
//        editor.clear()
        //Salvando sessão no sharedpreference
        editor.putString("sessao", sessaojson)
        editor.apply()
        editor.commit()
    }

    fun mensagem(context: Context, mensagem: String, sucesso: Boolean) {
        mHandler!!.post { Mensagem.exibeMensagem(context, "Aviso", mensagem, sucesso) }
        mProgressDialog!!.dismiss()
    }

    fun inicializa() {
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
        mProgressDialog!!.dismiss()
        finish()
    }

    @Suppress("NAME_SHADOWING")
    private fun messagemConect(mensagem: String) {
        mHandler!!.post {
            val dialog = AlertDialog.Builder(
                    this@LoginActivity)
            dialog.setMessage(mensagem)
            dialog.setIcon(R.drawable.interroga)
            dialog.setPositiveButton("Sim"
            ) { _, _ ->
                val i = Intent(this@LoginActivity,
                        ConfiguracaoFragment::class.java)
                startActivity(i)
            }
            dialog.setNeutralButton("Não"
            ) {  dialog, _ -> dialog.cancel() }
            dialog.setTitle("Aviso")
            dialog.show()
        }
        mProgressDialog!!.dismiss()
    }


    private fun autenticacao() {
        val usuario = mEdtUsuario!!.text.toString()
        val senha = mEdtSenha!!.text.toString()

        //guardarLogin();

        @Suppress("DEPRECATION")
        mProgressDialog = ProgressDialog(this)
        mProgressDialog!!.setMessage("Autenticando...")
        mProgressDialog!!.progress = 0
        try {
            mProgressDialog!!.show()
        } catch (ignored: Exception) {
        }

        autenticar(usuario, senha)
    }

    private fun guardarLogin() {
        val usuario = mEdtUsuario!!.text.toString()
        val senha = mEdtSenha!!.text.toString()
        //Acessando a preferencia salvas internamente
        sharedpreferences = this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = sharedpreferences.edit()

        if (mChkLembrarMe!!.isChecked) {
            editor.putString(LNome, usuario)
            editor.putString(LSenha, senha)
            editor.putString(LChecked, "S")
            editor.apply()
            editor.commit()
        } else {
            editor.clear()
            editor.apply()
            editor.commit()
        }
    }

    override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
        autenticacao()
        return true
    }

    override fun onClick(v: View) {
        if (v === mBtnLogin) {
            autenticacao()
        } else if (v === txtConfigurar) {
            //Ir para tela configurar
            val fragment = ConfiguracaoFragment()
            val i = Intent(this, ActEdicao::class.java)
            ActEdicao.fragment = fragment
            ActEdicao.tituloFragmento = "Configurações"
            startActivity(i)
        }
    }

    companion object {
        val MyPREFERENCES = "MeuLogin"
        val LNome = "LNome"
        val LSenha = "LSenha"
        val LChecked = "LChecked"
        val SESSAO = "Sessao"
    }
}
