package eres.osmobile.com.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView

import eres.osmobile.com.R
import eres.osmobile.com.controller.BuscarAdvertisingIdCliente
import eres.osmobile.com.controller.MetodoEmComum

class Splash : Activity() {

    private var mIsBackPressed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val font = Typeface.createFromAsset(assets, "MyriadPro-Regular.otf")
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_splash)

        val mTxtLabel = findViewById<View>(R.id.splash_Label) as TextView
        mTxtLabel.typeface = font

        val mSplashTread = object : Thread() {
            override fun run() {
                try {
                    BuscarAdvertisingIdCliente(applicationContext).execute()
                    Thread.sleep(3000)
                    Log.i("ADVERT", "run: " + MetodoEmComum.idDispositivo!!)
                    if (!mIsBackPressed) {

                        val myIntent = Intent(this@Splash, LoginActivity::class.java)
                        startActivity(myIntent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

                    }

                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {

                    finish()

                }
            }

        }

        mSplashTread.start()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            mIsBackPressed = true
            finish()
        }
        return super.onKeyDown(keyCode, event)

    }
}
