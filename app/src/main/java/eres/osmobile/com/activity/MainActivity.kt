package eres.osmobile.com.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView

import com.google.gson.Gson

import eres.osmobile.com.R
import eres.osmobile.com.fragments.AtendimentosFragment
import eres.osmobile.com.fragments.ConfiguracaoFragment
import eres.osmobile.com.fragments.ListaIdentificadoresTabelasListFragment
import eres.osmobile.com.fragments.TabelaCheckListFragment
import eres.osmobile.com.model.Sessao

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    internal lateinit var toolbar: Toolbar
    private lateinit var navigationView: NavigationView
    private lateinit var containerbody: FrameLayout
    internal var context: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        containerbody = findViewById<View>(R.id.containerbody) as FrameLayout

        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)


        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        //Mostrar usuário no navigation (Menu Lateral)
        mostrarUsuarioMenuLateral()
        //Mostrar Fragment
        mostrarFragment(AtendimentosFragment())

    }


    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var fragment: Fragment? = null
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.atendimentos -> fragment = AtendimentosFragment()
            R.id.checklists -> fragment = TabelaCheckListFragment()
            R.id.abertas -> fragment = ListaIdentificadoresTabelasListFragment()
            R.id.configuracoes -> fragment = ConfiguracaoFragment()
        }

        //Esconde menu lateral quando clicado
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)

        //Define o fragmento a ser executado
        if (fragment != null) {
            mostrarFragment(fragment)
        }
        return true
    }


    private fun mostrarFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerbody, fragment)
        fragmentTransaction.commit()
    }

    @SuppressLint("SetTextI18n")
    private fun mostrarUsuarioMenuLateral() {
        // Obter a view que guarda o textview para exibir as informações (id/usuario)
        val header = navigationView.getHeaderView(0)
        // Obter o arquivo que guarda os dados armazenados
        val sharedPreferences = getSharedPreferences(LoginActivity.SESSAO, Context.MODE_PRIVATE)
        // PEGAR O ID E NOME
        // Declarar um objeto JSON para abstrarir objeto
        val gson = Gson()
        val usuario: Sessao
        val teste = sharedPreferences.getString("sessao", null)
        try {
            // Pessa Sessão do gson
            usuario = gson.fromJson(sharedPreferences.getString("sessao", null), Sessao::class.java)

            //Atribuir os dados recuperados ao textview
            (header.findViewById<View>(R.id.idUsuario) as TextView).text = "${usuario.id} - ${usuario.Nome}"
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
