package eres.osmobile.com.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import eres.osmobile.com.R
import eres.osmobile.com.controller.MetodosemComum

class ActEdicao : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.app_bar_main)

        //aplicarCordeFundo();

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        val tabLayout = findViewById<View>(R.id.tabs) as TabLayout

        setSupportActionBar(toolbar)

        supportActionBar!!.title = tituloFragmento
        //Essa linha é responsável por ativar o botão voltar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        // Aplicando cor na toolbar.
        //corToolbar(toolbar);

        MetodosemComum.tabLayout = tabLayout
        //Enviando variavel que é para ver se é novo cliente Edição Cliente
        val bundle = Bundle()
        bundle.putBoolean("addnovo", addnovo)
        fragment!!.arguments = bundle

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerbody, fragment)
        fragmentTransaction.commit()

    }

    override fun onBackPressed() {
        if (fragment != null) {
            //user defined onBackPressed method. Not of Fragment.
            fragment!!.onDetach()
        } else {
            //this will pass BackPress event to activity. If not called, it will
            //prevent activity to get BackPress event.
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        fragment = null
        tituloFragmento = ""
        addnovo = false
        super.onDestroy()
    }

    companion object {

        var fragment: Fragment? = null
        lateinit var tituloFragmento: String
        var addnovo: Boolean = false
    }
}
