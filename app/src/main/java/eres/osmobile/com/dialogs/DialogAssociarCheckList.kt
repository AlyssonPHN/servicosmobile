package eres.osmobile.com.dialogs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import com.android.volley.Request
import com.google.gson.Gson
import eres.osmobile.com.R
import eres.osmobile.com.adapter.Adapter_SugestoesAtendimento
import eres.osmobile.com.adapter.SearchFeedResultsAdaptor
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.Progressos
import eres.osmobile.com.controller.ReturnException
import eres.osmobile.com.interfaces.AcoesLiteners
import eres.osmobile.com.interfaces.AtendimentoListener
import eres.osmobile.com.interfaces.VolleyResponseListener
import eres.osmobile.com.json.RespostaJson
import eres.osmobile.com.json.VolleyRequest
import eres.osmobile.com.model.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

//class DialogAssociarCheckList(var context: Context, var acoesLiteners: AcoesLiteners , private val identificadorTabela: IdentificadorTabelas?) : View.OnClickListener, VolleyResponseListener, AtendimentoListener {
class DialogAssociarCheckList(var context: Context, var acoesLiteners: AcoesLiteners, private val identificadorTabela: IdentificadorTabelas?) : View.OnClickListener, VolleyResponseListener, AtendimentoListener {

    private lateinit var mDialog: Dialog
    private var scroll: RecyclerView.OnScrollListener? = null

    private lateinit var ctl_topo: ConstraintLayout
    private var searchView: SearchView? = null
    private lateinit var btnCancelar: Button
    private lateinit var btnSalvar: Button
    private lateinit var ctl_botoes: ConstraintLayout
    private var mSearchViewAdapter: SearchFeedResultsAdaptor? = null
    private lateinit var recyclerView: RecyclerView
    private var atendimentoList: java.util.ArrayList<Atendimento?> = java.util.ArrayList()
    private var statusList: List<Status>? = null
    private var carregouStatus: Boolean = false
    private var naoRepetirScrolled: Boolean = false
    private var adapter = Any()
    private var pagina = 1
    private var pesquisa: String = ""
    private var atendimento: Atendimento? = null

    init {
        buscarStatus()
    }


    @SuppressLint("InflateParams")
    fun mostrar() {
        mDialog = Dialog(context)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setCancelable(true)

        mDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View
        // Se android for acima de Android 4.4
        view = if (Build.VERSION.SDK_INT > 20) {
            inflater.inflate(R.layout.dialog_associar_checklist, null)
        } else {
            inflater.inflate(R.layout.dialog_associar_checklist_api20, null)
        }
        mDialog.setContentView(view)

        ctl_topo = mDialog.findViewById(R.id.ctl_topo)
        ctl_botoes = mDialog.findViewById(R.id.ctl_botoes)
        recyclerView = mDialog.findViewById(R.id.recyclerView)
        searchView = mDialog.findViewById(R.id.searchView)
        searchView!!.setOnSearchClickListener(this)
        btnCancelar = mDialog.findViewById(R.id.btnCancelar)
        btnSalvar = mDialog.findViewById(R.id.btnSalvar)
        btnCancelar.setOnClickListener(this)
        btnSalvar.setOnClickListener(this)

        // Configuração recyclerview
        //Seta configuração da re cyclerview
        val llmanager = LinearLayoutManager(context)
        llmanager.orientation = LinearLayout.VERTICAL
        recyclerView.layoutManager = llmanager

        ativarSearchView()
        scrollInfinito()

        mDialog.show()
    }


    private fun scrollInfinito() {
        scroll = object : RecyclerView.OnScrollListener() {

            override fun onScrolled(mRecyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(mRecyclerView, dx, dy)

                val llm: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (atendimentoList.size == llm.findLastVisibleItemPosition() + 1) {
                    // Verifica se houve mudança no intervalo de ítens em Y
                    if (dy > 0) {
                        //Fazer um list contato com mais 20
                        adicionarNovosAtendimentos()
                    } else if (!naoRepetirScrolled) {
                        doAsync {
                            uiThread {
                                // Se não houve mudança e está no fim da página
                                removerProgressdaLista()
                            }
                        }
                        // impedir de chamar novamente o scroll infinito
                        recyclerView.removeOnScrollListener(scroll)
                    }
                }
            }
        }
    }

    private fun adicionarProgressnaLista() {
        atendimentoList.add(null)
        // Additional item in the recycleview mode adapter
        (adapter as Adapter_SugestoesAtendimento).notifyItemInserted(atendimentoList.size - 1)
    }


    private fun removerProgressdaLista() {
        naoRepetirScrolled = true
        try {
            //Check that the last position is not a service, so that you can exclude
            if (atendimentoList[atendimentoList.lastIndex] == null) {
                // Remove progress from the recyclerview
                try {
                    atendimentoList.removeAt(atendimentoList.lastIndex)
                    (adapter as Adapter_SugestoesAtendimento).notifyItemRemoved(atendimentoList.size)
                } catch (e: Exception) {

                }

            }
        } catch (ignored: Exception) {

        }
    }

    private fun ativarSearchView() {
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            // Pesquisa depois de digitar o texto e clicar no botão de pesquisar
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query!!.length > 1) {
                    pesquisa = query
                    buscarAtendimentos()
                }
                return false
            }

            // Pesquisa enquanto digita o texto
            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText!!.length > 1) {
                    pesquisa = newText
                    buscarAtendimentos()
                } else if (newText == "") {
                    pesquisa = newText
                    atendimentoList.clear()
                    searchView!!.isIconified = true
                    recyclerView.adapter.notifyDataSetChanged()
                }
                return false
            }

        })

        searchView!!.setOnCloseListener {
            mostrarComponentes()
            false
        }


    }

    private fun buscarStatus() {
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST("TOrdemServico/Status"), null, this, "buscaStatus")
    }

    private fun buscarAtendimentos() {
        pagina = 1
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montaURLdePesquisa()), null,
                this, "buscarAtendimentos")
    }

    private fun respostaAtendimento(resposta: String) {
        atendimentoList = RespostaJson.getListAtendimento(resposta)

        adapter = Adapter_SugestoesAtendimento(context, this, atendimentoList, statusList)
        recyclerView.adapter = adapter as Adapter_SugestoesAtendimento
        atendimentoList.add(null)
        naoRepetirScrolled = false
        recyclerView.addOnScrollListener(scroll)

    }

    private fun adicionarNovosAtendimentos() {
        pagina++
        VolleyRequest.stringRequest(context, Request.Method.GET,
                MetodoEmComum.trataEspacoemREST(montaURLdePesquisa()), null,
                this, "adicionarAtendimentos")
    }

    private fun respostaAdicionarAtendimentos(response: String) {
        val listAtendimento = RespostaJson.getListAtendimento(response)
        removerProgressdaLista()

        if (listAtendimento.size > 0) {
            for (i in 0 until listAtendimento.size) {
                (adapter as Adapter_SugestoesAtendimento).addItemList(listAtendimento[i], atendimentoList.size)
            }
            adicionarProgressnaLista()
            naoRepetirScrolled = false
        } else {
            recyclerView.removeOnScrollListener(scroll)
        }
    }


    /**
     * @param pesquisa - deve pesquisar por
     * * Nome do Cliente e identificador (juntos)
     * * cpf_cnpj
     * * Id do atendimento
     * @return url
     */
    fun montaURLdePesquisa(): String {
        //variáveis de contrução da url
        val url: String
        // Variáveis
        val clienteID = 0
        val nomeCliente = ""
        var nomeEidentificador = ""
        var cpf_cnpj = ""
        val usuario = ""
        val status = ""
        val dataEntrada = ""
        var atendimentoID = ""
        val identificador = ""
        val dataEntrada_inicio = ""
        val dataEntrada_final = ""

        if (!pesquisa.isEmpty()) {
            // Se só tem números
            if (pesquisa.trim { it <= ' ' }.matches("^[0-9]*$".toRegex())) {
                if (pesquisa.trim { it <= ' ' }.length <= 8) {
                    // Pesquisar por ID do ATENDIMENTO
                    atendimentoID = pesquisa.trim { it <= ' ' }
                } else {
                    // Pesquisar por CPF/CNPJ
                    cpf_cnpj = pesquisa.trim { it <= ' ' }
                }
            } else {
                //Se não; é string
                //Pesquisar por Nome
                nomeEidentificador = pesquisa
            }
        }


        url = "TOrdemServico/Atendimentos/" + clienteID + "/" + nomeCliente + "/" + nomeEidentificador + "/" +
                cpf_cnpj + "/" + usuario + "/" + status + "/" + atendimentoID + "/" + dataEntrada +
                "/" + dataEntrada_inicio + "/" + dataEntrada_final + "/" + identificador +
                "/" + pagina
        // limpar atendimentoid
        atendimentoID = ""
        return url
    }

    private fun esconderComponentes() {
        ctl_topo.visibility = View.GONE
        ctl_botoes.visibility = View.GONE
        // mostrar recyclerview
        recyclerView.visibility = View.VISIBLE
    }

    private fun mostrarComponentes() {
        ctl_topo.visibility = View.VISIBLE
        ctl_botoes.visibility = View.VISIBLE
        // Esconder recyclerview
        recyclerView.visibility = View.GONE
    }

    private fun salvarCheckList() {

        Progressos.criaProgressSemInterromper(context, "Aguarde!", "Enviando Checklist(s)")

        //Adicionando o id do atendimento
        identificadorTabela!!.atendimentoid = atendimento!!.idatd
        val objJson = Gson().toJson(identificadorTabela)
        VolleyRequest.stringRequest(context, Request.Method.POST,
                "TOrdemServico/\"AssociarCheckListAatendimento\"",
                        objJson, this, "salvarCheckList")

    }

    private fun mensagemChecklistEnviado() {
        val mensagem: AlertDialog.Builder = AlertDialog.Builder(context)
        mensagem.setTitle("Sucesso!")
        mensagem.setIcon(R.drawable.sucesso)

        mensagem.setMessage("Enviado com sucesso")
        mensagem.setNeutralButton("OK", { _: DialogInterface, _: Int ->
            mDialog.dismiss()
            acoesLiteners.onResumeInterface()
        })
        mensagem.show()
    }

    private fun mensagemChecklistErro(resposta: String) {
        val mensagem: AlertDialog.Builder = AlertDialog.Builder(context)
        mensagem.setTitle("Erro!")
        mensagem.setIcon(R.drawable.exclamacao)

        mensagem.setMessage("Erro ao enviar!\n ${ReturnException.mostraErroConexaoGeral(resposta)}")
        mensagem.setNeutralButton("OK", null)
        mensagem.show()
    }

    override fun onResponse(response: Any, tag: String) {
        when (tag) {
            "buscarAtendimentos" -> {
                respostaAtendimento(response.toString())
            }
            "buscaStatus" -> {
                carregouStatus = true
                statusList = RespostaJson.getLitStatus(response.toString())
            }
            "adicionarAtendimentos" -> {
                respostaAdicionarAtendimentos(response.toString())
            }
            "salvarCheckList" -> {
                Progressos.fecharProgress()
                mensagemChecklistEnviado()
            }
        }
    }

    override fun onError(message: String, tag: String) {
        when (tag) {
            "buscarAtendimentos" -> {
            }
            "buscaStatus" -> {
                carregouStatus = false
                removerProgressdaLista()
            }
            "salvarCheckList" -> {
                Progressos.fecharProgress()
                mensagemChecklistErro(message)
            }
        }
    }

    override fun onAtendimento(position: Int, atendimento: Atendimento?) {
        mostrarComponentes()
        searchView!!.setQuery(atendimento!!.idatd.toString(), false)
        this.atendimento = atendimento
    }

    override fun onClick(v: View?) {
        when (v) {
            btnCancelar -> {
                mDialog.dismiss()
            }
            btnSalvar -> {
                salvarCheckList()
            }
            searchView -> {
                if (carregouStatus) {
                    ctl_topo.visibility = View.GONE
                    esconderComponentes()
                }
            }
        }

    }

}