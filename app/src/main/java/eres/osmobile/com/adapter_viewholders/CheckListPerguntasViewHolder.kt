package eres.osmobile.com.adapter_viewholders

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.text.Editable
import android.text.InputFilter
import android.text.InputType.*
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import eres.osmobile.com.R
import eres.osmobile.com.controller.MascaraDecimal
import eres.osmobile.com.controller.MetodoEmComum
import eres.osmobile.com.controller.MetodosemComum
import eres.osmobile.com.model.CheckList_Resposta
import eres.osmobile.com.model.Lookup
import java.util.*


class CheckListPerguntasViewHolder(mView: View, val context: Context) : ChildViewHolder(mView), TextWatcher, View.OnClickListener, View.OnFocusChangeListener, AdapterView.OnItemSelectedListener {
    var posicoesPendentes: ArrayList<Int>? = null

    override fun onFocusChange(p0: View?, p1: Boolean) {
        editText.addTextChangedListener(this)
        tipoCampo(mCheckList_resposta!!)
    }

    val perguntaTextView: TextView = itemView.findViewById(R.id.txtPerguntaChecklist)
    private val ctl_texto: ConstraintLayout = itemView.findViewById(R.id.ctl_texto)
    private val editText: EditText = itemView.findViewById(R.id.editText)
    val spinner: Spinner = itemView.findViewById(R.id.spinner)
    val texto: TextView = itemView.findViewById(R.id.texto)
    private val imgtexto: ImageView = itemView.findViewById(R.id.img_texto)
    private var mCheckList_resposta: CheckList_Resposta? = null
    private var dataLocal = Date()
    private var tipomoeda: TextWatcher? = null

    fun pergunta(checkList_Resposta: CheckList_Resposta, date: Date) {
        mCheckList_resposta = checkList_Resposta
        ctl_texto.visibility = View.GONE
        spinner.visibility = View.GONE
        editText.visibility = View.GONE
        verificarPergunta(checkList_Resposta, date)
    }

    init {
        // Para gravar a cada alteração do texto
        editText.onFocusChangeListener = this
        texto.addTextChangedListener(this)
        spinner.onItemSelectedListener = this

    }


    private fun verificarPergunta(checkList_Resposta: CheckList_Resposta, date: Date) =
            when (checkList_Resposta.idtipo) {

            // Data
                12 -> {
                    mostrarTextView()
                    // Alterar formato da data
                    texto.text = MetodoEmComum.formatador_data.format(date)
                    // Alterar imagem
                    imgtexto.setImageResource(R.drawable.ic_calendario)
                    imgtexto.visibility = View.VISIBLE
                    // Ativando o click
                    ctl_texto.setOnClickListener(this)
                }
            // Time
                13 -> {
                    mostrarTextView()
                    // Alterar formato da data
                    texto.text = MetodoEmComum.formatador_hora.format(date)
                    // Alterar imagem
                    imgtexto.setImageResource(R.drawable.ic_relogio)
                    imgtexto.visibility = View.VISIBLE
                    // Ativando o click
                    ctl_texto.setOnClickListener(this)
                }
            // Timestamp
                35 -> {
                    mostrarTextView()
                    // Alterar formato da data
                    texto.text = MetodoEmComum.formatador_datahora.format(date)
                    // Alterar imagem
                    imgtexto.setImageResource(R.drawable.ic_calendario)
                    imgtexto.visibility = View.VISIBLE
                    // Ativando o click
                    ctl_texto.setOnClickListener(this)
                }


                else -> when {
                // Se for lookup mostrar Spinner
                    checkList_Resposta.lookup != null -> {
                        mostrarSpinner()
                        preencherSpinner(checkList_Resposta)
                    }
                // Se não satisfazer nenhum será Edittext
                    else -> {
                        mostrarEdittext()
                        tipomoeda = MascaraDecimal.insert(editText, checkList_Resposta.tamanho_num, -mCheckList_resposta!!.escala)
                        tamanhoEdittext(checkList_Resposta)
                        if (checkList_Resposta.prowess != null) {
                            editText.setText(checkList_Resposta.prowess.toString())
                        } else {
                            editText.setText("")
                        }
                        mostrarErroEdittext()
                    }
                }

            }

    private fun mostrarErroEdittext() {
        if (posicoesPendentes != null) {
            if (posicoesPendentes!!.contains(adapterPosition + 1)) {
                if (editText.text.toString().isEmpty()) {
                    editText.error = "Campo vazio!"
                }
            }
        }
    }

    private fun mostrarEdittext() {
        editText.visibility = View.VISIBLE
        ctl_texto.visibility = View.GONE
        spinner.visibility = View.GONE
    }

    private fun mostrarTextView() {
        ctl_texto.visibility = View.VISIBLE
        editText.visibility = View.GONE
        spinner.visibility = View.GONE
    }

    private fun mostrarSpinner() {
        spinner.visibility = View.VISIBLE
        editText.visibility = View.GONE
        ctl_texto.visibility = View.GONE
    }

    private fun preencherSpinner(checkList_Resposta: CheckList_Resposta) {
        val arraylookup: ArrayList<Lookup> = checkList_Resposta.lookup
        val arrayString: ArrayList<String> = arrayListOf()
        (0 until arraylookup.size).mapTo(arrayString) { arraylookup[it].descricao }
        MetodosemComum.preencherSpinnerStatus(context, spinner, arrayString)

    }

    private fun tamanhoEdittext(checkList_Resposta: CheckList_Resposta) = when {
    // Se for igual a 8 é inteiro e sem tamanho
        checkList_Resposta.idtipo == 8 -> {
            editText.inputType = TYPE_CLASS_NUMBER
        }

        else -> {
            val tamanho: Int = when (checkList_Resposta.idtipo) {
            // Inteiro
                7 -> {
                    checkList_Resposta.tamanho_num
                }
            // Double - INT64
                16 -> {
                    // coloquei qualquer tamanho, pois quem determina o tamanho é a mascara
                    30
                }
                else -> {
                    checkList_Resposta.tamanho_string
                }
            }
            editText.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(tamanho))
        }
    }

    private fun tipoCampo(checkList_Resposta: CheckList_Resposta) {
        when {
        // Quando for inteiro
            checkList_Resposta.idtipo == 8 -> {
                editText.inputType = TYPE_CLASS_NUMBER
                editText.removeTextChangedListener(tipomoeda)
            }

        // Quando for texto
            checkList_Resposta.tamanho_num == 0 -> {
                editText.inputType = TYPE_CLASS_TEXT
                editText.removeTextChangedListener(tipomoeda)
            }
        // Quando for double
            checkList_Resposta.idtipo == 16 -> {
                editText.inputType = TYPE_CLASS_NUMBER or TYPE_NUMBER_FLAG_DECIMAL
                // Ativa máscara de moeda ao digitar
                if (editText.isFocused) {
                    editText.addTextChangedListener(tipomoeda)
                } else {
                    editText.removeTextChangedListener(tipomoeda)
                }
            }

        // Quando for numérico
            else -> {
                editText.inputType = TYPE_CLASS_NUMBER
                editText.removeTextChangedListener(tipomoeda)
            }
        }

    }

    private fun dataEntrada(): Date {
        val dataEntrada: String = texto.text.toString()
        dataLocal = MetodoEmComum.formatador_data.parse(dataEntrada)
        return dataLocal
    }

    private fun horaEntrada(): Date {
        val dataEntrada: String = texto.text.toString()
        dataLocal = MetodoEmComum.formatador_hora.parse(dataEntrada)
        return dataLocal
    }

    private fun dataTimeEntrada(): Date {
        val dataEntrada: String = texto.text.toString()
        dataLocal = MetodosemComum.formatadorDateTime.parse(dataEntrada)
        return dataLocal
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {}

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        mCheckList_resposta!!.prowess = mCheckList_resposta!!.lookup[position].valor
    }

    override fun afterTextChanged(text: Editable?) {
        if (mCheckList_resposta != null) {
            mCheckList_resposta!!.prowess = text.toString().replace("/", ".").replace(",", "")
        }
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun onClick(v: View?) {
        when (v) {
            ctl_texto -> {
                when (mCheckList_resposta!!.idtipo) {
                // DATA
                    12 -> {
                        MetodosemComum.showDataPricker(context, dataEntrada(), texto)
                    }
                // TIME
                    13 -> {
                        MetodosemComum.showTimePricker(context, horaEntrada(), texto)
                    }
                // TIME STAMP
                    35 -> {
                        MetodosemComum.showDataandTimePricker(context, dataTimeEntrada(), texto)
                    }
                }
            }
        }
    }
}

