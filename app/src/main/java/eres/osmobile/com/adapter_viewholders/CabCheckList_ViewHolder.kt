package eres.osmobile.com.adapter_viewholders

import android.view.View
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder

import eres.osmobile.com.R
import eres.osmobile.com.model.CabCheckListPronto

import android.view.animation.Animation.RELATIVE_TO_SELF

class CabCheckList_ViewHolder(itemView: View) : GroupViewHolder(itemView) {

    private val tituloCheckList: TextView = itemView.findViewById(R.id.tituloChecklist)
    private val txtCod: TextView = itemView.findViewById(R.id.txtCod)
    private val arrow: ImageView = itemView.findViewById(R.id.list_item_arrow)

    fun setGenreTitle(genre: ExpandableGroup<*>) {
        if (genre is CabCheckListPronto) {
            tituloCheckList.text = genre.descricao
            txtCod.text = genre._id.toString()
        }
    }

    override fun expand() {
        animateExpand()
    }

    override fun collapse() {
        animateCollapse()
    }

    private fun animateExpand() {
        val rotate = RotateAnimation(360f, 180f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        arrow.animation = rotate

    }

    private fun animateCollapse() {
        val rotate = RotateAnimation(180f, 360f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        arrow.animation = rotate
    }


}
