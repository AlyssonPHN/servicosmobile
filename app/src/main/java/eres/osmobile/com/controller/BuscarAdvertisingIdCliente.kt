package eres.osmobile.com.controller

import android.content.Context
import android.os.AsyncTask

import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException

class BuscarAdvertisingIdCliente(var context: Context) : AsyncTask<Void, Void, String>() {

    override fun doInBackground(vararg voids: Void): String? {
        var idInfo: AdvertisingIdClient.Info? = null
        try {
            idInfo = AdvertisingIdClient.getAdvertisingIdInfo(context)
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        } catch (e: GooglePlayServicesRepairableException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        var advertId: String? = null
        try {
            advertId = idInfo!!.id
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return advertId
    }

    override fun onPostExecute(advertId: String) {
        super.onPostExecute(advertId)
        MetodoEmComum.idDispositivo = advertId
        //            Log.i("ADVERT" + context.getClass().toString(), "onPostExecute: " + advertId);
    }
}

