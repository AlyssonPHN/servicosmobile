package eres.osmobile.com.controller

import android.R
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v7.widget.RecyclerView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.google.gson.Gson
import eres.osmobile.com.activity.ActEdicao
import eres.osmobile.com.activity.LoginActivity
import eres.osmobile.com.model.Sessao
import java.text.SimpleDateFormat
import java.util.*


class MetodosemComum {

    // companion object - tudo que estiver dentro dele, será estatico
    companion object {
        @SuppressLint("StaticFieldLeak")
        var tabLayout: TabLayout? = null
        val formatadorDateTime = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US)
        var formatador_data = SimpleDateFormat("dd/MM/yyyy", Locale.US)
        var formatador_hora = SimpleDateFormat("HH:mm", Locale.US)

        // Show Date and Timer Picker and return object type Date
        fun showDataandTimePricker(context: Context, dateTime: Date, edittext: Any) {
            val cal = Calendar.getInstance()
            cal.time = dateTime
            var dateandTime: Date
            DatePickerDialog(context,
                    DatePickerDialog.OnDateSetListener { _, y, m, d ->
                        cal.set(Calendar.YEAR, y)
                        cal.set(Calendar.MONTH, m)
                        cal.set(Calendar.DAY_OF_MONTH, d)

                        val dataAlterada = "$d/${(m + 1)}/$y"
                        val formatador = SimpleDateFormat("dd/MM/yyyy", Locale.US)
                        dateandTime = formatador.parse(dataAlterada)

                        // now show the time picker
                        TimePickerDialog(context,
                                TimePickerDialog.OnTimeSetListener { _, h, min ->
                                    cal.set(Calendar.HOUR_OF_DAY, h)
                                    cal.set(Calendar.MINUTE, min)
                                    dateandTime = cal.time

                                    // Retorna a resposta
                                    when (edittext) {
                                        is EditText -> edittext.setText(formatadorDateTime.format(dateandTime))
                                        is TextView -> edittext.text = formatadorDateTime.format(dateandTime)
                                    }


                                }, cal.get(Calendar.HOUR_OF_DAY),
                                cal.get(Calendar.MINUTE), true).show()

                    }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
        }

        // Show Date and Timer Picker and return object type Date
        fun showDataPricker(context: Context, dateTime: Date, edittext: Any) {
            val cal = Calendar.getInstance()
            cal.time = dateTime
            var dateandTime: Date
            DatePickerDialog(context,
                    DatePickerDialog.OnDateSetListener { _, y, m, d ->
                        cal.set(Calendar.YEAR, y)
                        cal.set(Calendar.MONTH, m)
                        cal.set(Calendar.DAY_OF_MONTH, d)

                        val dataAlterada = "$d/${(m + 1)}/$y"
                        val formatador = SimpleDateFormat("dd/MM/yyyy", Locale.US)
                        dateandTime = formatador.parse(dataAlterada)
                        // Retorna a resposta
                        when (edittext) {
                            is EditText -> edittext.setText(formatador_data.format(dateandTime))
                            is TextView -> edittext.text = formatador_data.format(dateandTime)
                        }

                    }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
        }

        // Show Date and Timer Picker and return object type Date
        fun showTimePricker(context: Context, dateTime: Date, edittext: Any) {
            val cal = Calendar.getInstance()
            cal.time = dateTime
            var dateandTime: Date

            // now show the time picker
            TimePickerDialog(context,
                    TimePickerDialog.OnTimeSetListener { _, h, min ->
                        cal.set(Calendar.HOUR_OF_DAY, h)
                        cal.set(Calendar.MINUTE, min)
                        dateandTime = cal.time

                        // Retorna a resposta
                        when (edittext) {
                            is EditText -> edittext.setText(formatador_hora.format(dateandTime))
                            is TextView -> edittext.text = formatador_hora.format(dateandTime)
                        }


                    }, cal.get(Calendar.HOUR_OF_DAY),
                    cal.get(Calendar.MINUTE), true).show()


        }

        fun preencherSpinnerStatus(context: Context, spinner: Spinner, descricoes: List<String>) {
            val arrayAdapter = ArrayAdapter<String>(context, R.layout.simple_spinner_item, descricoes)
            arrayAdapter.setDropDownViewResource(eres.osmobile.com.R.layout.support_simple_spinner_dropdown_item)
            //Seta adapter do spinner
            spinner.adapter = arrayAdapter
        }

        fun mostrareSumuirFab(recyclerView: RecyclerView, fab: FloatingActionButton) {
            // Sumir botão flutuante ao rolar
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        fab.show()
                    }

                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0 || dy < 0 && fab.isShown) {
                        fab.hide()
                    }
                }
            })
        }

        fun aplicarStringPesquisa(texto: String): String {
            return if (texto.isEmpty()) {
                ""
            } else {
                texto
            }
        }

        fun getUsuarioSessao(context: Context): Sessao {
            // Obter o arquivo que guarda os dados armazenados
            val sharedPreferences = context.getSharedPreferences(LoginActivity.SESSAO, Context.MODE_PRIVATE)
            // PEGAR O ID E NOME
            // Declarar um objeto JSON para abstrarir objeto
            val gson = Gson()
            var usuario: Sessao? = null
            try {
                // Pessa Sessão do gson
                usuario = gson.fromJson(sharedPreferences.getString("sessao", null), Sessao::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return usuario!!
        }

        fun funcionarOnBackPressEdicao() {
            ActEdicao.fragment = null
        }

    }
}


