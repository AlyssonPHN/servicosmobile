package eres.osmobile.com.controller

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.widget.EditText

import java.text.DecimalFormat


abstract class MascaraDecimal : TextWatcher {
    companion object {

        // Método para tirar a máscara
        fun unmask(valor: String): Double =
                java.lang.Double.parseDouble(valor.replace(".", "").replace(",", "."))

        fun removermascara(valor: String): Double =
                java.lang.Double.parseDouble(valor.replace(".", "").replace(",", ".").replace("R$ ", ""))

        fun insert(edittext: EditText, tamanhoEdittext: Int, casasdecimais: Int): TextWatcher {
            return object : TextWatcher {
                private var isUpdating = false

                override fun onTextChanged(s: CharSequence, start: Int, before: Int,
                                           after: Int) {
                    // Evita que o método seja executado varias vezes.
                    // Se tirar ele entre em loop
                    if (isUpdating) {
                        isUpdating = false
                        return
                    }

                    isUpdating = true
                    // Verifica se já existe a máscara no texto.
                    var str = s.toString()
                    str = str.replace("[^\\d]".toRegex(), "")

                    try {
                        // Retorna valor no formato de moeda Brasil
                        val format = DecimalFormat()
                        //definir casas decimais
                        format.minimumFractionDigits = casasdecimais
                        // Divide valor pela para obter um double segundo as casas decimais
                        // ex:(Double.parseDouble(str) / dividendo(casasdecimais))
                        // resultado: 10002545 / 100 = 1000025.45

                        // Já o format pelo setMinimumFractionDigits, defini quantas casas deicimais,
                        // terá depois do (.), onde o mesmo se tornará (,) ao ser formatodo
                        // resultando: 1.000.025,45
                        str = format.format(java.lang.Double.parseDouble(str) / dividendo(casasdecimais))

                        aplicarTamanhoEditext(str)
                        // Aplica o texto no edittext
                        edittext.setText(str)
                        // Seta a posição do cursor na última posição do texto
                        edittext.setSelection(edittext.text.toString().length)
                    } catch (e: NumberFormatException) {
                        // Se vinher a da erro é que iniciou sem texto
                        isUpdating = false
                    }

                }

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int,
                                               after: Int) {
                    // Não utilizado
                }

                override fun afterTextChanged(s: Editable) {
                    // Não utilizado
                }

                private fun aplicarTamanhoEditext(texto: String) {
                    val tamanhostring = texto.replace("[^\\d]".toRegex(), "").length
                    var tamanho: Int
                    //Se o próximo tamanho de string for menor ou igual do que o tamanho pré-defino
                    //tamanho do edittext será o tamanho da string(com sinais) + 1,
                    // + 1 é para que possa adicionar mais um valor
                    if (tamanhostring + 1 <= tamanhoEdittext) {
                        tamanho = texto.length + 1
                        //Se o dois valores a frente passar do tamanho atual, então permite adicionar
                        // mais dois valores.
                        //Isso evita de uma casa decimal ficar incompleta
                        if (tamanhostring + casasdecimais > tamanhoEdittext) {
                            tamanho = texto.length + casasdecimais
                            //
                        }
                    } else {
                        // Se não for maior e nem igual então será o tamanho dá string já digitada
                        tamanho = texto.length
                    }

                    // Aplica o tamanho do edittext
                    edittext.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(tamanho))
                }

                //Irá definir o dividendo para quantidade de casas decimais ha se dividir
                private fun dividendo(casas_deicimais: Int): Int {
                    var string_valor = "1"
                    for (i in 0 until casas_deicimais) {
                        string_valor += "0"
                    }
                    return Integer.parseInt(string_valor)
                }

            }

        }
    }

}
