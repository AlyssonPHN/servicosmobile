package eres.osmobile.com.controller

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.Handler
import android.util.Log
import android.widget.Toast

import com.google.firebase.crash.FirebaseCrash

import java.util.Timer
import java.util.TimerTask

import eres.osmobile.com.R

object Mensagem {


    //Exime mensagem como caixa de diálogo
    fun exibeMensagem(act: Context, Titulo: String, texto: String, sucesso: Boolean) {

        val handler = Handler()
        handler.post {
            try {
                val mensagem: AlertDialog.Builder = AlertDialog.Builder(act)
                mensagem.setTitle(Titulo)
                if (sucesso) {
                    mensagem.setIcon(R.drawable.sucesso)
                } else {
                    mensagem.setIcon(R.drawable.exclamacao)
                }

                mensagem.setMessage(texto)
                mensagem.setNeutralButton("OK", null)
                mensagem.show()

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }


    fun exibeMensagem(context: Context, Titulo: String, Texto: String, tempo: Int) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle(Titulo)
        builder.setIcon(R.drawable.exclamacao)
        builder.setMessage(Texto)
        builder.setCancelable(true)

        val dlg = builder.create()
        dlg.show()

        val t = Timer()

        t.schedule(object : TimerTask() {
            override fun run() {
                dlg.dismiss()
                t.cancel()
            }
        }, tempo.toLong())
    }

    //Mensagem com caixa de diálogo que permite sair da tela ao clicar em OK
    fun mensagemeSair(activity: Activity, titulo: String, mensagem: String) {
        // Aparece uma caixa de diálogo na Activity atual
        val dialog = AlertDialog.Builder(activity)
        // Título da caixa de diálogo com o Nome de azul
        dialog.setTitle(titulo)
        // Com a mensagem de azul
        dialog.setMessage(mensagem)
        //Adicionando icone
        dialog.setIcon(R.drawable.sucesso)
        // Cria o Botão não na caixa de diálogo
        dialog.setNeutralButton("OK") { _, _ ->
            // Caso o botão não for clicado
            // Finaliza a Activity
            activity.finish()
        }
        // É o que faz a caixa de diálogo ser exibida
        dialog.show()
    }

    fun mandarCrashReporting(tag: String, texto: String, e: Throwable) {
        //Assim gera só um log
        FirebaseCrash.logcat(Log.ERROR, tag, texto)
        //Assim gera um execption completo
        FirebaseCrash.report(e)
    }

    fun toastHandlerCurto(context: Context, mHandler: Handler, menssagem: String) {
        mHandler.post { Toast.makeText(context, menssagem, Toast.LENGTH_SHORT).show() }
    }

    fun toastHandlerLongo(context: Context, mHandler: Handler, menssagem: String) {
        mHandler.post { Toast.makeText(context, menssagem, Toast.LENGTH_SHORT).show() }
    }

    fun situacaoPedido(situacao: Int): String {
        var mensagem = ""

        when (situacao) {
            0 -> mensagem = "Pedido Adicionado."
            1 -> mensagem = "Pedido Adicionado com Bloqueios."
            2 -> mensagem = "Pedido Liberado do Bloqueio."
            3 -> mensagem = "Pedido Parcialmente Atendido."
            4 -> mensagem = "Pedido Completamente Atendido."
            else -> {
            }
        }
        return mensagem
    }
}


