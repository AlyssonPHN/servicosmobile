@file:Suppress("DEPRECATION")

package eres.osmobile.com.controller

/**
 * PARA CONTROLE DOS PROGRESS
 */

import android.app.ProgressDialog
import android.content.Context

object Progressos {


    private var progressDialog: ProgressDialog? = null

    //Progress que não permite ser interrompido
    fun criaProgressSemInterromper(context: Context, titulo: String, mensagem: String) {
        // Cria a barra de progresso na variável do mesmo tipo
        if (progressDialog == null) {
            progressDialog = ProgressDialog(context)
        }
        // Adiciona o Título
        progressDialog!!.setTitle(titulo)
        // Adiciona o Texto
        progressDialog!!.setMessage(mensagem)
        // Indeterminado - isso significa que terminará apenas quando for
        // carregado
        progressDialog!!.isIndeterminate = true
        // Cancelamento
        progressDialog!!.setCancelable(false)
        //Mostra o progresso
        //Fechar progress se ele já estiver aberto
        progressDialog!!.dismiss()
        progressDialog!!.show()


    }

    fun fecharProgress() {
        try {
            progressDialog!!.dismiss()
            progressDialog = null
        } catch (ignored: Exception) {

        }

    }
}
