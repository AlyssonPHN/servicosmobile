package eres.osmobile.com.controller

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import eres.osmobile.com.R
import eres.osmobile.com.model.Status
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object MetodoEmComum {
    private var contador = 0.0
    lateinit var data: Date
    var formatador_data = SimpleDateFormat("dd/MM/yyyy", Locale.US)
    var formatador_datahora = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US)
    var formatador_datahoramin = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US)
    var formatador_hora = SimpleDateFormat("HH:mm", Locale.US)
    var doubleformatter: NumberFormat = DecimalFormat("#0.00")
    var doubleformatterum: NumberFormat = DecimalFormat("#0.0")
    var ptBR = Locale("pt", "BR")
    var moedaFormat = NumberFormat.getCurrencyInstance(ptBR)
    var SEM_ERRO = "SEMERRO"

    var idDispositivo: String? = null

    /////// PARA MOEDAS ///////////////////////
    fun formatoMoeda(valor: Double): String {
        //Se a versão do Android for superior a 4.4, não usa parenteses, na moeda negativa
        return if (Build.VERSION.SDK_INT > 19) {
            //Retorna preco no formato de moeda Brasil
            moedaFormat.format(valor).replace("R$", "R$ ")
        } else {
            //Se a versão do Android for igual ou inferior a 4.4,
            // tira os parenteses e coloca espaço entre o cifrão e o número.
            moedaFormat.format(valor).replace("(R$", "-R$ ").replace(")", "").replace("R$", "R$ ")
        }
    }

    fun formatoMoedasemCifrao(valor: Double): String {
        //Se a versão do Android for superior a 4.4, não usa parenteses, na moeda negativa
        return if (Build.VERSION.SDK_INT > 19) {
            //Retorna preco no formato de moeda Brasil
            moedaFormat.format(valor).replace("R$", "")
        } else {
            //Se a versão do Android for igual ou inferior a 4.4,
            // tira os parenteses e coloca espaço entre o cifrão e o número.
            moedaFormat.format(valor).replace("(R$", "").replace(")", "").replace("R$", "")
        }
    }
    ////// FIM - PARA MOEDAS

    fun camposEmBranco(edtNome: EditText, edtCNPJ_CPF: EditText): String {
        var retorno = ""

        /********** CAMPOS OBRIGATÓRIOS PADRÃO  */
        // //////////////NOME////////////////////////
        try {
            if (edtNome.text.toString().isEmpty())
                retorno += "\n* Nome"
        } catch (ignored: Exception) {

        }

        // //////////////CPF/CNPJ////////////////////////
        if (edtCNPJ_CPF.text.toString().isEmpty())
            retorno += "\n* CNPJ ou CPF"
        /** */

        return retorno
    }

    fun retornarDataAoContrario(data: String): String {
        //Separando em array a data pela barra
        val arrayDataInicial = data.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        //Passando a data ao contrário
        return arrayDataInicial[2] + arrayDataInicial[1] + arrayDataInicial[0]
    }

    private fun mostrarOndate(editText: TextView, mDia: Int,
                              mMes: Int, mAno: Int): DatePickerDialog.OnDateSetListener {

        return DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            val data1 = dayOfMonth.toString() + "/" + (monthOfYear + 1).toString() + "/" + year.toString()
            val formatador = SimpleDateFormat("dd/MM/yyyy", Locale.US)
            //Pega a data escolhida
            try {
                data = formatador.parse(data1)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            //Mostrar data no textview
            editText.text = formatador.format(data)
        }
    }


    fun mostrarOntime(textView: TextView,
                      mHora: Int, mMinuto: Int): TimePickerDialog.OnTimeSetListener {

        return TimePickerDialog.OnTimeSetListener { _, hora, minuto ->
            val horario1 = hora.toString() + ":" + minuto.toString()
            val formatador = SimpleDateFormat("HH:mm", Locale.US)
            //Pega a data escolhida
            try {
                data = formatador.parse(horario1)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            //Mostrar data no textview
            textView.text = formatador.format(data)
        }
    }

    /*
    public static void saberTamanhodaTela(FragmentActivity fragmentActivity) {
        DisplayMetrics metrics = new DisplayMetrics();
        fragmentActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int y = metrics.heightPixels;
        int x = metrics.widthPixels;
    }
    */

    fun trataEspacoemREST(string_com_espaco: String): String {
        //Substitui o espaço da string por " %20 " ///////////////////////////
        val padrao = "\\s"
        val regPat = Pattern.compile(padrao)
        val matcher = regPat.matcher(string_com_espaco)
        //Retorna string tratada
        return matcher.replaceAll("%20")
    }

    fun doubleComDuasCasas(valor: Double?): Double =
            java.lang.Double.parseDouble(doubleformatter.format(valor).replace(",", "."))

    fun doubleComUmaCasa(valor: Double?): Double =
            java.lang.Double.parseDouble(doubleformatterum.format(valor).replace(",", "."))

    fun aumentaQuantidadeProduto(edtquantidade: EditText) {
        try {
            var quantidade = java.lang.Double.valueOf("0" + edtquantidade.text.toString()
                    .replace(",", "."))!!
            quantidade++
            edtquantidade.setText(quantidade.toString())
        } catch (ignored: Exception) {
        }

    }

    fun diminuiQuantidadeProduto(edtquantidade: EditText) {
        try {
            var quantidade = java.lang.Double.valueOf("0" + edtquantidade.text.toString()
                    .replace(",", "."))!!
            if (quantidade > 0) {
                quantidade--
                edtquantidade.setText(quantidade.toString())
            }
        } catch (ignored: Exception) {
        }

    }

    fun aumentaQuantidadeProdutoTxt(txtquantidade: TextView) {
        try {
            var quantidade = java.lang.Double.valueOf("0" + txtquantidade.text.toString()
                    .replace(",", "."))!!
            quantidade++
            txtquantidade.text = quantidade.toString()
        } catch (ignored: Exception) {
        }

    }

    fun aumentaQuantidadeProdutoEdtRetirado(txtquantidade: TextView,
                                            mAutoIncrement: Boolean,
                                            quantideproduto: Double): Double {
        var quantidadeeditada = 0.0
        try {
            val quantidade = java.lang.Double.valueOf("0" + txtquantidade.text.toString()
                    .replace(",", "."))!!

            if (mAutoIncrement) {
                //Modulo, pega o resto da divisão, o último preco do número ex: 1894 preco é 4
                val valor = quantidade % 10
                if (quantidade >= 10 && valor == 0.0 || contador == 1.0) {
                    quantidadeeditada = quantidade + 10
                    contador = 1.0
                } else if (contador == 0.0) {
                    quantidadeeditada = quantidade + 1
                } else {
                    quantidadeeditada = quantidade + 1
                }
            } else {
                quantidadeeditada = quantidade + 1
            }
            //Evita de adicionar a mais que máximo
            if (quantidadeeditada <= quantideproduto) {
                txtquantidade.text = quantidadeeditada.toString()
            } else {
                //Verifica se a próxima edição vai passar se passar para para próxima até
                //completar a quantidade
                quantidadeeditada = 0.0
                if (quantidade + 10 <= quantideproduto) {
                    quantidadeeditada = quantidade + 10
                } else if (quantidade + 1 <= quantideproduto) {
                    quantidadeeditada = quantidade + 1
                }
                txtquantidade.text = quantidadeeditada.toString()
            }


        } catch (ignored: Exception) {
        }

        return quantidadeeditada
    }


    fun aumentaQuantidadeProdutoTxt(txtquantidade: TextView, mAutoIncrement: Boolean): Double {
        var quantidadeeditada = 0.0
        try {
            val quantidade = java.lang.Double.valueOf("0" + txtquantidade.text.toString()
                    .replace(",", "."))!!

            if (mAutoIncrement) {
                //Modulo, pega o resto da divisão, o último preco do número ex: 1894 preco é 4
                val valor = quantidade % 10
                if (quantidade >= 10 && valor == 0.0 || contador == 1.0) {
                    quantidadeeditada = quantidade + 10
                    contador = 1.0
                } else if (contador == 0.0) {
                    quantidadeeditada = quantidade + 1
                } else {
                    quantidadeeditada = quantidade + 1
                }
            } else {
                quantidadeeditada = quantidade + 1
            }

            txtquantidade.text = quantidadeeditada.toString()
        } catch (ignored: Exception) {
        }

        return if (mAutoIncrement) {
            quantidadeeditada
        } else {
            contador = 0.0
            0.0
        }
    }

    fun aumentaQuantidadeProdutoEdt(edtquantidade: EditText, mAutoIncrement: Boolean): Double {
        var quantidadeeditada = 0.0
        try {

            val quantidade = java.lang.Double.valueOf("0" + edtquantidade.text.toString()
                    .replace(",", "."))!!
            if (mAutoIncrement) {
                //Modulo, pega o resto da divisão, o último preco do número ex: 1894 preco é 4
                val valor = quantidade % 10

                if (quantidade >= 10 && valor == 0.0 || contador == 1.0) {
                    quantidadeeditada = quantidade + 10
                    contador = 1.0
                } else if (contador == 0.0) {
                    quantidadeeditada = quantidade + 1
                } else {
                    quantidadeeditada = quantidade + 1
                }
            } else {
                quantidadeeditada = quantidade + 1
            }

            edtquantidade.setText(quantidadeeditada.toString())
        } catch (ignored: Exception) {
        }

        return if (mAutoIncrement) {
            quantidadeeditada
        } else {
            0.0
        }
    }

    fun diminuiQuantidadeProdutoTxt(txtquantidade: TextView, mAutoDecrement: Boolean): Double {
        var quantidadeeditada = 0.0
        try {
            val quantidade = java.lang.Double.valueOf("0" + txtquantidade.text.toString()
                    .replace(",", "."))!!
            if (quantidade > 0) {
                //Diminui conforme a numeração
                if (mAutoDecrement) {
                    //Modulo, pega o resto da divisão, o último preco do número ex: 1894 preco é 4
                    val valor = quantidade % 10
                    if (quantidade >= 10 && valor == 0.0 || contador == 1.0) {
                        quantidadeeditada = quantidade - 10
                        contador = 1.0
                    } else if (contador == 0.0) {
                        quantidadeeditada = quantidade - 1
                    } else {
                        quantidadeeditada = quantidade - 1
                    }
                } else {
                    quantidadeeditada = quantidade - 1
                }
            }
            if (quantidadeeditada >= 0) {
                txtquantidade.text = quantidadeeditada.toString()
            } else {
                //Verifica se a próxima edição vai passar se passar para para próxima até
                //completar a quantidade
                quantidadeeditada = 0.0
                if (quantidade - 10 >= 0) {
                    quantidadeeditada = quantidade - 10
                } else if (quantidade - 1 >= 0) {
                    quantidadeeditada = quantidade - 1
                }
                txtquantidade.text = quantidadeeditada.toString()
            }
        } catch (ignored: Exception) {
        }

        return quantidadeeditada
    }

    fun diminuiQuantidadeProdutoEdt(edtquantidade: EditText, mAutoDecrement: Boolean): Double {
        var quantidadeeditada = 0.0
        try {
            val quantidade = java.lang.Double.valueOf("0" + edtquantidade.text.toString()
                    .replace(",", "."))!!
            if (quantidade > 0) {
                //Diminui conforme a numeração
                if (mAutoDecrement) {
                    //Modulo, pega o resto da divisão, o último preco do número ex: 1894 preco é 4
                    val valor = quantidade % 10
                    if (quantidade >= 10 && valor == 0.0 || contador == 1.0) {
                        quantidadeeditada = quantidade - 10
                        contador = 1.0
                    } else if (contador == 0.0) {
                        quantidadeeditada = quantidade - 1
                    } else {
                        quantidadeeditada = quantidade - 1
                    }
                } else {
                    quantidadeeditada = quantidade - 1
                }
            }
            if (quantidadeeditada >= 0) {
                edtquantidade.setText(quantidadeeditada.toString())
            } else {
                //Verifica se a próxima edição vai passar se passar para para próxima até
                //completar a quantidade
                quantidadeeditada = 0.0
                if (quantidade - 10 >= 0) {
                    quantidadeeditada = quantidade - 10
                } else if (quantidade - 1 >= 0) {
                    quantidadeeditada = quantidade - 1
                }
                edtquantidade.setText(quantidadeeditada.toString())
            }
        } catch (ignored: Exception) {
        }

        return quantidadeeditada
    }

    fun diminuiQuantidadeProdutoTxt(txtquantidade: TextView) {
        try {
            var quantidade = java.lang.Double.valueOf("0" + txtquantidade.text.toString()
                    .replace(",", "."))!!
            if (quantidade > 0) {
                quantidade--
                txtquantidade.text = quantidade.toString()
            }
        } catch (ignored: Exception) {
        }

    }

    fun zerarContador() {
        contador = 0.0
    }

    fun mostrarTecladoaoFocar(context: Context, editText: EditText) {
        editText.isFocusableInTouchMode = true
        editText.requestFocus()
        val imm = context.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText.windowToken, 0)

        editText.requestFocus()
        imm.showSoftInput(editText, 0)
    }

    fun stringparaArray(texto: String): List<String>? {
        try {
            return ArrayList(Arrays.asList(*texto.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()))
        } catch (ignored: Exception) {

        }

        return null
    }

    fun preencherSpinner(context: Context, spinner: Spinner, listDescricoes: List<String>) {
        //ArrayAdapter para preencher o spinner, com  o arrayList de string
        val arrayAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, listDescricoes)
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        //Seta adapter do spinner
        spinner.adapter = arrayAdapter
    }

    fun preencherSpinnerCenterBranco(context: Context, spinner: Spinner, listDescricoes: List<String>) {
        //ArrayAdapter para preencher o spinner, com  o arrayList de string
        val arrayAdapter = ArrayAdapter(context, R.layout.spinner_item_branco, listDescricoes)
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        //Seta adapter do spinner
        spinner.adapter = arrayAdapter

    }

    fun preencherRadioButtons(radioButtonSim: RadioButton, radioButtonNao: RadioButton,
                              arrayList: List<String>) {
        try {
            // Aplicando o texto do radiobutton
            radioButtonSim.text = arrayList[0]
            radioButtonNao.text = arrayList[1]
            // Aplicando o tamanho do texto do radiobutton
            radioButtonSim.textSize = 16f
            radioButtonNao.textSize = 16f
        } catch (ignored: Exception) {

        }

    }

    fun buscarStatus(statusList: List<Status>, status_id: Int): Status? {
        // Busca statos que tenha o id semelhante
        // ao encontrar status recebe o encontrado e sai
        return statusList.firstOrNull { status_id == it.status_id }
    }

    fun busDescricaoStatus(statusList: List<Status>, status_id: Int): String {
        val status = buscarStatus(statusList, status_id)

        return if (status != null) status.descricao!! else "STATUS INVÁLIDO OU NÃO EXISTE"
    }

    fun mostrarNadaConstaOuPerdadeConexao(resposta: String,
                                          llcarregando: LinearLayout,
                                          txtNadaconsta: TextView,
                                          txtSemConexao: TextView) {
        // Esconder progress
        llcarregando.visibility = View.GONE

        if (resposta.equals(SEM_ERRO, ignoreCase = true)) {
            txtSemConexao.visibility = View.GONE
            txtNadaconsta.visibility = View.VISIBLE
        } else {
            txtNadaconsta.visibility = View.GONE
            txtSemConexao.text = ReturnException.mostraErroConexaoGeral(resposta)
            txtSemConexao.visibility = View.VISIBLE
        }
    }

    fun mostrarProgressnoAsync(mRecyclerView: RecyclerView, llcarregando: LinearLayout,
                               txtNadaconsta: TextView, txtSemConexao: TextView) {
        // Progress visível caso não esteja visível
        llcarregando.visibility = View.VISIBLE
        // Esconder nada consta
        txtNadaconsta.visibility = View.GONE
        // Esconder TextView sem Conexão
        txtSemConexao.visibility = View.GONE
        // Esconder Recyclerview
        mRecyclerView.visibility = View.GONE

    }

}
