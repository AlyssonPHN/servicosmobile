package eres.osmobile.com.controller

import android.content.Context
import android.content.SharedPreferences
import eres.osmobile.com.fragments.ConfiguracaoFragment

class Start {
    companion object {
        var sharedPreference: SharedPreferences? = null
        var sURL: String = ""
        var USUARIO: String = ""
        var SENHA: String = ""
        var CAMINHO: String = ""
        var CREDENCIAIS: String = ""
        var HOST: String = ""
        var PORTA: String = ""

        fun preencherCaminho(context: Context){
            sharedPreference = context.getSharedPreferences(ConfiguracaoFragment.WEB_SERVICE, Context.MODE_PRIVATE)
            try {
                if (sharedPreference!!.getString(ConfiguracaoFragment.CAMINHO, null).isNotEmpty()) {
                    HOST = sharedPreference!!.getString(ConfiguracaoFragment.CAMINHO, null)
                    PORTA = sharedPreference!!.getString(ConfiguracaoFragment.PORTA, null)
                    setAutentication(context)
                    getURL()
                }
            } catch (e: Exception){}
        }

        fun setAutentication(context: Context){
            try {
                sharedPreference = context.getSharedPreferences("LoginTemporario", Context.MODE_PRIVATE)
                USUARIO = sharedPreference!!.getString("LNome", null)
                SENHA = sharedPreference!!.getString("LSenha", null)
            } catch (ignored: Exception){}
        }

        fun getURL() :String{
            CREDENCIAIS = "$USUARIO:$SENHA"
            CAMINHO = "http://$HOST:$PORTA"
            sURL = "$CAMINHO/Datasnap/rest/"
            return sURL
        }

    }
}