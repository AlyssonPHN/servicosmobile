package eres.osmobile.com.controller

import android.annotation.SuppressLint

import java.util.HashMap

class ReturnException : Throwable {
    private var mErro: Int = 0
    private var mKeyWord: String? = null
    private var mErroEstendido = ""
    private var erroSecundario = 0

    //Retorna erro
    //Retorna erro com mensagem secundária
    val mensagem: String
        @SuppressLint("UseValueOf")
        get() {
            val retorno: String
            if (erroSecundario == 0) {
                if (mKeyWord != null) {
                    retorno = msgExceptions[mKeyWord!!].toString()
                } else {
                    retorno = mErros[mErro] + "\n" + this.mErroEstendido
                }
            } else {
                retorno = mErros[erroSecundario] + "\n" + this.mErroEstendido
            }
            return retorno
        }


    constructor(erro: Int) {
        this.mErro = erro
    }

    constructor(keyWord: String) {
        this.mKeyWord = keyWord
    }

    constructor(erro: Int, erroEstendido: String) : super() {
        this.mErro = erro
        this.mErroEstendido = erroEstendido
    }

    constructor(erro: Int, erroEstendido: String, erroSecundario: Int) : super() {
        this.mErro = erro
        this.mErroEstendido = erroEstendido
        this.erroSecundario = erroSecundario
    }

    companion object {
        private val serialVersionUID = 1L

        @SuppressLint("UseSparseArrays")
        private val mErros = object : HashMap<Int, String>() {

            private val serialVersionUID = 1L

            init {
                put(0, "Sem erro!")
                put(1, "Erro ao acessar base de dados!")
                put(-2, "Dispositivo não liberado!")
                put(-3, "Usuário não encontrado!")
                put(-4, "Senha Incorreta!")
                put(-5, "Informações não encontradas!")
                put(-6, "Erro ao logar!")
                put(-7, "Erro na execução da solicitação!")
                put(-8, "Cliente já cadastrado!")
                put(-9, "Produto sem Saldo de Estoque!")
                put(-10, "Versão do webservice incompatível!")
            }
        }

        private val msgExceptions = object : HashMap<String, String>() {

            internal val aux = "Requisição não transmitida!\n"
            private val serialVersionUID = 1L

            init {
                put("ECONNREFUSED", aux + "Não foi possível conectar ao webservice!")
                put("ETIMEDOUT", aux + "Tempo de espera esgotado!")
                put("ENETUNREACH", aux + "Aparelho sem internet!")
            }
        }

        fun mostraErroConexaoVisitacao(erro: String): String {
            val mensagem: String
            if (erro.contains("SocketTimeoutException")) {
                mensagem = "Tempo de espera esgotado!"
            } else if (erro.contains("ECONNREFUSED")) {
                mensagem = "Não foi possível conectar ao webservice!"
            } else if (erro.contains("ETIMEDOUT")) {
                mensagem = "Tempo de espera esgotado!"
            } else if (erro.contains("ENETUNREACH")) {
                mensagem = "Aparelho sem internet!"
            } else if (erro.contains("failed to connect")) {
                mensagem = "Problema na conexão com o web service.\n Verifique o número host."
            } else if (erro.contains("Index: 0")) {
                mensagem = "Sem clientes para o dia."
            } else {
                mensagem = erro
            }
            return mensagem
        }

        fun mostraErroConexaoGeral(erro: String): String {
            val mensagem: String
            if (erro.contains("SocketTimeoutException") || erro.contains("timed out")) {
                mensagem = "Tempo de espera esgotado!"
            } else if (erro.contains("ECONNREFUSED") || erro.contains("refused")) {
                mensagem = "Não foi possível conectar ao webservice!"
            } else if (erro.contains("ETIMEDOUT")) {
                mensagem = "Tempo de espera esgotado!"
            } else if (erro.contains("ENETUNREACH")) {
                mensagem = "Aparelho sem internet!"
            } else if (erro.contains("Failed to connect")) {
                mensagem = "Problema na conexão com o web service.\nVerifique o endereço do Web Service."
            } else if (erro.contains("Network is unreachable")) {
                mensagem = "Aparelho sem internet!"
            } else {
                mensagem = erro
            }
            return mensagem
        }
    }
}
